#!/usr/bin/perl
# <one line to give a brief idea of what this does.>
# 
# Copyright 2003-2005 (c) Mathieu Roy <yeupou--gnu.org>
#                         Timothee Besset <ttimo--ttimo.net>
# Copyright (C) 2011-2016 Sergey Poznyakoff <gray@gnu.org>
#
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#

##
## Desc: any subs related to database access...
## Usually used in other subs.
##
package Savane::Conf;
    
use strict "vars";
require Exporter;
use Carp;
use Text::Wrap;
use Savane::Config;
use Savane::Setup;

# Exports
our @ISA = qw(Exporter);
our @EXPORT = qw(GetConf IsSet Parse Load UpToDate);
our @EXPORT_OK = qw(for_each_keyword find_keyword);
our %EXPORT_TAGS = (
    'all' => [ qw(GetConf IsSet Parse Load for_each_keyword find_keyword) ] );
our $version = 1;

use constant {
    CONF_CHECK_FILES => 0x1,
    CONF_CHECK_USERS => 0x2,
    CONF_CHECK_ALL => CONF_CHECK_FILES|CONF_CHECK_USERS
};

my $check_level = CONF_CHECK_ALL;

# Override import.
sub import {
    my $pkg = shift;            # package
    my @syms = ();              # symbols to import
    my $load;                   # configuration
    my $dest = \@syms;          # symbols first
    for (@_) {
        if ($_ eq ':load') {
            $load = 1;          # config next
            next;
        }
        push(@$dest, $_);       # push
    }
    if (my $s = $ENV{SAVANE_CONFIG_CHECK_MASK}) {
	foreach my $v (split $s, /,/) {
	    my %kw = (
		files => CONF_CHECK_FILES,
		users => CONF_CHECK_USERS,
		all => CONF_CHECK_ALL
	    );
	    if (my $v = $kw{$1}) {
		$check_level &= ~$v;
	    }
	}
    }
    # Hide one level and call super.
    local $Exporter::ExportLevel = 1;
    $pkg->SUPER::import(@syms);
    Load() if $load;
}

sub check_enum {
    my $vref = shift;
    unless (grep { $_ eq $$vref } @_) {
	return "valid values are: " . join(', ', @_);
    }
    return undef;
}

sub check_bool {
    my $vref = shift;
    my %btab = (
	1 => 1,
	0 => 0,
	'yes'  => 1,
	'no'   => 0,
	'true' => 1,
	'false' => 0,
	'on'  => 1,
	'off' => 0,
	't'   => 1,
	'nil' => 0
    );
    my $v = lc $$vref;
    return "Not a boolean: $$vref" unless exists $btab{$v};
    $$vref = $btab{$v};
    return undef;
}

sub repr_bool {
    my $v = shift;
    return $v ? 'On' : 'Off';
}

sub check_dir {
    my $vref = shift;
    if ($check_level & CONF_CHECK_FILES) {
	return "Directory $$vref does not exist" unless -e $$vref;
	return "$$vref is not a directory" unless -d $$vref;
    }
    return undef;
}

sub check_int {
    my $vref = shift;
    return "not a valid number: $$vref" unless $$vref =~ /^\d+$/;
    return undef;
}

sub check_unix_user {
    my $vref = shift;
    if ($check_level & CONF_CHECK_USERS) {
	my $uid = getpwnam $$vref;
	return "No such user: $$vref" unless defined $uid;
    }
    return undef;
}

sub check_unix_group {
    my $vref = shift;
    if ($check_level & CONF_CHECK_USERS) {
	my $gid = getgrnam $$vref; 
	return "No such group: $$vref" unless defined $gid;
    }
    return undef;
}

sub check_top_directory {
    my $vref = shift;
    if ($check_level & CONF_CHECK_FILES) {
	return "Directory '$$vref' doesn't exist" unless -d $$vref;
	return "The file $$vref/index.php does not exist"
	    unless -e "$$vref/index.php";
    }
    return undef;
}

sub check_securimage_directory {
    my $vref = shift;
    if (($check_level & CONF_CHECK_FILES) && defined($$vref)) {
	return "File $$vref/securimage.php does not exist"
	    unless -f "$$vref/securimage.php";
    }
    return undef;
}

sub check_shell {
    my $vref = shift;
    if (($check_level & CONF_CHECK_FILES) && defined($$vref)) {
	return "File $$vref does not exist" unless -e $$vref;
	return "File $$vref is not executable" unless -x $$vref;
    }
    return undef;
}

sub check_upload_max {
    my $vref = shift;
    if (defined($$vref)) {
	return "Not a valid size: $$vref"
	    unless $$vref =~ /\d+([kKmMgG][bB]?)?/;
    }
    return undef;
}

my %keywords = (
    core => {
	section => {
	    domain => {
		mandatory => 1,
		oldvar => '$sys_default_domain',
		php => 1
	    },
	    https_port => {
		oldvar => '$sys_https_port',
		php => 1
	    },
	    brother_domain => {
		oldvar => '$sys_brother_domain',
		php => 1
	    },
	    top_url => {
		oldvar => '$sys_url_topdir',
		php => 1
	    },
	    locale => {
		oldvar => '$sys_default_locale',
		php => 1
	    },
	    date_format => {
		oldvar => '$sys_datefmt',
		php => 1
	    },
	    wiki_url => {
		oldvar => '$sys_wiki_url',
		php => 1
	    },
	    upload_max => {
		check => \&check_upload_max,
		oldvar => '$sys_upload_max',
		php => 1
	    },
	}
    },
    sql => {
	section => {
	    host => {
		mandatory => 1,
		oldvar => '$sys_dbhost',
		php => 1
	    },
	    database => {
		mandatory => 1,
		oldvar => '$sys_dbname',
		php => 1
	    },
	    user => {
		mandatory => 1,
		oldvar => '$sys_dbuser',
		php => 1
	    },
	    password => {
		oldvar => '$sys_dbpasswd',
		php => 1
	    },
	    params => {
		oldvar => '$sys_dbparams',
		php => 1
	    }
	}
    },
    path => {
	section => {
	    top_directory => {
		mandatory => 1,
		check => \&check_top_directory,
		oldvar => '$sys_www_topdir',
		php => 1,
	    },
	    include_directory => {
		mandatory => 1,
		check => \&check_dir,
		oldvar => '$sys_incdir',
		php => 1
	    },
	    application_directory => {
		check => \&check_dir,
		oldvar => '$sys_appdatadir',
		php => 1
	    },
	    attachment_directory => {
		check => \&check_dir,
		oldvar => '$sys_trackers_attachments_dir',
		php => 1
	    },
	    common_name => {
		mandatory => 1,
		oldvar => '$sys_name',
		php => 1
	    },
	    admin_project_name => {
		mandatory => 1,
		check => \&check_unix_group,
		oldvar => '$sys_unix_group_name',
		php => 1
	    },
	    default_theme => {
		mandatory => 1,
		oldvar => '$sys_themedefault',		
		php => 1
		# FIXME: Consistency check after parse
	    },
	    logo_name => {
		# FIXME: Consistency check after parse
		oldvar => '$sys_logo_name',
		php => 1
	    },
	    userx_prefix => {
		oldvar => '$sys_userx_prefix',
		php => 1
	    }
	}
    },
    forum => {
	section => {
	    enable_comments => {
		check => \&check_bool,
		repr => \&repr_bool,
		oldvar => '$sys_enable_forum_comments',
		php => 1
	    },
	    registration_captcha => {
		check => \&check_bool,
		repr => \&repr_bool,
		oldvar => '$sys_registration_captcha',
		php => 1
	    },
	    securimage_directory => {
		check => \&check_securimage_directory,
		oldvar => '$sys_securimagedir',
		php => 1		
	    }
	}
    },
    auth => {
	section => {
	    crack_dictionary => {
		# FIXME: check
		oldvar => '$sys_crack_dictionary',
		php => 1
	    },
	    pwcheck => {
		check => \&check_bool,
		repr => \&repr_bool,
		oldvar => '$use_pwqcheck',
		php => 1		
	    },
	    accept_md5_hashes => {
		check => \&check_bool,
		repr => \&repr_bool,
		oldvar => '$sys_accept_md5_hashes',
		php => 1		
	    },
	    upgrade_hash => {
		check => \&check_bool,
		repr => \&repr_bool,
		oldvar => '$sys_upgrade_user_pw_hash',
		php => 1		
	    },
	    use_pam => {
		check => \&check_bool,
		repr => \&repr_bool,
		oldvar => '$sys_use_pamauth',
		php => 1		
            },
	    use_krb5 => {
		check => \&check_bool,
		repr => \&repr_bool,
		oldvar => '$sys_use_krb5',
		php => 1		
	    }
	}
    },
    mail => {
	section => {
	    mail_domain => {
		mandatory => 1,
		oldvar => '$sys_mail_domain',
		php => 1		
	    },
	    admin_address => {
		mandatory => 1,
		oldvar => '$sys_mail_admin',
		php => 1		
	    },
	    sender_address => {
		mandatory => 1,
		oldvar => '$sys_mail_replyto',
		php => 1		
	    }
	}
    },
    users => {
	section => {
	    group => {
		check => \&check_unix_group,
		oldvar => '$sys_savane_user_group'
	    },
	    min_uid => {
		mandatory => 1,
		oldvar => '$sys_min_uid'
	    },
	    min_gid => {
		mandatory => 1,
		oldvar => '$sys_min_gid'
  	    },
	    home_dir => {
		check => \&check_dir,
		oldvar => '$sys_homedir'
	    },
	    home_subdirs => {
		check => \&check_int,
		oldvar => '$sys_homedir_subdirs'
	    },
	    shell => {
		check => \&check_shell,
		oldvar => '$sys_shell',
                php => 1  # Not exactly necessary, but some site-specific
                          # scripts might rely on it in order to discern 
                          # between different user categories. 
	    }
	}
    },
    download => {
	section => {
	    owner => { oldvar => '$sys_download_owner' } #{ check => \&check_download_owner }
	}
    },
    homepage => {
	section => {
	    owner => 1
	}
    },
    web => {
	section => {
	    oncommit => {
		section => { '*' => 1 },
		oldvar => '%sys_web_oncommit'
	    }
	}
    },
    source => {
	section => {
	    oncommit => {
		section => { '*' => 1 },
		oldvar => '%sys_sources_oncommit'
	    }
	}
    },
    development => {
	section => {
	    debug => {
		check => \&check_bool,
		repr => \&repr_bool,
		oldvar => '$sys_debug_on',
		php => 1
	    },
	    no_base_host => {
		check => \&check_bool,
		repr => \&repr_bool,
		oldvar => '$sys_debug_nobasehost',
		php => 1
            },
	    allow_mail_to => {
		oldvar => '@sys_allow_mail_to',
		php => 1
	    },
	    force_mail_to => {
		oldvar => '@sys_force_mail_to',
		php => 1
	    }
	}
    },
    backend => {
	section => {
	    syslog_facility => { oldvar => '$sys_syslog_facility' },
	    sv_cleaner => {
		section => {
		    cron => {
			check => \&check_bool,
			repr => \&repr_bool,
			oldvar => '$sys_cron_cleaner'
		    },
		    timeout => {
			section => {
			    pending_accounts => 1,
			    inactive_groups  => 1,
			    inactive_forms   => 1,
			    banned_ips       => 1,
			    sessions         => 1,
			    passwd_requests  => 1
			}
		    }
		}
	    },
	    sv_reminder => {
		section => {
		    cron => {
			check => \&check_bool,
			repr => \&repr_bool,
			oldvar => '$sys_cron_reminder'
		    },
		    max_mails => {
			check => \&check_int
		    },
		    max_items => {
			check => \&check_int
		    }	    
		}
	    },
	    sv_aliases => {
		section => {
		    cron => {
			check => \&check_bool,
			repr => \&repr_bool,
			oldvar => '$sys_cron_mail'
		    },
		    alias_file => {
			oldvar => '$sys_mail_aliases'
		    },
		    rebuild_command => {
			oldvar => '$sys_mail_rebuild'
		    },
		    ignore_groups => {
			oldvar => '$sys_mail_ignore_groups'
		    },
		    ignore_lurkers => {
			check => \&check_bool,
			repr => \&repr_bool,
			oldvar => '$sys_mail_ignore_lurkers'
		    },
		}
	    },
	    sv_users => {
		section => {
		    cron => {
			check => \&check_bool,	
			repr => \&repr_bool,
			oldvar => '$sys_cron_users'
		    },
		    authorized_keys => {
			check => \&check_bool,
			repr => \&repr_bool,
			oldvar => '$sys_authorized_keys'
		    },
		    skel_dir => {
			check => \&check_dir,
			oldvar => '$sys_skel_dir'
		    },
		    remove_unassigned_users => {
			check => \&check_bool,
			repr => \&repr_bool,
			oldvar => '$sys_remove_unassigned_users'
		    },
		}
	    },
	    sv_groups => {
		section => {
		    cron => {
			check => \&check_bool,
			repr => \&repr_bool,
			oldvar => '$sys_cron_groups'
		    },
		    project_list => {
			oldvar => '$sys_git_projects_file'
		    },
		    cgitrepos_file => {
			oldvar => '$sys_git_cgitrepos_file'
		    }
		}
	    },
	    sv_mailman => {
		section => {
		    cron => {
			check => \&check_bool,
			repr => \&repr_bool,
			oldvar => '$sys_mailman_cron'
		    },
		    mailman_dir => {
                        check => \&check_dir,
			oldvar => '$sys_mailman_dir'
		    },
		    mailman_bin_dir => {
                        check => \&check_dir
		    },
		    list_dir => {
			check => \&check_dir,
			oldvar => '$sys_mailman_list_dir'
		    },
		    user => {
			check => \&check_unix_user,
			oldvar => '$sys_mailman_user'
		    },
		    archive_dir => {
			check => \&check_dir,
			oldvar => '$sys_mailman_archive_dir'
		    },
		    keep_archives => {
			check => \&check_bool,
			repr => \&repr_bool,
			oldvar => '$sys_mailman_keep_archives'
		    },
		    directory_file => {
			oldvar => '$sys_mailman_directory_file'
		    },
		    mailman_command => 1
		}
	    },
	    sv_spamchecker => {
		section => {
		    check => {
			check => sub {
			    my ($val, $msgref) = @_;
			    return check_enum($val, $msgref,
					      '1', '2', 'anonymous', 'all');
			},
			oldvar => '$sys_spamcheck_spamassassin',
			php => 1
		    },
		    socket => { oldvar => '$sys_spamc_socketpath' },
		    host => { oldvar => '$sys_spamc_host' },
		    port => { oldvar => '$sys_spamc_port' },
		    timeout => { oldvar => '$sys_spamc_timeout' },
		    expiry => { oldvar => '$sys_spamcheck_expiry' },	    
		    user => { oldvar => '$sys_spamc_username' },
		    cron => {
			oldvar => '$sys_cron_spamchecker',
			check => \&check_bool,
			repr => \&repr_bool,
		    }
		}
	    },
	    sv_dumpdb => {
		section => {
		    cron => {
			check => \&check_bool,
			repr => \&repr_bool,
			oldvar => '$sys_cron_dumpdb'
		    },
		    dumpdir => {
			check => \&check_dir,
			oldvar => '$sys_dumpdir'
		    },
		    max_level => {
			check => \&check_int,
			oldvar => '$sys_backup_max_level'
		    },
		    max_rotate => {
			check => \&check_int,
			oldvar => '$sys_backup_maxrotate'
		    },
		    compress_program => {
			oldvar => '$sys_backup_compress_program'
		    },
		    compress_suffix => {
			oldvar => '$sys_compress_suffix'
		    }
		}
	    },
	    sv_repo_backup => {
		section => {
		    cron => {
			check => \&check_bool,
			repr => \&repr_bool,
			oldvar => '$sys_cron_repo_backup'
		    },
		    user => {
			check => \&check_unix_user,
			oldvar => '$sys_repo_backup_user'
		    },
		    snapshots => {
			check => \&check_bool
		    },
		    backup_types => 1,
		    exclude_types => 1,
		    exclude_groups => 1,
		    output => {
			section => { '*' => 1 },
		    }
		}
	    },
	    sv_getpubkeys => {
		section => {
		    pubkey_options => {
			oldvar => '$sys_pubkey_options'
		    }
		}
	    }
	}
    },
    index => {
	section => {
	    html_root => {
		check => \&check_dir,
		oldvar => '$sys_software_html_root'
	    },
	    doc_dir => {
		oldvar => '$sys_software_docdirs'
	    },
	    index_dir => {
		oldvar => '$sys_software_index_dir'
	    },
	    index_config_trunk => {
		oldvar => '$sys_software_index_config_trunk'
	    },
	    xmlpipe_command => {
		oldvar => '$sys_software_xmlpipe_command'
	    },
	    reindex_command => {
		oldvar => '$sys_software_reindex_command'
	    },
	    reindex_db => {
		oldvar => '$sys_software_reindex_db'
	    },
	    reindex_delay => {
		oldvar => '$sys_software_reindex_delay'
	    }
	}
    }
);

sub _for_each_kw {
    my $code = shift;
    my $aref = shift;

    while (my ($k, $v) = each %{$aref}) {
	if (ref($v) eq 'HASH' and exists($v->{section})) {
	    _for_each_kw($code, $v->{section}, @_, $k);
	} else {
	    &{$code}($v, @_, $k);
	}
    }
}

sub for_each_keyword {
    my $code = shift;
    _for_each_kw($code, \%keywords);
}

sub find_keyword {
    my $kw = \%keywords;

    while (my $arg = shift) {
	last unless ref($kw) eq 'HASH';
	last unless exists($kw->{$arg});
	$kw = $kw->{$arg};
	last unless ref($kw) eq 'HASH';
	if ($#_ == -1) {
	    return $kw;
	} elsif (exists($kw->{section})) {
	    $kw = $kw->{section};
	} else {
	    last;
	}
    }
    return undef;
}

sub Parse {
    my $filename = shift;
    my $conf = new Savane::Config($filename, parameters => \%keywords, @_);
    $conf->parse() or return undef;
    return $conf;
}

my $config;

sub UpToDate {
    my $file = shift;
    carp "configuration not loaded" unless defined $config;
    return $config->file_up_to_date($file);
}

# Load: read configuration file
# 
# FIXME: we should have some way to select different Savannah installation
# on the same computer, for instance by saving the location of their conffiles
# in /etc/savannah.list
# Currently the solution is to do what we do with Apache, we set an environment
# variable, SAVANE_CONF.
sub Load {
    my $confdir = $ENV{SAVANE_CONF} || setupconf(SV_CONFIG_DIR);
    die "configuration directory $confdir does not exist"
	unless -d $confdir;
    my $conffile = "$confdir/savane.conf";
    die "configuration file $conffile does not exist"
	unless -e $conffile;

    $config = Parse($conffile,
		    cache => 1,
		    error => sub {
			my $text = shift;
			print STDERR "$text\n";
		    }) or die;
}

# Return a configuration item
sub GetConf {
    my $s = shift;
    Load unless $config;
    return $config->get(split /\./, $s);
}

sub IsSet {
    my $s = shift;
    Load unless $config;
    return $config->isset(split /\./, $s);
}

1;
