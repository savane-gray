# Backward compatibility layer for basic homepages.
# Copyright (C) 2016  Sergey Poznyakoff <gray@gnu.org>
# License GPLv3+: GNU GPL version 3 or later
# <http://gnu.org/licenses/gpl.html>
# This is free software: you are free  to  change  and  redistribute  it.
# There is NO WARRANTY, to the extent permitted by law.

package Savane::compat::homepage::basicdirectory;
use strict;

require Savane::Feature::Homepage::Basic;
our @ISA = qw(Savane::Feature::Homepage::Basic);
