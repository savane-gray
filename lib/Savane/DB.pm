#!/usr/bin/perl
# <one line to give a brief idea of what this does.>
# 
# Copyright 2003-2005 (c) Mathieu Roy <yeupou--gnu.org>
#                         Yves Perrin <yves.perrin--cern.ch>
# Copyright (C) 2011-2016 Sergey Poznyakoff <gray@gnu.org>
#
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


##
## Desc: any subs related to database access...
## Usually used in other subs.
##
package Savane::DB;
use strict;
use DBI;
use Carp;
use Savane::Conf qw(:load);
require Exporter;

# Exports
our @ISA = qw(Exporter);
our @EXPORT = qw(GetDBDescribe GetDBSettings GetDBList GetDBAsIs GetDB GetDBAlt GetDBHash GetDBHashArray GetDBLists GetDBListsRef SetDBSettings DeleteDB InsertDB db_insert db_foreach db_modify db_delete);
our $version = 1;

# Imports (needed for strict).
my $sys_dbname = GetConf('sql.database');
my $sys_dbhost = GetConf('sql.host');
my $sys_dbuser = GetConf('sql.user');
my $sys_dbpasswd = GetConf('sql.password');
my $sys_dbparams = GetConf('sql.params');

# Init
our $dbd =DBI->connect('DBI:mysql:database='.$sys_dbname
		       .':host='.$sys_dbhost
		       .':'.$sys_dbparams,
                       $sys_dbuser, $sys_dbpasswd,
                       { RaiseError => 1, AutoCommit => 1});
my $hop = $dbd->prepare("SET NAMES utf8");
$hop->execute;
$hop->finish;

## Returns the name and type of every field of the table
# arg0 : table
sub GetDBDescribe {
    my $table = $_[0];
    my $hop;
    my @ret;

    $hop = $dbd->prepare("DESCRIBE ".$table);
    $hop->execute;
    while (my (@line) = $hop->fetchrow_array) {
        push(@ret, join(",", map {defined $_ ? $_ : "0"} @line));

    }
    $hop->finish;
    return @ret;
}

## Returns all settings  for a group, user (a row)..
# @ary = GetDBSettings(COLS, TABLE, COND, ...)
# Arguments:
#  COLS  - columns to retrieve
#  TABLE - SQL table to look in
#  COND  - WHERE condition
# Rest of arguments supplies values for ? placeholders in COND
sub GetDBSettings {
    my $columns = shift;
    my $table = shift;
    my $cond = shift;

    my $q = qq{SELECT $columns FROM $table WHERE $cond};
    my $sth = $dbd->prepare($q);
    my $res = $sth->execute(@_) or croak($sth->errstr);
    return $sth->fetchrow_array;
}

## Returns a list of entries 
# arg0 : table
# arg1 : criterion
# arg2 : fields to show (* for all)
sub GetDBList {
    my $table = $_[0];
    my $criterion;
    my $fields = "*";

    $criterion = "WHERE ".$_[1] if $_[1];
    $fields = $_[2] if $_[2];

    my $list = $dbd->selectcol_arrayref("SELECT ".$fields." FROM ".$table." ".$criterion);

    return @$list;

}

## Returns a list of entries
# arg0 : sql command
sub GetDBAsIs {
    my $sql = $_[0];
    my $hop;
    my @ret;
                                                                                
    $hop = $dbd->prepare($sql);
    $hop->execute;
    while (my (@line) = $hop->fetchrow_array) {
        push(@ret, join(",", map {defined $_ ? $_ : "0"} @line));
    }
    $hop->finish;
    return @ret;
}

## Returns a list of entries 
# arg0 : table
# arg1 : criterion
# arg2 : fields to show (* for all)
sub GetDB {
    my $table = $_[0];
    my $criterion;
    my $fields = "*";
    my $hop;
    my @ret;

    $criterion = "WHERE ".$_[1] if $_[1];
    $fields = $_[2] if $_[2];

    $hop = $dbd->prepare("SELECT ".$fields." FROM ".$table." ".$criterion);
    $hop->execute;
    while (my (@line) = $hop->fetchrow_array) {
	push(@ret, join(",", map {defined $_ ? $_ : "0"} @line));
	
    }
    $hop->finish;
    return @ret;
}

## Returns a list of entries, like GetDB but with an unusual separator
# arg0 : table
# arg1 : criterion
# arg2 : fields to show (* for all)
sub GetDBAlt {
    my $table = $_[0];
    my $criterion;
    my $fields = "*";
    my $hop;
    my @ret;

    $criterion = "WHERE ".$_[1] if $_[1];
    $fields = $_[2] if $_[2];

    $hop = $dbd->prepare("SELECT ".$fields." FROM ".$table." ".$criterion);
    $hop->execute;
    while (my (@line) = $hop->fetchrow_array) {
	push(@ret, join("------SPLIT------", map {defined $_ ? $_ : "0"} @line));
	
    }
    $hop->finish;
    return @ret;
}

## Returns a hash of lists of entries
# arg0 : table
# arg1 : criterion
# arg2 : fields to show (* for all)
sub GetDBHash {
    my $table = $_[0];
    my $criterion;
    my $fields = "*";
    my $hop;
    my %ret;

    $criterion = "WHERE ".$_[1] if $_[1];
    $fields = $_[2] if $_[2];

    $hop = $dbd->prepare("SELECT ".$fields." FROM ".$table." ".$criterion);
    $hop->execute;
    my $count;
    while (my (@line) = $hop->fetchrow_array) {
	$ret{$count} = [@line];
	$count++;
    }
    $hop->finish;
    return %ret;
}

## Returns a hash of lists of entries
# arg0 : table
# arg1 : criterion
# arg2 : fields to show (* for all)
sub GetDBHashArray {
    my $table = $_[0];
    my $criterion;
    my $fields = "*";
    my $hop;
    my @ret;

    $criterion = "WHERE ".$_[1] if $_[1];
    $fields = $_[2] if $_[2];

    $hop = $dbd->prepare("SELECT ".$fields." FROM ".$table." ".$criterion);
    $hop->execute;
    while (my $ref = $hop->fetchrow_hashref) {
	push(@ret, $ref);
    }
    $hop->finish;
    return @ret;
}


## Same purpose, but return true list 
# arg0 : table
# arg1 : criterion
# arg2 : fields to show (* for all)
sub GetDBLists {
    return @{GetDBListsRef(@_)};
}

## This one is faster, since it returns a reference to the array and
# hence doesn't make a copy of the array. It is a bit less intuitive
# to manipulate on the calling side, and is not consistent with other
# GetDB* functions, so it is a separate function.
sub GetDBListsRef {
    my $table = $_[0];
    my $criterion;
    my $fields = "*";
    my $hop;
    my @ret;

    $criterion = "WHERE ".$_[1] if $_[1];
    $fields = $_[2] if $_[2];

    $hop = $dbd->prepare("SELECT ".$fields." FROM ".$table." ".$criterion);
    $hop->execute;
    my $ret = $hop->fetchall_arrayref();
    $hop->finish;

    # fetchall_arrayref does not reuse the data pointed by the
    # reference in later calls (unlike fetchrow_arrayref), so it is
    # safe to use it.
    return $ret;
}

## Update settings for a group, user..
# SetDBSettings(TABLE, COND, ASGN, ...)
# @ary = GetDBSettings(COLS, TABLE, COND, ...)
# Arguments:
#  TABLE  - Name of the SQL table
#  COND   - WHERE condition
#  ASGN   - String of valid SQL assignments
# Rest of arguments supplies values for ? placeholders in COND
sub SetDBSettings {
    my $table = shift;
    my $cond = shift;
    my $values = shift;
    # If $values contains ? markers, reorder the actual arguments.
    if (my $n = () = $values =~ /\?/g) {
	@_ = ( @_[@_-$n..$#_], @_[0..$#_-$n] );
    }
    db_modify("UPDATE $table SET $values WHERE $cond", @_);
}


## Delete entries in database
# Use carefully
sub DeleteDB {
    my $table = $_[0];
    my $criterion = $_[1];

    unless ($table || $criterion) {
	return 0;
    }
    
    return $dbd->do("DELETE FROM ".$table." WHERE ".$criterion);
}

## Insert in database
# InsertDB(TABLE, COLS, ...)
# Arguments:
#  TABLE  - Name of the SQL table
#  COLS   - List of columns to insert
# Rest of arguments supplies values to insert.  Its count must correspond
# to the number of columns in COLS.
sub InsertDB {
    my $table = shift;
    my $fields = shift;
    db_modify("INSERT INTO $table ($fields) VALUES (" .
	      join(',', ('?') x @_) . ")",
	      @_);
}

# db_insert(TABLE, COLUMN => VALUE, ...)
sub db_insert {
    my $arg = shift;
    confess "no arguments" unless defined($arg);

    my $table;
    my @colnames;
    my @values;
    my $update;
    
    if (ref($arg) eq 'HASH') {
	$table = $arg->{table} or confess "table must be given";
	unless (exists($arg->{values}) && ref($arg->{values}) eq 'HASH') {
	    confess "bad values";
	}
	while (my ($column, $value) = each %{$arg->{values}}) {
	    push @colnames, $column;
	    push @values, $value;
	}
	if (exists($arg->{update})) {
	    my @c;
	    while (my ($column, $value) = each %{$arg->{update}}) {
		push @c, $column;
		push @values, $value;
	    }
	    $update = ' ON DUPLICATE KEY UPDATE '
		      . join(',', map { "$_=?" } @c);
	}
    } else {
	$table = $arg;
	confess "bad number of arguments" if ($@ % 2);
	for (my $i = 0; $i <= $#_; $i += 2) {
	    push @colnames, $_[$i];
	    push @values, $_[$i+1];
	}
    }

    my $fields = join(',',@colnames);
    db_modify("INSERT INTO $table ($fields) VALUES (" .
	      join(',', ('?') x @colnames) . ")" . $update,
	      @values);
}   

# db_foreach(CALLBACK, QUERY, ARGS)
sub db_foreach {
    my $cb = shift;
    my $q = shift;
    my $sth = $dbd->prepare($q);
    $sth->execute(@_) or croak($sth->errstr);
    while (my $ref = $sth->fetchrow_hashref) {
	&{$cb}($ref);
    }
    $sth->finish;
}   

# db_modify(QUERY, ARGS)
sub db_modify {
    my $q = shift;
    my $sth = $dbd->prepare($q);
    my $res = $sth->execute(@_) or croak($sth->errstr);
    $sth->finish;
    return $res;
}   

# db_delete(table => T, cond => C, args => A, dry_run => B)
sub db_delete {
    local %_ = @_;
    my $q = ($_{dry_run} ? 'SELECT count(*) as count' : 'DELETE')
	     . " FROM $_{table}";
    $q .= " WHERE $_{cond}" if exists $_{cond};
    my $sth = $dbd->prepare($q);
    my $res = $sth->execute(@{$_{args}}) or croak($sth->errstr);
    if ($_{dry_run}) {
	my $ref = $sth->fetchrow_hashref;
	$res = $ref->{count};
    }
    $sth->finish;
    return $res;
}

1;
