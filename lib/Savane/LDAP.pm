package Savane::LDAP;
use strict;
use warnings;
use Net::LDAP;
use Savane::LDAP::Config;
use Savane::LDAP::pwent;
use Savane::LDAP::grent;
use Savane::LDAP::alias;
use Savane::LDAP::Map;
use Carp;

sub find_config {
    my $class = shift;
    my @config_files = qw(/etc/ldap.conf
                          /etc/ldap/ldap.conf
			  /etc/openldap/ldap.conf);
    unshift @config_files, $ENV{LDAP_CONF} if defined($ENV{LDAP_CONF});
    for (@config_files) {
	return $_ if -e $_
    }
}

sub new {
    my ($class, %opt) = @_;
    my $self = bless {}, $class;
    my $file = delete $opt{configfile} || $class->find_config;
    if (delete $opt{noconfig}) {
	$file = undef;
    }
    $opt{savane_getpwnam_filter} ||= q{(&(objectClass=posixAccount)(uid=$arg))};
    $opt{savane_getpwuid_filter} ||= q{(&(objectClass=posixAccount)(uidNumber=$arg))};
    $opt{savane_getgrnam_filter} ||= q{(&(objectClass=posixGroup)(cn=$arg))};
    $opt{savane_getgrgid_filter} ||= q{(&(objectClass=posixGroup)(gidNumber=$arg))};
    $opt{savane_membership_filter} ||= q{(&(objectclass=posixGroup)(memberUid=$arg))};
    $opt{savane_account_object} ||= [ 'posixAccount', 'device', 'ldapPublicKey' ];
    $opt{savane_group_object} ||= [ 'posixGroup' ];

    $opt{savane_getalias_filter} ||= q{(&(objectClass=nisMailAlias)(cn=$arg))};
    $opt{savane_alias_object} ||= [ 'nisMailAlias' ];
    
    $self->{_cfg} = new Savane::LDAP::Config(%opt);
    $self->cfg->read($file) if $file;
    unless ($self->cfg->{savane_account_dn}) {
	$self->cfg->{savane_account_dn} = 'uid=$name,ou=user,'.$self->cfg->{base};
    }
    unless ($self->cfg->{savane_group_dn}) {
	$self->cfg->{savane_group_dn} = 'cn=$name,ou=group,'.$self->cfg->{base};
    }
    unless ($self->cfg->{savane_alias_dn}) {
	$self->cfg->{savane_alias_dn} = 'cn=$name,ou=mail,'.$self->cfg->{base};
    }

    $self->{_ldap} = Net::LDAP->new($self->cfg->{uri})
	or croak "unable to connect to LDAP server ".$self->cfg->{uri}.": $!";
    $self->bind;
    return $self;
}

sub bind {
    my $self = shift;
    
    if (exists($self->cfg->{ssl})
	&& $self->cfg->{ssl} eq 'start_tls') {
	my %args;
    
	$args{capath} = $self->cfg->{tls_cacertdir}
	    if (defined($self->cfg->{tls_cacertdir}));
	$args{cafile} = $self->cfg->{tls_cacert}
            if (defined($self->cfg->{tls_cacert}));
	if ($self->cfg->{tls_reqcert} eq 'none') {
	    $args{verify} = 'never';
	} elsif ($self->cfg->{tls_reqcert} eq 'allow') {
	    $args{verify} = 'optional';
	} elsif ($self->cfg->{tls_reqcert} eq 'demand'
		 or $self->cfg->{tls_reqcert} eq 'hard') {
	    $args{verify} = 'require';
	} elsif ($self->cfg->{tls_reqcert} eq 'try') {
	    $args{verify} = 'optional'; # FIXME: That's wrong
	}
	$args{clientcert} = $self->cfg->{tls_cert}
            if (defined($self->cfg->{tls_cert}));
	$args{clientkey} = $self->cfg->{tls_key}
            if (defined($self->cfg->{tls_key}));
	$args{ciphers} = $self->cfg->{tls_cipher_suite}
            if (defined($self->cfg->{tls_cipher_suite}));

	my $mesg = $self->ldap->start_tls(%args);
	croak "TLS negotiation: ".$mesg->error if $mesg->code;
    }

    my @bindargs = ();
    if (defined($self->cfg->{binddn})) {
	push(@bindargs, $self->cfg->{binddn});
	if (my $pw = $self->cfg->{'bindpw'} || $self->readpwfile()) {
	    push(@bindargs, password => $pw);
	}
    }
    my $mesg = $self->ldap->bind(@bindargs);
    croak "can't bind: ".$mesg->error if $mesg->code;
    return $self;
}

sub readpwfile {
    my $self = shift;
    my $name = $self->cfg->{bindpwfile};
    return unless $name;
    if (open(my $fh, '<', $name)) {
	my ($pw) = <$fh>;
	chomp($pw);
	close $fh;
	return $pw;
    }
}

sub cfg { shift->{_cfg} }
sub ldap { shift->{_ldap} }

my %getpwnam_attr = (
    uid => 'name',
    cn => 'cn',
    uidNumber => 'uid',
    gidNumber => 'gid',
    homeDirectory => 'dir',
    gecos => 'gecos',
    loginShell => 'shell',
    sshPublicKey => 'sshpubkey'
);

my %getgrnam_attr = (
    gidNumber => 'gid',
    cn => 'name',
    memberUid => 'mem'
);

sub getpwnam {
    my ($self, $name) = @_;
    (my $filter = $self->cfg->{savane_getpwnam_filter})
	=~ s/(?<!\\)(\$(?:name|arg))/$name/g;
    my $res = $self->ldap->search(base => $self->cfg->{base},
				  filter => $filter,
				  attrs => [keys %getpwnam_attr]);
    if ($res->code) {
	warn "$filter: ".$res->error;
	return undef
    }
	
    if (my $entry = ($res->entries())[0]) {
	return new Savane::LDAP::pwent(dn => $entry->dn,
				       %{Savane::LDAP::Map
					     ->new(%getpwnam_attr)
					     ->map($entry)});
    }
}

sub getpwuid {
    my ($self, $uid) = @_;
    (my $filter = $self->cfg->{savane_getpwuid_filter})
	=~ s/(?<!\\)(\$(?:uid|arg))/$uid/g;
    my $res = $self->ldap->search(base => $self->cfg->{base},
				  filter => $filter,
				  attrs => [keys %getpwnam_attr]);
    if ($res->code) {
	warn "$filter: ".$res->error;
	return undef
    }
	
    if (my $entry = ($res->entries())[0]) {
	return new Savane::LDAP::pwent(dn => $entry->dn,
				       %{Savane::LDAP::Map
					     ->new(%getpwnam_attr)
					     ->map($entry)});
    }
}

sub user_groups {
    my ($self, $name) = @_;
    my $arg = $name;
    (my $filter = $self->cfg->{savane_membership_filter})
	=~ s/(?<!\\)(\$(?:name|arg))/$1/eeg;
    my $res = $self->ldap->search(base => $self->cfg->{base},
				  filter => $filter,
				  attrs => [ keys %getgrnam_attr ]);
    if ($res->code) {
	warn "$filter: ".$res->error;
	return undef
    }

    map {
	new Savane::LDAP::grent(dn => $_->dn,
				%{Savane::LDAP::Map
				      ->new(%getgrnam_attr)
				      ->map($_)})
    } $res->entries()
}

sub getgrnam {
    my ($self, $name) = @_;
    my $arg = $name;
    (my $filter = $self->cfg->{savane_getgrnam_filter})
	=~ s/(?<!\\)(\$(?:name|arg))/$1/eeg;
    my $res = $self->ldap->search(base => $self->cfg->{base},
				  filter => $filter,
				  attrs => [keys %getgrnam_attr]);
    if ($res->code) {
	warn "$filter: ".$res->error;
	return undef
    }
 	
    if (my $entry = ($res->entries())[0]) {
	return new Savane::LDAP::grent(dn => $entry->dn,
				       %{Savane::LDAP::Map
					     ->new(%getgrnam_attr)
					     ->map($entry)});
    }
}

sub getgrgid {
    my ($self, $gid) = @_;
    (my $filter = $self->cfg->{savane_getgrgid_filter})
	=~ s/(?<!\\)(\$(?:name|arg))/$gid/g;
    my $res = $self->ldap->search(base => $self->cfg->{base},
				  filter => $filter,
				  attrs => [keys %getgrnam_attr]);
    if ($res->code) {
	warn "$filter: ".$res->error;
	return undef
    }
 	
    if (my $entry = ($res->entries())[0]) {
	return new Savane::LDAP::grent(dn => $entry->dn,
				       %{Savane::LDAP::Map
					     ->new(%getgrnam_attr)
					     ->map($entry)});
    }
}

sub change {
    my ($self, $ent, $attrmap, $base, $objclass) = @_;
    return unless $ent->changed;
    my $cs = Savane::LDAP::Map->rev(%$attrmap)->map($ent->changeset);
    my $res;
    if ($ent->dn) {
	$res = $self->ldap->modify($ent->dn, %$cs);
	if ($res->code) {
	    warn "modify ".$ent->dn,": ".$res->error;
	}
    } else {
	(my $dn = $base) =~ s/(?<!\\)\$(\w+)/$ent->${\$1}/gex;
	$res = $self->ldap->add($dn,
				attrs => [
				    objectClass => $objclass,
				    map { ($_, $cs->{add}{$_}) } keys %{$cs->{add}}
				]);
	if ($res->code) {
	    warn "add $dn: ".$res->error;
	} else {
	    $ent->dn($dn);
	}
    }
    return !$res->code;
}

sub chpwent {
    my ($self, $ent) = @_;
    $self->change($ent, \%getpwnam_attr,
		  $self->cfg->{savane_account_dn},
		  $self->cfg->{savane_account_object});
}

sub chgrent {
    my ($self, $grent) = @_;
    $self->change($grent, \%getgrnam_attr,
		  $self->cfg->{savane_group_dn},
		  $self->cfg->{savane_group_object});
}

sub delete {
    my ($self, $ent) = @_;
    croak "no dn!" unless $ent->dn;
    my $res = $self->ldap->delete($ent->dn);
    if ($res->code) {
	warn 'delete '.$ent->dn.': '.$res->error;
	return 0
    }
    return 1
}

sub select_ids {
    my ($self, $filter, $attr, $min, $max) = @_;
    my $res = $self->ldap->search(base => $self->cfg->{base},
				  filter => $filter,
				  attrs => [ $attr ]);
    if ($res->code) {
	warn "$filter: ".$res->error;
	return undef
    }

    sort {$a <=> $b}
    grep { !((defined($min) && $_ < $min) || (defined($max) && $_ > $max)) }
    map {
	my $u = $_->get_value($attr, asref => 1);
	$u ? @$u : ()
    } $res->entries()
}

sub next_id {
    my ($self, $filter, $attr, $min, $max) = @_;
    $min ||= 1;
    my $u = [$self->select_ids($filter, $attr, $min, $max)];
    
    if (@$u) {
	my $next = pop @$u;
	if (!defined($max) || $next < $max) {
	    return $next + 1;
	}
	while (my $cur = pop @$u) {
	    if ($cur + 1 < $next) {
		return $cur + 1;
	    }
	    $next = $cur;
	}
	if ($next > $min) {
	    return $next - 1;
	}
	croak "no free IDs";
    }
    return $min;
}

sub uids {
    my $self = shift;
    $self->select_ids('(objectClass=posixAccount)', 'uidNumber', @_);
}

sub nextuid {
    my $self = shift;
    return $self->next_id('(objectClass=posixAccount)', 'uidNumber', @_);
}
    
sub gids {
    my $self = shift;
    $self->select_ids('(objectClass=posixGroup)', 'gidNumber', @_);
}

sub nextgid {
    my $self = shift;
    return $self->next_id('(objectClass=posixGroup)', 'gidNumber', @_);
}

sub user_names {
    my ($self,$gid) = @_;
    my $filter = defined($gid)
	           ? "(&(objectClass=posixAccount)(gidNumber=$gid))"
		   : '(objectClass=posixAccount)';
    my $attr = 'uid';
    my $res = $self->ldap->search(base => $self->cfg->{base},
				  filter => $filter,
				  attrs => [ $attr ]);
    if ($res->code) {
	warn "$filter: ".$res->error;
	return undef
    }

    map {
	my $u = $_->get_value($attr, asref => 1);
	$u ? @$u : ()
    } $res->entries()
}

my %getalias_attr = (
    cn => 'name',
    rfc822MailMember => 'mem'
);

sub getalias {
    my ($self, $name) = @_;
    (my $filter = $self->cfg->{savane_getalias_filter})
	=~ s/(?<!\\)(\$(?:name|arg))/$name/g;
    my $res = $self->ldap->search(base => $self->cfg->{base},
				  filter => $filter,
				  attrs => [keys %getalias_attr]);
    if ($res->code) {
	warn "$filter: ".$res->error;
	return undef
    }
	
    if (my $entry = ($res->entries())[0]) {
	return new Savane::LDAP::alias(dn => $entry->dn,
				       %{Savane::LDAP::Map
					     ->new(%getalias_attr)
					     ->map($entry)});
    }
}

sub chalias {
    my ($self, $ent) = @_;
    $self->change($ent, \%getalias_attr,
		  $self->cfg->{savane_alias_dn},
		  $self->cfg->{savane_alias_object});
}

1;

    

