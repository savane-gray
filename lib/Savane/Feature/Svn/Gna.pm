# GNA-specific SVN creation for Savane               -*- perl -*-
# Copyright (C) 2016  Sergey Poznyakoff <gray@gnu.org>
#
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package Savane::Feature::Svn::Gna;

use strict;
use Carp;

require Savane::Feature::Svn::Basic;
our @ISA = qw(Savane::Feature::Svn::Basic);

use Savane::Feature qw(:all);

our $description = 'GNA SVN layout';

sub build {
    my $self = shift;

    return $self->{status} unless $self->SUPER::build(@_);
    my $dir = $self->{directory};
    my $group = $self->{group};

    $self->run('svn', 'mkdir', '-q', '-m "default layout"',
	       "file://$dir/trunk");
    $self->run('svn', 'mkdir', '-q', '-m "default layout"',
	       "file://$dir/tags");
    $self->run('svn', 'mkdir', '-q', '-m "default layout"',
	       "file://$dir/branches");

    # hardcode svnmailer + ciabot support
    my $fd;
    if (open(my $fd, '>', "$dir/hooks/post-commit")) {
	print $fd <<EOT;
#!/bin/sh
# (obviously, svn-mailer and ciabot.sh must be in the relevant PATH)	
sv_extra_svn_postcommit_brigde -t \"\$1\" -r \"\$2\" -p \"$group->{unix_group_name}\"
svn-mailer -d \"\$1\" -r \"\$2\" -f/etc/svn-mailer.conf
ciabot.sh \"\$1\" \"\$2\" \"$group->{unix_group_name}\"
EOT
;
	close $fd;

	chmod 0755, "$dir/hooks", "$dir/hooks/post-commit";
    } else {
	$self->log(PRIO_ERROR, "cannot open $dir/hooks/post-commit: $!");
    }

    return $self->{status};
}    

    
    

    
