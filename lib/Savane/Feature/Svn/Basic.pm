# Basic SVN layout for Savane               -*- perl -*-
# Copyright (C) 2016  Sergey Poznyakoff <gray@gnu.org>
#
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package Savane::Feature::Svn::Basic;
use strict;
use Carp;

require Savane::Feature;
our @ISA = qw(Savane::Feature);

use Savane::Feature qw(:all);
use Savane::Backend qw(debug);
use File::Find;
use File::Basename qw(basename);
use Fcntl qw(:mode);

our $description = 'Basic SVN layout';

sub build {
    my $self = shift;

    return $self->{status} unless $self->SUPER::build(@_);
    my $dir = $self->{directory};
    my $group = $self->{group};

    if (-e $dir) {
	$self->{status} = FEATURE_UNCHANGED;
    } else {
	if ($self->run('svnadmin', 'create', '--fs-type', 'fsfs', $dir)) {
	    $self->created($dir);
	    $self->{status} = FEATURE_CREATED;
	    find(sub {
	             chown(-1, $group->{gidNumber}, $_)
			 or $self->log(PRIO_ERROR,
				       "${File::Find::name}: cannot chown: $!");
		     my (undef,undef,$mode) = lstat($_);
		     unless ($mode & S_IWGRP) {
			 chmod((($mode | S_IWGRP) & 0777), $_)
			     or $self->log(PRIO_ERROR,
					   "${File::Find::name}: cannot chmod: $!");
		     }
		 }, "$dir/db");
	}
    }
    return $self->{status};
}

sub backup {
    my $self = shift;
    my ($outdir) = @_;

    return $self->{status} unless $self->SUPER::backup(@_);
    my $group = $self->{group};

    my $rev;
    if ($self->run('svnlook', 'youngest', $self->{directory})) {
	$rev = pop @{$self->{log}[PRIO_INFO]};
    } else {
	return $self->{status} = FEATURE_FAIL;
    }

    debug("$group->{unix_group_name}: last revision $rev");

    my $target = "$outdir/$group->{unix_group_name}.$rev.dump.gz";
    if (-e $target) {
	$self->log(PRIO_INFO, "$target already exists");
	return $self->{status} = FEATURE_UNCHANGED;
    }

    $self->log(PRIO_INFO, "$group->{unix_group_name}: creating dumpfile $target");

    # get the list of old dumps to be removed later
    my @oldfiles = glob "$outdir/$group->{unix_group_name}.*.dump.gz";

    # build this one
    if ($self->run("svnadmin -q dump $self->{directory} | gzip > $target")) {
	$self->created($target);
	$self->{status} = FEATURE_CREATED;
	$self->symlink(basename($target),
		       "$group->{unix_group_name}.dump.gz", $outdir);
	unlink @oldfiles if (@oldfiles);
    } else {
	unlink $target if -e $target;
	$self->{status} = FEATURE_FAIL;
    }
    return $self->{status};
}
    
sub snapshot {
    my $self = shift;
    my ($outdir) = @_;

    return $self->{status} unless $self->SUPER::snapshot(@_);
    my $group = $self->{group};
    
    my $target = "$outdir/$group->{unix_group_name}-snapshot.tar.gz";
    return $self->{status} = FEATURE_UNCHANGED if -e $target;
    
    debug("$group->{unix_group_name}: creating snapshot $target from SVN");

    unless ($self->pushdir()) {
	return $self->{status} = FEATURE_FAIL;
    }

    if ($self->run("svn export -q file://$self->{directory}/trunk $group->{unix_group_name}")) {
	if ($self->run("/bin/tar --exclude-vcs -zhcf $target $group->{unix_group_name}")) {
	    $self->created($target);
	    $self->{status} = FEATURE_CREATED;
	} else {
	    $self->{status} = FEATURE_FAIL;
	}
    } else {
	$self->{status} = FEATURE_FAIL;
    }
    $self->popdir();
    return $self->{status};
}

				
