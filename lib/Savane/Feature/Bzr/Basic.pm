package Savane::Feature::Bzr::Basic;
use strict;
use Carp;

require Savane::Feature;
our @ISA = qw(Savane::Feature);

use File::Find;
use Savane::Feature qw(:all);
use Savane::Backend qw(debug);
use File::Basename;
use Fcntl qw(:mode);

our $description = 'Basic BZR layout';

sub build {
    my ($self) = @_;

    return $self->{status} unless $self->SUPER::build(@_);
    my $dir = $self->{directory};
    my $group = $self->{group};
    
    if (-e $dir) {
	$self->{status} = FEATURE_UNCHANGED;
    } else {
	unless ($self->run('bzr', 'init-repository', '--no-tree', '--quiet',
			   $dir)) {
	    return $self->{status} = FEATURE_FAIL;
	}
	$self->created($dir);
	$self->{status} = FEATURE_CREATED;
	$self->setup_privs(gid => $group->{gidNumber},
			   mode => S_IWGRP,
			   dirs => $dir);
    }

    return $self->{status};
}

sub backup {
    my $self = shift;
    my ($outdir) = @_;

    return $self->{status} unless $self->SUPER::backup(@_);
    my $group = $self->{group};

    my $rev;
    if ($self->run('bzr', 'revno', $self->{directory})) {
	$rev = pop @{$self->{log}[PRIO_INFO]};
    } else {
	return $self->{status} = FEATURE_FAIL;
    }

    debug("$group->{unix_group_name}: last revision $rev");

    my $target = "$outdir/$group->{unix_group_name}.$rev.dump.gz";

    if (-e $target) {
	$self->log(PRIO_INFO, "$target already exists");
	return $self->{status} = FEATURE_UNCHANGED;
    }

    $self->log(PRIO_INFO, "$group->{unix_group_name}: creating dumpfile $target");

    # get the list of old dumps to be removed later
    my @oldfiles = "$outdir/$group->{unix_group_name}.*.dump.gz";
    
    if ($self->run('/bin/tar', '-C', $self->{directory},
		   '-zhcf', $target, '.')) {
	$self->created($target);
	$self->{status} = FEATURE_CREATED;
    } else {
	unlink $target if -e $target;
	$self->{status} = FEATURE_FAIL;
    }
    return $self->{status};
}

sub snapshot {
    my $self = shift;
    my ($outdir) = @_;

    return $self->{status} unless $self->SUPER::snapshot(@_);
    my $group = $self->{group};

    my $target_base_name = basename($self->{directory});
    my $target = "$outdir/${target_base_name}-snapshot.tar.gz";

    return $self->{status} = FEATURE_UNCHANGED if -e $target;
    
    $self->log(PRIO_INFO, "$group->{unix_group_name}: creating snapshot $target");

    if ($self->run('bzr', 'export',
		   "--directory=$self->{directory}",
		   "--root=$group->{unix_group_name}",
		   '--format=tgz', $target)) {
	$self->created($target);
	$self->{status} = FEATURE_CREATED;
    } else {
	unlink $target if -e $target;
	$self->{status} = FEATURE_FAIL;
    }
    return $self->{status};
}
    
    
