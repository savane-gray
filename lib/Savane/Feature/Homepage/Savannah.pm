# Savannah-specific project homepage creation for Savane         -*- perl -*-
# Copyright (C) 2016  Sergey Poznyakoff <gray@gnu.org>
#
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package Savane::Feature::Homepage::Savannah;

use strict;
use Carp;

require Savane::Feature::Cvs::Savannah;
our @ISA = qw(Savane::Feature::Cvs::Savannah);

use Savane::Feature qw(:all);

our $description = 'Savannah-specific project homepage layout (CVS-based)';

sub new {
    my $class = shift;
    my $self = $class->SUPER::new(@_);
    $self->{repo_type} = 'web';
    return $self;
}

sub build {
    my $self = shift;

    return $self->{status} unless $self->SUPER::build(@_);
    my $dir = $self->{directory};
    my $group = $self->{group};
    
    # Allow modifications from group 'www' in the web module
    $self->run('setfacl',
	       '-m',         'group:www:rwx',
	       '-m', 'default:group:www:rwx',
	       "$dir/$group->{unix_group_name}");

    # Same for CVSROOT/history
    $self->run('setfacl',
	       '-m',         'group:www:rw',
	       "$dir/CVSROOT/history");

    # perform an initial checkout so that updates happen:
    # (will also be done at commit time now)
    $self->run('/usr/bin/curl',
	       'http://www.gnu.org/new-savannah-project/new.py',
	       '-s',
	       '-F', "type=gnu",
	       '-F', "project=$self->{unix_group_name}");
    return $self->{status};
}
   
