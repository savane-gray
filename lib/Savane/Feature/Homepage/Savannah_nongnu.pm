# Savannah (non-GNU) project homepage creation for Savane          -*- perl -*-
# Copyright (C) 2016  Sergey Poznyakoff <gray@gnu.org>
#
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package Savane::Feature::Homepage::Savannah_nongnu;

use strict;
use Carp;

require Savane::Feature::Cvs::Savannah;
our @ISA = qw(Savane::Feature::Cvs::Savannah);

use Savane::Feature qw(:all);
use Savane::Groups;

our $description = 'Savannah-specific (non-GNU) project homepage layout (CVS-based)';

sub new {
    my $class = shift;
    my $self = $class->SUPER::new(@_);
    $self->{repo_type} = 'web';
    return $self;
}

sub build {
    my $self = shift;

    return $self->{status} unless $self->SUPER::build(@_);
    my $dir = $self->{directory};
    my $group = $self->{group};
    
    my $ws_type;
    if ($group->{unix_group_name} eq 'www') {
	$ws_type = 'www';
    } elsif (GetGroupSettings($group->{unix_group_name}, 'type') == 6) {
	$ws_type = 'translations';
    } else {
	$ws_type = 'non-gnu';
    }

    # perform an initial checkout so that updates happen:
    # (will also be done at commit time now)
    $self->run('/usr/bin/curl',
	       'http://www.gnu.org/new-savannah-project/new.py',
	       '-s',
	       '-F', "type=$ws_type",
	       '-F', "project=$self->{unix_group_name}");
    return $self->{status};
}
   
