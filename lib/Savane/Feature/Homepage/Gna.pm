# GNA-specific project homepage layout for Savane               -*- perl -*-
# Copyright (C) 2016  Sergey Poznyakoff <gray@gnu.org>
#
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package Savane::Feature::Homepage::Gna;

use strict;
use Carp;

require Savane::Feature::Svn::Basic;
our @ISA = qw(Savane::Feature::Svn::Basic);

use Savane::Feature qw(:all);

our $description = 'GNA-specific project homepage layout (SVN-based)';

sub check_dir {
    my $self = shift;
    my $svndir = shift;
    my $dir = shift;
    if (open(my $fd, '-|', "svnlook tree $svndir /$dir 2>/dev/null")) {
	my $s = <$fd>;
	chomp $s;
	close $fd;
	return $s eq "$dir/";
    } else {
	$self->log(PRIO_ERROR, "can't run svnlook: $!");
    }
    return undef;
}

sub build {
    my $self = shift;

    return $self->{status} unless $self->SUPER::build(@_);
    my $svndir = $self->{directory};
    my $group = $self->{group};
    
    if ($self->check_dir($svndir, 'website')) {
	$self->{status} = FEATURE_UNCHANGED;
    } elsif ($self->run('svn', 'mkdir', '-q', '-m "website at home.gna.org"',
		       "file://$svndir/website")) {
	$self->created("file://$svndir/website");
	$self->{status} = FEATURE_CREATED;
    } else {
	$self->{status} = FEATURE_FAIL;
    }

    return $self->{status};
}
    

    
    
