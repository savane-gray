# Basic Mercurial layout for Savane               -*- perl -*-
# Copyright (C) 2016  Sergey Poznyakoff <gray@gnu.org>
#
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package Savane::Feature::Hg::Basic;
use strict;
use Carp;

require Savane::Feature;
our @ISA = qw(Savane::Feature);

use Savane::Feature qw(:all);
use Savane::Conf;
use File::Basename;
use Fcntl qw(:mode);

our $description = 'Basic Mercurial layout';

sub build {
    my $self = shift;

    return $self->{status} unless $self->SUPER::build(@_);
    my $dir = $self->{directory};
    my $group = $self->{group};
    return $self->{status} = FEATURE_UNCHANGED if -e $dir;

    my $old_umask = umask(0002);
    if ($self->run('hg', 'init', $dir)) {
	$self->created($dir);
	$self->{status} = FEATURE_CREATED;
    } else {
	return $self->{status} = FEATURE_FAIL;
    }

    my (undef,undef,$mode) = lstat $dir;
    chmod($mode | S_ISGID, $dir)
	or $self->log(PRIO_ERROR, "can't chmod $dir: $!");

    my $gid;
    if ((undef,undef,$gid) = getgrnam($group->{unix_group_name})) {
	$self->setup_privs(dirs => $dir, gid => $gid);
    } else {
	$self->log(PRIO_ERROR, "can't get GID of $group->{unix_group_name}");
    }

    if (open(my $fd, '>', "$dir/.hg/hgrc")) {
	print $fd <<EOT;
#<savane>
[web]
contact =
description = $group->{unix_group_name}
#</savane>
EOT
;
        if (defined($gid)) {
	    chown(-1, $gid, $fd)
		or $self->log(PRIO_ERROR, "can't chown $dir/.hg/hgrc: $!");
	}
	close $fd;
    }

    umask($old_umask);

    return $self->{status};
}

sub id {
    my ($self) = @_;

    if ($self->run('hg', 'log',
		   '-R', $self->{directory},
		   '--limit', '1',
		   '--template', '{node|short}')) {
	return pop @{$self->{log}[PRIO_INFO]};
    } else {
	$self->{status} = FEATURE_FAIL;
    }
    return undef;
}

sub backup {
    my $self = shift;
    my ($outdir) = @_;

    return $self->{status} unless $self->SUPER::backup(@_);
    my $group = $self->{group};
    
    my $id = $self->id();
    return $self->{status} unless defined $id;

    my $target_base_name = basename($self->{directory});
    my $target = "$outdir/$target_base_name.$id.tar.gz";

    return $self->{status} = FEATURE_UNCHANGED if -e $target;

    $self->log(PRIO_INFO, "$group->{unix_group_name}: creating dumpfile $target");

    # Get the list of old archives to be removed on success
    my @oldfiles = glob "$outdir/$target_base_name.*.tar.gz";

    if ($self->run('tar', '-P',
		   '--transform',
		   "s|^$self->{directory}|$self->{group}{unix_group_name}|",
		   '-zhcf', $target,
		   $self->{directory})) {
	$self->created($target);
	$self->{status} = FEATURE_CREATED;
	$self->symlink(basename($target),
		       "${target_base_name}-latest.tar.gz", $outdir);
	unlink @oldfiles if (@oldfiles);
    } else {
	unlink $target if -e $target;
	$self->{status} = FEATURE_FAIL;
    }
    return $self->{status};
}

sub snapshot {
    my $self = shift;
    my ($outdir) = @_;

    return $self->{status} unless $self->SUPER::snapshot(@_);
    my $group = $self->{group};

    my $id = $self->id();
    return $self->{status} unless defined $id;

    my $target_base_name = basename($self->{directory});
    my $target = "$outdir/${target_base_name}-snapshot.tar.gz";

    return $self->{status} = FEATURE_UNCHANGED if -e $target;
    
    $self->log(PRIO_INFO, "$group->{unix_group_name}: creating snapshot $target");

    if ($self->run('hg', 'archive',
		   '-R', $self->{directory},
		   '--prefix', $target_base_name,
		   '--rev', $id,
		   '--type', 'tgz',
		   $target)) {
	$self->created($target);
	$self->{status} = FEATURE_CREATED;
    } else {
	unlink $target if -e $target;
	$self->{status} = FEATURE_FAIL;
    }
    return $self->{status};
}
	




	
