# External download directory layout for Savane               -*- perl -*-
# Copyright (C) 2017 Sergey Poznyakoff <gray@gnu.org>
#
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package Savane::Feature::Download::External;
use strict;

require Savane::Feature;
our @ISA = qw(Savane::Feature);

use Savane::Feature qw(:all);
use Savane::Conf qw(GetConf);

our $description = 'External download directory';

# External download directory is located somewhere on the remote server,
# which is responsible for its creation and maintenance.  Thus, this 
# download type will never attempt to create it.  It is best suited for
# mirroring projects from another forge.

sub new {
    my ($class, $feature, $ref, @args) = @_;
    my $savedir;
    unless ($ref->{feature}{$feature}{dir} =~ /%PROJECT/) {
	$savedir = $ref->{feature}{$feature}{dir};
	$ref->{feature}{$feature}{dir} = '/tmp/%PROJECT';
    }
    my $self = $class->SUPER::new($feature, $ref, @args);
    $ref->{feature}{$feature}{dir} = $savedir
	if defined $savedir;
    return $self;
}

1;
