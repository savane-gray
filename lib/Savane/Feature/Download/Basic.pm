# Basic download directory layout for Savane               -*- perl -*-
# Copyright (C) 2016  Sergey Poznyakoff <gray@gnu.org>
#
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package Savane::Feature::Download::Basic;
use strict;

require Savane::Feature;
our @ISA = qw(Savane::Feature);

use File::Path qw(make_path);
use Carp;
use Savane::Feature qw(:all);
use Savane::Conf qw(GetConf);

our $description = 'Basic download directory';

sub get_owner {
    my $self = shift;
    Savane::Conf::GetConf('download.owner')
}

sub build {
    my $self = shift;

    return $self->{status} unless $self->SUPER::build(@_);
    my $dir = $self->{directory};
    my $group = $self->{group};
    if (-e $dir) {
	$self->{status} = FEATURE_UNCHANGED;
    } else {
	my %opts;
	$opts{mode} = $group->{is_public} ? 02775 : 02770;
	if (my $owner = $self->get_owner) {
	    $opts{owner} = $owner;
	}
	$opts{group} = $group->{unix_group_name};
	my $err;
	$opts{error} = \$err;
	make_path($dir, \%opts);
	if (@$err) {
	    for my $diag (@$err) {
		my ($file, $message) = %$diag;
		if ($file eq '') {
		    $self->log(PRIO_ERROR, $message);
		} else {
		    $self->log(PRIO_ERROR, "$file: $message");
		}
	    }
	    $self->{status} = FEATURE_FAIL;
	} else {
	    $self->created($dir);
	    $self->{status} = FEATURE_CREATED;
	}
    }
    return $self->{status};
}
    
1;

