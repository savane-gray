# Basic CVS layout for Savane               -*- perl -*-
# Copyright (C) 2016, 2019 Sergey Poznyakoff <gray@gnu.org>
#
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package Savane::Feature::Cvs::Basic;
use strict;
use Carp;

require Savane::Feature;
our @ISA = qw(Savane::Feature);

use Savane::Feature qw(:all);
use Savane::Conf;
use Savane::Backend qw(debug);
use File::Path qw(make_path);
use File::stat;
use Cwd;

our $description = 'Basic CVS layout';

sub new {
    my $class = shift;
    my $self = $class->SUPER::new(@_);
    $self->{repo_type} = 'sources';
    return $self;
}

sub set_file_access {
    my ($self, $file, $gid, $mode) = @_;
    chown -1, $gid, $file
	or do {
	    $self->log(PRIO_ERROR, "$file: cannot chown: $!");
	    return;
    };
    chmod $mode, $file
	or $self->log(PRIO_ERROR, "$file: cannot chmod: $!");
}

sub edit {
    my ($self, $file) = @_;

    confess "attempting to edit $file, while $self->{file_name} is being edited"
	if exists $self->{file_name};
    my $fd;
    unless (open($fd, '>', $file)) {
	$self->log(PRIO_ERROR, "$file: can't open for writing: $!");
	return 0;
    }
    $self->{file_name} = $file;
    $self->{file_descr} = $fd;
    print $fd "#<savane>\n";
    return 1;
}

sub append {
    my $self = shift;
    confess "no file open for editing" unless exists $self->{file_descr};
    foreach my $text (@_) {
	my $fd = $self->{file_descr};
	print $fd $text;
    }
}

sub checkin {
    my ($self, $message) = @_;
    confess "no file open for editing" unless exists $self->{file_descr};
    my $file = $self->{file_name};
    $self->commit();
    $self->run('rcs', '-q', '-U', $file) or return 0;
    $self->run('ci', '-q', "-m\"$message\"", $file) or return 0;
    $self->run('co', '-q', $file) or return 0;
    return 1;
}

sub commit {
    my ($self) = @_;
    confess "no file open for editing" unless exists $self->{file_descr};
    my $fd = $self->{file_descr};
    print $fd "#</savane>\n";
    close $self->{file_descr};
    delete $self->{file_descr};
    delete $self->{file_name};
}    

sub lockdir {
    my ($self) = @_;
    return '/var/lock/cvs/' . $self->{group}{unix_group_name}; #FIXME
}

sub build {
    my $self = shift;

    return $self->{status} if $self->SUPER::build(@_) == FEATURE_FAIL;
    return $self->{status} if $self->make_repo() == FEATURE_FAIL;
    my $lockdir = $self->lockdir;
    unless (-d $lockdir) {
	# build the locks
	$self->make_dir($lockdir);
	$self->set_file_access($lockdir, $self->{group}{gidNumber}, 0777);
    }
    return $self->{status};
}

sub make_repo {
    my $self = shift;

    my $dir = $self->{directory};
    my $group = $self->{group};

    return $self->{status} = FEATURE_UNCHANGED if -e $dir;
    
    return $self->{status} = FEATURE_FAIL
	unless $self->make_dir($dir,
			       mode => $group->{is_public} ? 02775 : 02770);

    return $self->{status} = FEATURE_FAIL
	unless $self->run('cvs', '-d', $dir, 'init');
    $self->created("cvs $dir");
    $self->{status} = FEATURE_CREATED;
    my $lockdir = $self->lockdir;   
    if ($self->edit("$dir/CVSROOT/config")) {
	$self->append(<<EOT
SystemAuth=yes
LockDir=$lockdir
LogHistory=AMRT
EOT
	    );
	$self->checkin("added by " . __PACKAGE__);
    }
    
    if ($self->edit("$dir/CVSROOT/commitinfo")) {
	$self->append(<<EOT
$group->{unix_group_name}  /bin/true
DEFAULT /bin/false
EOT
	    );
	$self->checkin("added by " . __PACKAGE__);
    }
    
    if (exists($self->{repo_type})) {
	if (my $var = GetConf("$self->{repo_type}.oncommit.cvs")) { 
	    if ($self->edit("$dir/CVSROOT/loginfo")) {
		$self->append("ALL $var\n");
		$self->checkin("added by " . __PACKAGE__);
	    }
	}
    }

    my $gid = $group->{gidNumber};
    
    if ($self->edit("$dir/CVSROOT/val-tags")) {
	$self->commit();
	$self->set_file_access("$dir/CVSROOT/val-tags",  $gid, 0664);
    }
    
    $self->make_dir("$dir/$group->{unix_group_name}", mode => 02775);
    $self->setup_privs(dirs => $dir, gid => $gid);

    # Make the CVSROOT ro for anybody.  Doing otherwise is a major
    # security hole.
    $self->setup_privs(dirs => "$dir/CVSROOT", uid => 0, gid => 0);

    # history must be group writable
    $self->set_file_access("$dir/CVSROOT/history", $gid, 0664);

    return $self->{status};
}    

sub file_mtime {
    my ($self, $path) = @_;
    my $st = stat($path);
    return $st->mtime;
}

sub file_newer {
    my ($self, $a, $b) = @_;
    return $self->file_mtime($a) > $self->file_mtime($b);
}

sub grep {
    my ($self, $rx, $file) = @_;
    
    if (open(my $fd, '<', $file)) {
	my @ret;
	while (<$fd>) {
	    chomp;
	    push @ret, $_ if /$rx/;
	}
	return @ret if (@ret);
    } else {
	$self->log(PRIO_ERROR, "can't open $file: $!");
    }
    
    return undef;
}

sub find_newer {
    my ($self, $dir, $file) = @_;

    my $time = $self->file_mtime($file);
    my $found;
    eval {
	find(sub {
	        if ($self->file_mtime($_) > $time) {
		    $found = $File::Find::name;
		    die;
		}
	     }, 
	     $dir);
    };
    return $found;
}

sub backup_needed {
    my ($self, $group_name, $backup) = @_;
    
    unless (-f $backup) {
	debug("$group_name: there is no backup yet");
	return 1;
    }

    my $dir = $self->{directory};

    if (-f "$dir/CVSROOT/history") {
	# Use the history file to figure out if we need to backup
	# the tree.
	#
	my $loghistory;
	if (-f "$dir/CVSROOT/config") {
	    ($loghistory) = $self->grep(qr{^\s*LogHistory},
					"$dir/CVSROOT/config");
	    #	T	"Tag" cmd.
	    #	O	"Checkout" cmd.
	    #   E       "Export" cmd.
	    #	F	"Release" cmd.
	    #	W	"Update" cmd - No User file, Remove from Entries file.
	    #	U	"Update" cmd - File was checked out over User file.
	    #	G	"Update" cmd - File was merged successfully.
	    #	C	"Update" cmd - File was merged and shows overlaps.
	    #	M	"Commit" cmd - "Modified" file.
	    #	A	"Commit" cmd - "Added" file.
	    #	R	"Commit" cmd - "Removed" file.
	    #
	    if ($loghistory) {
		debug("$group_name: loghistory contains $loghistory");
		$loghistory =~ s/^.*=//;
	    }
	}
	if ($loghistory && $loghistory !~ /[OEFWUGC]/) {
	    # If read-only events are not logged, we can rely on its
	    # modification time.
	    #
	    debug("$group_name: history file only logs RW events, rely on history file modification time");
	    return $self->file_newer("$dir/CVSROOT/history", $backup);
	} else {
	    # Get the date of the last read-write event from the content
	    # of the history file.
	    my ($lastrw) = $self->grep(qr{^[TMAR]}, "$dir/CVSROOT/history");
	    if ($lastrw) {
		$lastrw = hex(substr($lastrw, 1, 8));
		my $modtime = $self->file_mtime($backup);
		debug("$group_name: tarball is dated "
		      . localtime($modtime)
		      . " and the last history event "
		      . localtime($lastrw));
		if ($lastrw > $modtime) {
		    debug("$group_name: last RW history event more recent than backup");
		    return 1;
		} else {
		    debug("$group_name: no need to backup according to last RW history event");
		    return 0;
		}
	    } else {
		# No last RW event, cannot say nothing, maybe history
		# file was reset by hand or something...
	    }
	}
    }

    # Do it the hard way: walk the tree until we find a file
    # that is more recent than the backup.
    if (my $file = $self->find_newer("$dir/$group_name", $backup)) {
	debug("$group_name: the tree contains a file ($file) that is newer than the backup $backup");
	return 1;
    }
    debug("$group_name: the tree does not contain a file newer than the backup");

    return 0;
}

sub backup {
    my $self = shift;
    my ($outdir) = @_;

    return $self->{status} unless $self->SUPER::backup(@_);
    my $group = $self->{group};
    
    my $backup = "$outdir/$group->{unix_group_name}.tar.gz";
    unless ($self->backup_needed($group->{unix_group_name}, $backup)) {
	debug("no need to backup $group->{unix_group_name}");
	return $self->{status} = FEATURE_UNCHANGED;
    }

    my $dir = $self->{directory};
    debug("$group->{unix_group_name}: backing up");

    if ($self->run('/bin/tar', '-C', $dir, '-zhcf', $backup,
		   $group->{unix_group_name})) {
	$self->created($backup);
	$self->{status} = FEATURE_CREATED;
    } else {
	unlink $backup if -e $backup;
	$self->{status} = FEATURE_FAIL;
    }
    return $self->{status};
}

sub snapshot {
    my $self = shift;
    my ($outdir) = @_;

    return $self->{status} unless $self->SUPER::snapshot(@_);
    my $group = $self->{group};
    
    my $snapshot = "$outdir/$group->{unix_group_name}-snapshot.tar.gz";

    return $self->{status} = FEATURE_UNCHANGED if -e $snapshot;
    
    debug("$group->{unix_group_name}: creating snapshot from CVS");

    unless ($self->pushdir()) {
	return $self->{status} = FEATURE_FAIL;
    }

    if ($self->run('cvs', '-Q', '-d', $self->{directory},
		   'co', $group->{unix_group_name})) {
	if ($self->run('/bin/tar',
		       '--exclude-vcs',
		       '-zhcf', $snapshot,
		       $group->{unix_group_name})) {
	    $self->created($snapshot);
	    $self->{status} = FEATURE_CREATED;
	} else {
	    unlink $snapshot if -e $snapshot;
	    $self->{status} = FEATURE_FAIL;
	}
    } else {
	$self->{status} = FEATURE_FAIL;
    }
    $self->popdir();
    return $self->{status};
}
