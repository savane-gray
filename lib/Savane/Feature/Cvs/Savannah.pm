# Savannah-specific CVS layout for Savane               -*- perl -*-
# Copyright (C) 2016  Sergey Poznyakoff <gray@gnu.org>
#
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package Savane::Feature::Cvs::Savannah;
use strict;
use Carp;

require Savane::Feature::Cvs::Basic;
our @ISA = qw(Savane::Feature::Cvs::Basic);
use Savane::Feature qw(:all);

our $description = 'CVS layout for Savannah';

sub build {
    my ($self) = @_;

    return $self->{status} unless $self->Savane::Feature::build(@_);
    my $dir = $self->{directory};
    my $group = $self->{group};
    return $self->{status} = FEATURE_UNCHANGED if -e $dir;
    return $self->{status} = FEATURE_FAIL
	unless $self->make_dir($dir, mode => $group->{is_public} ? 02775 : 02770);

    return $self->{status} = FEATURE_FAIL
	unless $self->run('cvs', '-d', $dir, 'init');
    $self->created("cvs $dir");
    $self->{status} = FEATURE_CREATED;
    
    my $repo_type = exists($self->{repo_type}) ? $self->{repo_type} : 'sources';

    my (undef,undef,$gid) = getgrnam($group->{unix_group_name});
    my $lockdir = "/var/lock/cvs/$repo_type/$group->{unix_group_name}";
    if ($self->edit("$dir/CVSROOT/config")) {
	$self->append(<<EOT
# Set this to "no" if pserver shouldn't check system users/passwords
SystemAuth=no
# Put CVS lock files in this directory rather than directly in the repository.
LockDir=$lockdir
# Cut down on disk I/O by only logging write operations
LogHistory=TMAR
# Set `UserAdminOptions' to the list of `cvs admin' commands (options)
# that users not in the `cvsadmin' group are allowed to run.  This
# defaults to `k', or only allowing the changing of the default
# keyword expansion mode for files for users not in the `cvsadmin' group.
# This value is ignored if the `cvsadmin' group does not exist.
#
# The following string would enable all `cvs admin' commands for all
# users:
#UserAdminOptions=aAbceIklLmnNostuU
UserAdminOptions=k
# Set `UseNewInfoFmtStrings' to `no' if you must support a legacy system by
# enabling the deprecated old style info file command line format strings.
# Be warned that these strings could be disabled in any new version of CVS.
UseNewInfoFmtStrings=yes
EOT
	    );
	$self->checkin("added by " . __PACKAGE__);
	$self->set_file_access("$dir/CVSROOT/config", $gid, 0644);
    }
    
    if ($self->edit("$dir/CVSROOT/passwd")) {
	$self->append(<<EOT
anonymous::nobody
anoncvs::nobody
EOT
	    );
	$self->commit;
	$self->set_file_access("$dir/CVSROOT/passwd", $gid, 0644);
    }
    
    if ($self->edit("$dir/CVSROOT/readers")) {
	$self->append("anonymous\n");
	$self->append("anoncvs\n");
	$self->commit;
	$self->set_file_access("$dir/CVSROOT/readers", $gid, 0644);
    }
    
    if ($self->edit("$dir/CVSROOT/commitinfo")) {
	$self->checkin("added by " . __PACKAGE__);
    }
    
    if ($self->edit("$dir/CVSROOT/loginfo")) {
	$self->checkin("added by " . __PACKAGE__);
    }
    
    if ($self->edit("$dir/CVSROOT/writers")) {
	$self->commit();
	$self->set_file_access("$dir/CVSROOT/writers", $gid, 0644);
    }

    if ($self->edit("$dir/CVSROOT/val-tags")) {
	$self->commit();
	$self->set_file_access("$dir/CVSROOT/val-tags", $gid, 0664);
    }
    # history must be group writable
    $self->set_file_access("$dir/CVSROOT/history", $gid, 0664);

    if ($self->make_dir("$dir/$group->{unix_group_name}",
			mode => 02775)) {
	$self->setup_privs(dirs => $dir, gid => $gid);
	
	# Make the CVSROOT ro for anybody.  Doing otherwise is a major
	# security hole.
	$self->setup_privs(dirs => "$dir/CVSROOT", uid => 0, gid => 0);
	$self->run('chattr', '+i', "$dir/CVSROOT");
    }
    
    # build the locks
    $self->make_dir($lockdir);
    $self->set_file_access($lockdir, $gid, 0777);

    return $self->{status};
}    
    
    
		  
