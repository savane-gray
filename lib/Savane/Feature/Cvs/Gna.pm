# GNA-specific CVS layout for Savane               -*- perl -*-
# Copyright (C) 2016  Sergey Poznyakoff <gray@gnu.org>
#
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package Savane::Feature::Cvs::Gna;
use strict;
use Carp;

require Savane::Feature::Cvs::Basic;
our @ISA = qw(Savane::Feature::Cvs::Basic);
use Savane::Feature qw(:all);

our $description = 'CVS layout for GNA';

## Make a cvs area at gn!elsewhere.
## Ask yeupou@gna.org before modifying this function
sub build {
    my ($self) = @_;

    return $self->{status} unless $self->SUPER::build(@_);
    my $dir = $self->{directory};
    my $group = $self->{group};
    # hardcode cvsreport support
    $self->edit("$dir/CVSROOT/commitinfo");
    $self->append("ALL\tcvsreport -e 'mail text+html $group->{unix_group_name}-commits'\n");
    $self->checkin("added by " . __PACKAGE__);
    return $self->{status};
}
