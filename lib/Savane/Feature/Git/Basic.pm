# Basic GIT layout for Savane               -*- perl -*-
# Copyright (C) 2016  Sergey Poznyakoff <gray@gnu.org>
#
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package Savane::Feature::Git::Basic;
use strict;
use Carp;

require Savane::Feature;
our @ISA = qw(Savane::Feature);

use Savane::Feature qw(:all);
use Savane::Conf;
use Savane::DB;
use File::Basename;
use Fcntl qw(:mode);

our $description = 'Basic Git layout';

sub write_file {
    my $self = shift;
    my $name = shift;
    my $file = "$self->{directory}/$name";
    local %_ = @_;
    my $text = shift;
    if (open(my $fd, '>', $file)) {
	print $fd $_{text} if exists $_{text};
	if (exists($_{mode})) {
	    my (undef,undef,$mode) = lstat $fd;
	    chmod($mode | $_{mode}, $fd)
		  or $self->log(PRIO_ERROR, "can't chmod $file: $!");
	}
	close $fd;
	$self->created($file) if $_{report};
	return 1;
    } else {
	$self->log(PRIO_ERROR, "can't open $file for writing: $!");
	return 0;
    }
}

sub make_post_update {
    my $self = shift;
    $self->write_file("hooks/post-update",
		      mode => S_IXUSR|S_IXGRP|S_IXOTH,
		      text => <<EOT
#!/bin/sh
exec git update-server-info
EOT
	);
}

sub make_post_receive {
    my $self = shift;
    
    if (exists($self->{repo_type})) {
	if (my $var = GetConf("$self->{repo_type}.oncommit.git")) {
	    $self->write_file("hooks/post-receive",
			      mode => S_IXUSR|S_IXGRP|S_IXOTH,
			      text => <<EOT
#!/bin/sh
umask 002
$var "\$@"
EOT
		);
	}
    }
}

sub config {
    my $self = shift;
    $self->run('git', 'config', @_);
}

sub build {
    my $self = shift;

    return $self->{status} unless $self->SUPER::build(@_);
    my $dir = $self->{directory};
    my $group = $self->{group};
    unless (-e $dir) {
	return $self->{status} = FEATURE_FAIL
	    unless $self->make_dir(dirname($dir));

	$ENV{'GIT_DIR'} = $dir;
	if ($self->run('git', 'init', '--shared=all')) {
	    $self->created($dir);
	    $self->{status} = FEATURE_CREATED;
	} else {
	    return $self->{status} = FEATURE_FAIL;
	}
	
	if (my (undef,undef,$gid) = getgrnam($group->{unix_group_name})) {
	    $self->setup_privs(dirs => $dir, gid => $gid);
	} else {
	    $self->log(PRIO_ERROR, "can't get GID of $group->{unix_group_name}");
	}

	# needed to make the repo accessible via bare HTTP
	$self->make_post_update();
	$self->make_post_receive();

	# forbid access to hooks
	$self->setup_privs(dirs => "$dir/hooks", uid => 0, gid => 0);
	$self->run('chattr', '+i', "$dir/hooks");
	# 'git-cvsserver' support
	$self->config('gitcvs.pserver.enabled', 1);
	$self->config('gitcvs.ext.enabled', 0);
	$self->config('gitcvs.dbname', '%G/gitcvs-db/sqlite');
	
	my $sqlite_dir = "$dir/gitcvs-db";
	$self->make_dir($sqlite_dir,
			mode => 0755,
			owner => 'nobody');

	delete $ENV{'GIT_DIR'};
    } else {
	# $self->log(PRIO_WARNING, "$dir already exists")
	#     unless (exists($group->{repo_short_description}) or
	# 	     exists($group->{repo_long_description}));
	$self->{status} = FEATURE_UNCHANGED;
    }

    $self->{repo_id} = $group->{repo_id} if exists $group->{repo_id};
    if (exists($group->{repo_short_description})) {
	if ($self->write_file('description',
			      text => $group->{repo_short_description},
			      report => 1)
	    and $self->{status} == FEATURE_UNCHANGED) {
	    $self->{status} = FEATURE_UPDATED;
	}
    }
	    
    if (exists($group->{repo_long_description})) {
	if ($self->write_file('README.html',
			      text => $group->{repo_long_description},
			      report => 1)
	    and $self->{status} == FEATURE_UNCHANGED) {
	    $self->{status} = FEATURE_UPDATED;
	}
    }
    
    $self->{group_id} = $group->{group_id};
    $self->{group_name} = $group->{unix_group_name};
    
    return $self->{status};
}

sub updatedb_after_create {
    my ($self) = @_;
    my @admin;
    my $owner;

    db_foreach(sub { push @admin, $_[0]->{name} },
	       qq{SELECT user.realname as name
		    FROM user, user_group
		   WHERE user_group.group_id=?
		     AND user.user_id = user_group.user_id
 		     AND user_group.admin_flags='A'},
	       $self->{group_id});
    if ($#admin == -1) {
	$owner = "The $self->{unix_group_name} team";
    } elsif ($#admin == 0) {
	$owner = $admin[0];
    } elsif ($#admin == 1) {
	$owner = "$admin[0] and $admin[1]";
    } else {
	$owner = join(@admin[0..2],', ');
	$owner .= ', et al.' if $#admin > 2;
    }
    db_modify(qq{INSERT INTO git_repo
                 (master,updated,description,name,group_id,owner)
                 VALUES ('Y',now(),?,?,?,?)
                 ON DUPLICATE KEY UPDATE synchronized=0},
	      'Authors did not supply a description yet',
	      $self->{group_name},
	      $self->{group_id},
	      $owner);
}
    
sub updatedb_after_update {
    my ($self) = @_;

    SetDBSettings("git_repo",
		  "repo_id=?",
		  "synchronized=now()", $self->{repo_id})
	if exists $self->{repo_id};
}

sub updatedb {
    my ($self) = @_;
    if ($self->{status} == FEATURE_CREATED) {
	$self->updatedb_after_create(@_);
    } elsif ($self->{status} == FEATURE_UPDATED) {
	$self->updatedb_after_update(@_);
    }
}

sub git_id {
    my ($self) = @_;

    if ($self->run("git --git-dir=$self->{directory} rev-parse --short HEAD")) {
	return pop @{$self->{log}[PRIO_INFO]};
    } elsif (${$self->{log}[PRIO_WARNING]}[-1] =~ /Needed a single revision/) {
	pop @{$self->{log}[PRIO_WARNING]};
	pop @{$self->{log}[PRIO_ERROR]};
	$self->log(PRIO_INFO, "$self->{group}{unix_group_name}: Git repository is empty");
	$self->{status} = FEATURE_UNCHANGED;
    } else {
	$self->{status} = FEATURE_FAIL;
    }
    return undef;
}

sub backup {
    my $self = shift;
    my ($outdir) = @_;

    return $self->{status} unless $self->SUPER::backup(@_);
    my $group = $self->{group};
    
    my $id = $self->git_id();
    return $self->{status} unless defined $id;

    my $target_base_name = basename($self->{directory}, '.git');
    my $target = "$outdir/$target_base_name.$id.tar.gz";

    return $self->{status} = FEATURE_UNCHANGED if -e $target;

    $self->log(PRIO_INFO, "$group->{unix_group_name}: creating dumpfile $target");

    # Get the list of old archives to be removed on success
    my @oldfiles = glob "$outdir/$target_base_name.*.tar.gz";

    if ($self->run('tar', '-P',
		   '--transform',
		   "s|^$self->{directory}|$group->{unix_group_name}.git|",
		   '-zhcf', $target, $self->{directory})) {
	$self->created($target);
	$self->{status} = FEATURE_CREATED;
	$self->symlink(basename($target),
		       "${target_base_name}-latest.tar.gz", $outdir);
	unlink @oldfiles if (@oldfiles);
    } else {
	unlink $target if -e $target;
	$self->{status} = FEATURE_FAIL;
    }
    return $self->{status};
}

sub snapshot {
    my $self = shift;
    my ($outdir) = @_;

    return $self->{status} unless $self->SUPER::snapshot(@_);
    my $group = $self->{group};

    my $target_base_name = basename($self->{directory}, '.git');
    my $target = "$outdir/${target_base_name}-snapshot.tar.gz";

    return $self->{status} = FEATURE_UNCHANGED if -e $target;
    
    $self->log(PRIO_INFO, "$group->{unix_group_name}: creating snapshot $target");

    my $id = $self->git_id();
    return $self->{status} unless defined $id;
    
    if ($self->run("git archive --remote=file://$self->{directory} --format=tar --prefix=$target_base_name/ master | gzip - > $target")) {
	$self->created($target);
	$self->{status} = FEATURE_CREATED;
    } else {
	unlink $target if -e $target;
	$self->{status} = FEATURE_FAIL;
    }
    return $self->{status};
}

    


