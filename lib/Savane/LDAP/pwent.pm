package Savane::LDAP::pwent;
use strict;
use warnings;
use Carp;
use Savane::LDAP::Base qw(name passwd:ro uid gid gecos dir shell cn sshpubkey:ar);
use overload
    '""' => sub { shift->as_string };

1;
