package Savane::LDAP::Config;
use strict;
use warnings;
use Carp;

sub new {
    my ($class, %opt) = @_;
    my $self = bless {}, $class;
    @{$self}{map {lc} keys %opt} = values %opt;
    @{$self->{_dfl}}{map {lc} keys %opt} = (1) x keys %opt;
    return $self;
}

sub as_string {
    my $self = shift;
    join("\n",
	 map {
	     if (/^_/ || !defined($self->{$_})) {
		 ()
	     } elsif (ref($self->{$_}) eq 'ARRAY') {
		 my $k = $_;
		 map { "$k $_" } @{$self->{$_}}
	     } else {
		 "$_ $self->{$_}"
	     }
	 } sort keys %$self)
}

use overload
    '""' => sub { shift->as_string };

sub read {
    my ($self, $config_file) = @_;
    my $file;
    my $line = 0;

    open($file, "<", $config_file)
	or croak("cannot open $config_file: $!");
    while (<$file>) {
	++$line;
	chomp;
	s/^\s+//;
	s/\s+$//;
        s/#.*//;
	next if ($_ eq "");
	my ($k,$v) = split(/\s+/, $_, 2);
	$k = lc $k;
	if (exists($self->{$k}) && !$self->{_dfl}{$k}) {
	    $self->{$k} = [ $self->{$k} ] unless ref($self->{$k}) eq 'ARRAY';
	    push @{$self->{$k}}, $v
	} else {
	    $self->{$k} = $v;
	    delete $self->{_dfl}{$k}
        }
    }
    close($file);
}

sub store {
    my ($self, $config_file) = @_;
    open(my $file, ">", $config_file)
	or croak("cannot open $config_file: $!");
    print $file "$self\n";
    close $file;
}

1;
