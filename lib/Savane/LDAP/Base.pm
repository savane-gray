package Savane::LDAP::Base;
use strict;
use warnings;
use Carp;
use parent 'Exporter';
no strict 'refs';

sub import {
    my $pkg = shift;            # package
    my ($package, $filename, $line) = caller;
    * { $package . '::new' } = sub {
	my ($class, %ini) = @_;
	my $self = bless {}, $class;
	if ($self->can('passwd')) {
	    $self->{passwd} = 'x';
	}
	while (my ($k,$v) = each %ini) {
	    $self->${ \$k }($v);
	}
	delete $self->{_orig} if $self->dn;
	$self;
    };
    * { $package . '::changed' } = sub { exists shift->{_orig} };
    * { $package . '::changeset' } = sub {
	my $self = shift;
	my %r;
	if ($self->changed) {
	    while (my ($a,$v) = each %{$self->{_orig}}) {
		if (defined($v)) {
		    if ($self->{$a}) {
			$r{replace}{$a} = $self->${\$a};
		    } else {
			push @{$r{delete}}, $a
		    }
		} elsif (my $cur = $self->${\$a}) {
		    if (ref($cur) eq 'ARRAY' && @$cur == 0) {
			next;
		    }
		    $r{add}{$a} = $cur;
		}
	    }
	}
	\%r;
    };
    * { $package . '::as_string' } = sub {
	my $self = shift;
	join(',',
	     map {
		 "$_=".
		 (ref($self->{$_}) eq 'ARRAY'
		     ? '['.join(',',@{$self->{$_}}).']' : $self->{$_})
	     } sort grep { !/^_/ && $self->${\$_} } keys %{$self});
    };
    unshift @_, 'dn';
    foreach my $dfn (@_) {
	if ($dfn =~ m/^(?<attr>[a-zA-Z_][a-zA-Z_0-9]*)
                       (?::(?<flag>.*))?$/x) {
	    my $attribute = $+{attr};
	    my $flag = $+{flag} || 'rw';
	    if ($flag eq 'ro') {
		*{ $package . '::' . $attribute } = sub {
		    my $self = shift;
		    croak "too many arguments for ${package}::$attribute"
			if @_;
		    return $self->{$attribute};
		}
	    } elsif ($flag eq 'ar') {
		*{ $package . '::' . $attribute } = sub {
		    my $self = shift;
		    if (@_) {
			croak "too many arguments for ${package}::$attribute"
			    if @_ > 1;
			my $val = shift;
			$self->{_orig}{$attribute} = $self->{$attribute}
			    unless $self->{_orig}{$attribute};
			if (defined($val)) {
			    $self->{$attribute} = ref($val) eq 'ARRAY'
				                  ? $val : [ $val ];
			} else {
			    delete $self->{$attribute};
			}
		    }
		    return $self->{$attribute};
		}
	    } else {
		*{ $package . '::' . $attribute } = sub {
		    my $self = shift;
		    if (@_) {
			croak "too many arguments for ${package}::$attribute"
			    if @_ > 1;
			$self->{_orig}{$attribute} = $self->{$attribute}
			    unless $self->{_orig}{$attribute};
		        $self->{$attribute} = shift;
		    }
		    return $self->{$attribute};
		}
	    }
	} else {
	    croak "$filename:$line: bad attribute spec: $dfn"
	}
    }
}

1;

