package Savane::LDAP::Map;
use strict;
use warnings;
use Carp;

sub new {
    my ($class, %ini) = @_;
    my $self = bless {}, $class;
    @{$self}{keys %ini} = values %ini;
    $self->{_map} = \&Savane::LDAP::Map::_fromLDAP;
    $self;
}

sub rev {
    my ($class, %ini) = @_;
    my $self = bless {}, $class;
    @{$self}{values %ini} = keys %ini;
    $self->{_map} = \&Savane::LDAP::Map::_toLDAP;
    $self;
}

sub _fromLDAP {
    my ($self, $entry) = @_;
    my %r;
    while (my ($attr,$field) = each %$self) {
	if (my $v = $entry->get_value($attr, asref => 1)) {
	    $r{$field} = (@$v == 1) ? $v->[0] : $v;
	}
    }
    return \%r;
}

sub map { shift->{_map}(@_) }

sub _toLDAP {
    my ($self, $cs) = @_;
    my %r;
    while (my ($cat, $subset) = each %$cs) {
	if (ref($subset) eq 'ARRAY') {
	    $r{$cat} = [ map {
		             croak "missing mapping for $_"
				 unless exists $self->{$_};
			     $self->{$_}
			 } @$subset ];
	} elsif (ref($subset) eq 'HASH') {
	    $r{$cat} = { map {
		             croak "missing mapping for $_"
				 unless exists $self->{$_};
		             $self->{$_} => $subset->{$_}
			 } keys %$subset };
	} else {
	    croak "map value must be hash or array ref";
	}
    }
    \%r
}

1;
