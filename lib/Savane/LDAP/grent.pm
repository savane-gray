package Savane::LDAP::grent;
use strict;
use warnings;
use Carp;
use Savane::LDAP::Base qw(name passwd:ro gid mem:ar);
use overload
    '""' => sub { shift->as_string };

sub mem_add {
    my $self = shift;
    my $mem = $self->mem;
    foreach my $name (@_) {
	push @$mem, $name
	    unless grep { $_ eq $name } @$mem;
    }
    $self->mem($mem);
}
    

sub mem_del {
    my $self = shift;
    my $mem = $self->mem;
    foreach my $name (@_) {
	foreach my $n (reverse grep { $mem->[$_] eq $name } (0 .. $#{$mem})) {
	    splice @$mem, $n, 1;
	}
    }
    $self->mem($mem);
}

1;
