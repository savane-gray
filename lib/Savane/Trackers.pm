#!/usr/bin/perl
# <one line to give a brief idea of what this does.>
# 
# Copyright 2006 (c) Mathieu Roy <yeupou--gnu.org>
# Copyright (C) 2011-2016 Sergey Poznyakoff <gray@gnu.org>
#
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#

##
## Desc: any subs related to trackers
## (not loaded by default)

use strict;
use Savane::Conf qw(:load);
require Exporter;
use Savane::Mail;
use POSIX 'strftime';
use Encode;

# Exports
our @ISA = qw(Exporter);
our @EXPORT = qw(SendTrackersDelayedNotification GetTrackersContentAsMail TrackerNames);
our $version = 1;

my @trackers = ("cookbook", "support", "bugs", "task", "patch");

sub TrackerNames {
    return @trackers;
}

## To send mail notif for items temporarily delayed until they are checked
# arg0: tracker
# arg1: item_id
# arg2: comment_id
# arg3: spamscore
sub SendTrackersDelayedNotification { 
    # Using PHP CLI to do that would avoid reimplementing frontend code to
    # build notifications but it is not shipped in SLC4, so it is blocker
    #
    # Only sensible option: putting in the database the content of mail notifs
    # as the frontend would have sent it. 
    # Not wonderful for the database but... well. It saves the time needed
    # to maintain duplicated code.

    # Get parameters
    my ($tracker, $item_id, $comment_id, $spamscore) = @_;

    my $table = "trackers_spamcheck_queue_notification";
    my $where = "artifact='$tracker' AND item_id='$item_id' AND comment_id='$comment_id'";

    # If spamscore if < 5, it is not a spam, send the notification delayed
    if ($spamscore < 5) {
	
	foreach my $entry (GetDBLists($table, $where, "to_header,other_headers,subject_header,message")) {
	    my ($to_header, $other_headers, $subject_header, $message) = @$entry;
	    my (@other_headers) = split("\n", $other_headers);
	    MailSend(0, 
		     $to_header,
		     $subject_header,
		     $message,
		     0,
		     @other_headers);
	} 
    }

    # Remove the notif from the queue
    DeleteDB($table, $where);
}


## Build a simili mail from some content (item or comment)
# arg0 = user
# arg1 = sender ip
# arg2 = tracker
# arg3 = item id
# arg4 = comment id id
# arg5 = timestamp
# arg6 = subject
# arg7 = message
sub GetTrackersContentAsMail { 
    my ($uid, $sender_ip, $tracker, $item_id, $comment_id, $now,
	$subject, $message) = @_;

    $subject = encode('MIME-Header', $subject);

    my $maildomain = GetConf('mail.mail_domain');
    my $wwwdomain = GetConf('core.domain');
    my $version = GetVersion();

    my $user = "A Logged In User";
    $user = "Ann Honymous One" if $uid eq "100";   

    my $sender = 'user+'.$uid.'@'.$maildomain;
    my $recipient = 'tracker+'.$tracker.'@'.$maildomain;
    my $date = strftime("%a, %e %b %Y %H:%M:%S %z", localtime);

    return qq{Return-Path: <$sender>
Delivered-To: $recipient
Received: from [$sender_ip]
        by $wwwdomain with esmtp (Savane $version)
	id ${tracker}-i${item_id}-c$comment_id
	for $recipient; $now
Mime-Version: 1.0
Message-Id: <${tracker}${item_id}-c${comment_id}\@$maildomain>
Date: $date
To: Tracker $tracker <$recipient>
From: $user <$sender>
Subject: $subject
Content-Type: text/plain; charset="utf-8"
Content-Transfer-Encoding: binary
Sender: $sender
Reply-To: $sender

$message
};

}

1;
