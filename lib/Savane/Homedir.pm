package Savane::Homedir;
use strict;
use warnings;
use Savane::Conf qw(:load);
use Savane::User;
use Savane::Backend;
use File::Find;
use File::Copy 'cp';
use File::Path 'remove_tree';
use File::stat;
use Fcntl ':mode';
use User::pwent;
use Carp;

sub new {
    my ($class, $user, %opts) = @_;
    my $self = bless {
	user => $user,
	dir => delete $opts{dir} || GetUserHome($user),
	skel => delete $opts{skel} || GetConf('backend.sv_users.skel_dir'),
	errors => [],
	report => delete $opts{report}
    }, $class;
    croak "surplus arguments" if keys %opts;
    return $self;
}

sub user { shift->{user} }
sub dir  { shift->{dir} }
sub skel { shift->{skel} }

sub exists {
    my $self = shift;
    -d $self->dir
}

sub error {
    my ($self, $obj, $op, $mesg) = @_;
    push @{$self->{errors}}, [ $obj, $op, $mesg ];
    if ($self->{report}) {
	logit('err', "$obj: $op: $mesg");
    }
}

sub errors { @{shift->{errors}} }

sub clear_errors { shift->{errors} = [] }

sub populate {
    my $self = shift;
    my $s = $self->skel
	or return 1;
    my $rx = qr{$s};
    my @modes;
    my $pw = getpwnam($self->user);
    unless ($pw) {
	$self->error($self->user, 'getpwnam', $!);
	return 0;
    }
    find(sub {
	    return if $_ eq '.';
	    (my $dest = $File::Find::name) =~ s{^$rx}{$self->dir}e;
	    my $st = stat($_);
	    if (S_ISDIR($st->mode)) {
		unless (mkdir $dest, 0700) {
		    $self->error($dest, 'mkdir', $!);
		    return;
		}
		unshift @modes, [ $dest, $st->mode & 0777 ]
	    } else {
		cp($_, $dest)
		    or $self->error($dest, 'cp', $!);
	    }
	    chown $pw->uid, $pw->gid, $dest
		or $self->error($dest, 'chown', $!);
	 }, $self->skel);
    while (my $ent = shift @modes) {
	chmod $ent->[1], $ent->[0]
	    or $self->error($ent->[1], 'chmod', $!);
    }
    return !$self->errors
}

sub create {
    my $self = shift;
    if ($self->exists) {
	$self->error($self->dir, 'create', 'already exists');
	return 0;
    }

    my $pw = getpwnam($self->user);
    unless ($pw) {
	$self->error($self->user, 'getpwnam', $!);
	return 0;
    }

    unless (mkdir($self->dir, 0700)) {
	$self->error($self->dir, 'mkdir', $!);
	return 0;
    }
    chown $pw->uid, $pw->gid, $self->dir
	or $self->error($self->dir, 'chown', $!);
    $self->populate;
    chmod 0755, $self->dir
	or $self->error($self->dir, 'chmod', $!);
    return !$self->errors
}

sub delete {
    my $self = shift;
    remove_tree($self->dir,
		{ error  => \my $err });
    if (@$err) {
	for my $diag (@$err) {
	    my ($file, $message) = %$diag;
	    $self->error($file, 'remove', $message);
	}
    }
    return !$self->errors
}
    

1;

