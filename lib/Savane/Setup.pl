# Savane installation settings                 -*- perl -*-
# Copyright (C) 2016  Sergey Poznyakoff <gray@gnu.org>
#
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package Savane::Setup;
use strict;
use Carp;
require Exporter;

our @ISA = qw(Exporter);
our @EXPORT = qw(setupconf
                 SV_CONFIG_DIR
                 SV_HTTPD_USER
                 SV_DOCUMENT_ROOT
                 SV_SITE_ADMIN_PROJECT
                 SV_USERS_GROUP);

use constant SV_CONFIG_DIR         => 'SV_CONFDIR';
use constant SV_HTTPD_USER         => 'SV_HTTPD_USER';
use constant SV_DOCUMENT_ROOT      => 'SV_DOCUMENT_ROOT';
use constant SV_SITE_ADMIN_PROJECT => 'SV_SITE_ADMIN_PROJECT';
use constant SV_USERS_GROUP        => 'SV_USERS_GROUP';
    
my %setup = (
    SV_CONFIG_DIR()         => '/SV_CONF/',
    SV_HTTPD_USER()         => '/SV_HTTPD_USER/',
    SV_DOCUMENT_ROOT()      => '/SV_WWW_TOPDIR/',
    SV_SITE_ADMIN_PROJECT() => '/SV_SITEADM_GROUP/',
    SV_USERS_GROUP()        => '/SV_USERS_GROUP/'
);

sub setupconf {
    my $var = shift;
    confess "Unknown setup variable $var" unless exists $setup{$var};
    return $setup{$var}
}

1;
