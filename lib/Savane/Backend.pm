# Backend-specific functions for Savane.   -*- perl -*-
# Copyright (C) 2011-2016 Sergey Poznyakoff <gray@gnu.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
package Savane::Backend;
use strict;
use Getopt::Long qw(:config gnu_getopt no_ignore_case);
use File::Basename;
use Pod::Usage;
use Sys::Syslog;
use Savane;
use Savane::Conf qw(:load);
use Savane::Feature qw(:prio);
use feature "state";

use constant EX_OK           => 0;
use constant EX_USAGE        => 64;    # command line usage error
use constant EX_DATAERR      => 65;    # data format error
use constant EX_NOINPUT      => 66;    # cannot open input
use constant EX_NOUSER       => 67;    # addressee unknown
use constant EX_NOHOST       => 68;    # host name unknown
use constant EX_UNAVAILABLE  => 69;    # service unavailable
use constant EX_SOFTWARE     => 70;    # internal software error
use constant EX_OSERR        => 71;    # system error (e.g., can't fork)
use constant EX_OSFILE       => 72;    # critical OS file missing
use constant EX_CANTCREAT    => 73;    # can't create (user) output file
use constant EX_IOERR        => 74;    # input/output error
use constant EX_TEMPFAIL     => 75;    # temp failure; user is invited to retry
use constant EX_PROTOCOL     => 76;    # remote error in protocol
use constant EX_NOPERM       => 77;    # permission denied
use constant EX_CONFIG       => 78;    # configuration error
    
# Exports
our @ISA = qw(Exporter);
our @EXPORT = qw(backend_init
                 backend_setup
                 backend_done
                 backend_atexit
                 abend
                 logit
                 logoutput
                 runcommand
                 dry_run
                 debug_level
                 debug
                 backend_facility
                 backend_name
                 backend_feature
                 backend_feature_log
		 EX_OK
                 EX_USAGE        
                 EX_DATAERR            
                 EX_NOINPUT      
                 EX_NOUSER       
                 EX_NOHOST       
                 EX_UNAVAILABLE  
                 EX_SOFTWARE     
                 EX_OSERR        
                 EX_OSFILE       
                 EX_CANTCREAT    
                 EX_IOERR        
                 EX_TEMPFAIL     
                 EX_PROTOCOL     
                 EX_NOPERM       
                 EX_CONFIG);
    
my $dry_run;
my $debug;
my $help;
my $man;

my $script;
my $sys_syslog_facility = GetConf('backend.syslog_facility') || 'user';
my $facility;

sub isatty() {
    no autodie;
    state $isatty = open(my $tty, '+<', '/dev/tty');
    return $isatty;
}

my %defopts = ("debug|d+" => \$debug,
	       "dry-run|n" => \$dry_run,
	       "facility|F=s" => sub { $sys_syslog_facility = $_[0];
			  	       $facility = $sys_syslog_facility; },
	       "syslog" => sub { $facility = $sys_syslog_facility; },
	       "stderr" => sub { $facility = undef; },
	       "h" => \$help,
	       "help" => \$man);

# backend_setup(%options)
# Options:
#    options => \%opts
#    errout => stderr|syslog
#    facility => str
#    cron => val
#    quiet => 0|1
sub backend_setup {
    local %_ = @_;

    my %opts;
    if (defined($_{options}) and ref($_{options}) eq 'HASH') {
	%opts = (%{$_{options}}, %defopts);
    } else {
	%opts = %defopts;
    }
    if (exists($_{cron})) {
	$opts{cron} = sub {
	    exit unless $_{cron};
	    $facility = $sys_syslog_facility;
	};
    }

    if ($_{facility}) {
	$sys_syslog_facility = $_{facility};
    } elsif (!defined($sys_syslog_facility)) {
	$sys_syslog_facility = "user";
    }

    if (defined($_{errout})) {
	if ($_{errout} eq 'stderr') {
	    $facility = undef;
	} elsif ($_{errout} eq 'syslog') {
	    $facility = $sys_syslog_facility;
	} else {
	    die "invalid value for 'errout'";
	}
    }

    $script = basename($0);

    GetOptions(%opts) or do {
	print STDERR "Try $0 --help for more info\n";
	exit(EX_USAGE);
    };
    ++$debug if $dry_run;
    pod2usage(-message => $_{descr}, -exitstatus => EX_OK) if $help;
    pod2usage(-exitstatus => EX_OK, -verbose => 2) if $man;

    # Log: Starting logging
    if ($facility) {
	if ($facility =~ m#^/#) {
	    open(STDERR, ">>", $facility);
	    $facility = undef;
	} else {
	    openlog($script, "pid", $facility);
	}
    } elsif (!isatty()) {
	$facility = $sys_syslog_facility;
	openlog($script, "pid", $sys_syslog_facility);	
    }
    unless ($_{quiet}) {
	logit("info", "starting");
	backend_atexit(sub { logit("info", "finished"); });
    }
}

sub backend_init($$;$) {
    my %args;

    $args{options} = $_[0];
    $args{descr} = $_[1];
    $args{facility} = $_[2] if ($#_ == 2);
    backend_setup(%args);
}

my @atexit_list;

sub backend_atexit {
    push @atexit_list, @_;
}

END {
    foreach my $f (@atexit_list) {
	&$f;
    }
}

sub backend_done {
}

sub abend {
    my $code = shift;
    logit("crit", @_);
    exit($code);
}

sub logit {
    my $prio = shift;
    my $msg = join("", @_);
    if (defined($facility)) {
	syslog($prio, $msg);
    } else {
	$msg =~ s/\n/\\n/g;
	print STDERR "$script: $prio";
	print STDERR ": $msg" if ($msg);
	print STDERR "\n";
    }
}

sub logoutput {
    my $cmd = shift;
    my $output = $_[0];
    foreach my $line (split("\n", $output)) {
	logit("info", "$cmd: $line");
    }
}

sub debug {
    if ($debug) {
	logit("debug", @_) if $#_ > -1;
	return 1;
    }
    return 0;
}

our $error_count;

sub runcommand {
    debug("running \"@_\"");
    
    unless ($dry_run) {
	my $prog = $_[0];
	pipe(RDFILE, WRFILE);
	my $pid = fork();
	if (not defined $pid) {
	    logit("err", "Cannot run @_: fork failed: $!");
	    return 0;
	} elsif ($pid == 0) {
	    open(STDOUT, ">&WRFILE");
	    open(STDERR, ">>&WRFILE");
	    exec @_;
	    die "Failed to run @_: $!\n";
	} else {
	    close(WRFILE);
	    while (<RDFILE>) {
		logoutput("($prog)", $_);
	    }
	    close(RDFILE);
	    waitpid($pid, 0);
	    if ($?) {
		my $msg;
		if ($? == -1) {
		    $msg = "failed to execute";
		} elsif ($? & 127) {
		    $msg = "terminated on signal ".($? & 127);
		} else {
		    $msg = "exited with value ".($? >> 8);
		}
		logit("err", "Failed to run @_: $msg");
		$error_count++;
		return 0;
	    }
	}
    }
    return 1;
}

sub dry_run {
    return $dry_run;
}

sub debug_level {
    return $debug;
}

sub backend_facility {
    return $facility;
}

sub backend_name {
    return $script;
}

sub backend_feature {
    my ($feature, $ref) = @_;

    return unless exists $ref->{feature}{$feature};
    return if $ref->{feature}{$feature}{dir} eq '';
    
    my $pack = 'Savane::Feature::' . ucfirst($feature)
	       . '::' . ucfirst($ref->{feature}{$feature}{dir_type});
#    debug("trying $pack");
    my $obj = eval "use $pack; new $pack(\$feature, \$ref);";
    if ($@) {
	my $err = $@;

	$pack = 'Savane::compat::'.$feature.'::'.$ref->{feature}{$feature}{dir_type};
	$pack =~ s/-/_/g;
#	debug("trying $pack");
	$obj = eval "use $pack; new $pack(\$feature, \$ref);";
	if ($@) {
	    logit('err', $err);
	    logit('err', "Compatibility layer: $@");
	    return undef;
	}
    }

    if (dry_run()) {
	# Override the created $obj.  Savane::Feature does not
	# actually create anything, so it is safe to use in
	# dry-run mode.
	$obj = new Savane::Feature($feature, $ref);
    }
    
    return $obj;
}

sub backend_feature_log {
    my $obj = shift;
    my $tag = "$obj->{group}{unix_group_name}: ".ref($obj);

    if ($obj->has_log(PRIO_ERROR)) {
	foreach my $msg ($obj->get_log(PRIO_ERROR)) {
	    logit('err', "$tag: $msg");
	}
    }
    if ($obj->has_log(PRIO_WARNING)) {
	foreach my $msg ($obj->get_log(PRIO_WARNING)) {
	    logit('warning', "$tag: warning: $msg");
	}
    }
    if ($obj->has_log(PRIO_INFO)) {
	foreach my $msg ($obj->get_log(PRIO_INFO)) {
	    logit('info', "$tag: info: $msg");
	}
    }
    
    if (my @items = $obj->get_created()) {
	if (@items) {
	    foreach my $item (@items) {
		logit('info', "$tag: created $item");
	    }
	}
    }
}

1;
