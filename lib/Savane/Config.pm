# Configuration parser for Savane                -*- perl -*-
# Copyright (C) 2016  Sergey Poznyakoff <gray@gnu.org>
#
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package Savane::Config;

use strict;
use Carp;
use File::stat;
use Storable qw(retrieve store);
use Data::Dumper;

require Exporter;
our @ISA = qw(Exporter);

our $VERSION = "1.00";

sub deferror {
    my ($err) = @_;
    carp "$err";
}

sub new {
    my $class = shift;
    my $filename = shift;
    local %_ = @_;
    my $self = bless { filename => $filename }, $class;
    my $v;
    my $err;
    
    if (defined($v = delete $_{error})) {
	if (ref($v) eq 'CODE') {
	    $self->{error} = $v;
	} else {
	    carp "error must refer to a CODE";
	    ++$err;
	}
    } else {
	$self->{error} = \&deferror;
    }
    
    if (defined($v = delete $_{debug})) {
	if (ref($v) eq 'CODE') {
	    $self->{debug} = $v;
	} else {
	    carp "debug must refer to a CODE";
	    ++$err;
	}
    }

    if (defined($v = delete $_{ci})) {
	$self->{ci} = $v;
    }
    
    if (defined($v = delete $_{parameters})) {
	if (ref($v) eq 'HASH') {
	    $self->{parameters} = $v;
	} else {
	    carp "parameters must refer to a HASH";
	    ++$err;
	}
    }

    if (defined($v = delete $_{cachefile})) {
	$self->{cachefile} = $v;
    }

    if (defined($v = delete $_{cache})) {
	unless (exists($self->{cachefile})) {
	    $v = $self->{filename};
	    $v =~ s/\.(conf|cnf|cfg)$//;
	    unless ($v =~ s#(.+/)?(.+)#$1.$2#) {
		$v = ".$v";
	    }
	    $self->{cachefile} = "$v.cache";
	}
    }
    
    if (defined($v = delete $_{rw})) {
	$self->{rw} = $v;
    }
    
    if (keys(%_)) {
	foreach my $k (keys %_) {
	    carp "unknown parameter $k"
	}
	++$err;
    }
    return undef if $err;
    return $self;
}

sub DESTROY {
    my $self = shift;
    $self->writecache();
}

sub writecache {
    my $self = shift;
    return unless exists $self->{cachefile};
    return unless exists $self->{conf};
    return unless $self->{updated};
    &{$self->{debug}}("storing cache file $self->{cachefile}")
	if exists $self->{debug};
    store $self->{conf}, $self->{cachefile};
}

sub parse_section {
    my ($self, $conf, $input) = @_;
    my $ref = $conf;
    my $quote;
    my $rootname;
    my $kw = $self->{parameters} if exists $self->{parameters};
    while ($input ne '') {
	my $name;
	if (!defined($quote)) {
	    if ($input =~ /^"(.*)/) {
		$quote = '';
		$input = $1;
	    } elsif ($input =~ /^(.+?)(?:\s+|")(.*)/) {
		$name = $1;
		$input = $2;
	    } else {
		$name = $input;
		$input = '';
	    }
	} else {
	    if ($input =~ /^([^\\"]*)\\(.)(.*)/) {
		$quote .= $1 . $2;
		$input = $3;
	    } elsif ($input =~ /^([^\\"]*)"\s*(.*)/) {
		$name = $quote . $1;
		$input = $2;
		$quote = undef;
	    } else {
		croak "unparsable input $input";
	    }
	}

	if (defined($name)) {
	    $rootname = $name unless defined $rootname;
	    $ref->{$name} = {} unless ref($ref->{$name}) eq 'HASH';
	    $ref = $ref->{$name};

	    if (defined($kw)
		and ref($kw) eq 'HASH' and exists($kw->{$name}{section})) {
		$kw = $kw->{$name}{section};
	    }
	    
	    $name = undef;
	}
    }
    return ($ref, $rootname, $kw);
}

sub check_mandatory {
    my ($self, $section, $kw, $loc, $s) = @_;
    my $err = 0;
    while (my ($k, $d) = each %{$kw}) {
	if (ref($d) eq 'HASH'
	    and $d->{mandatory}
	    and !exists($section->{$k})) {
	    if (exists($d->{section})) {
		if ($s) {
		    $self->{error}("$loc: mandatory section [$k] not present");
		    ++$err;
		}
	    } else {
		$self->{error}("$loc: mandatory variable \"$k\" not set");
		++$err;
	    }
	}
    }
    return $err;
}

sub readconfig {
    my $self = shift;
    my $file = shift;
    my $conf = shift;

    &{$self->{debug}}("reading file $file") if exists $self->{debug};
    open(my $fd, "<", $file)
	or do {
	    $self->{error}("can't open configuration file $file: $!");
	    return 1;
        };
    
    my $line;
    my $err;
    my $section = $conf;
    my $kw = $self->{parameters};
    my $include = 0;
    my $rootname;
    
    while (<$fd>) {
	++$line;
	chomp;
	if (/\\$/) {
	    chop;
	    $_ .= <$fd>;
	    redo;
	}
	
	s/^\s+//;
	s/\s+$//;
	s/#.*//;
	next if ($_ eq "");

	if (/^\[(.+?)\]$/) {
	    $include = 0;
	    my $arg = $1;
	    $arg =~ s/^\s+//;
	    $arg =~ s/\s+$//;
	    if ($arg eq 'include') {
		$include = 1;
	    } else {
		($section, $rootname, $kw) = $self->parse_section($conf, $1);
		$self->{error}("$file:$line: unknown section")
		    if (exists($self->{parameters}) and !defined($kw));
	    }
	} elsif (/([\w_-]+)\s*=\s*(.*)/) {
	    my ($k, $v) = ($1, $2);
	    $k = lc($k) if $self->{ci};

	    if ($include) {
		if ($k eq 'path') {
		    $err += $self->readconfig($v, $conf, include => 1);
		} elsif ($k eq 'pathopt') {
		    $err += $self->readconfig($v, $conf, include => 1)
			if -f $v;
		} elsif ($k eq 'glob') {
		    foreach my $file (bsd_glob($v, 0)) {
			$err += $self->readconfig($file, $conf, include => 1);
		    }
		} else {
		    $self->{error}("$file:$line: unknown keyword");
		    ++$err;
		}
		next;
	    }

	    if (defined($kw)) {
		my $x = $kw->{$k};
		$x = $kw->{'*'} unless defined $x;
		if (!defined($x)) {
		    $self->{error}("$file:$line: unknown keyword $k");
		    ++$err;
		    next;
		} elsif (ref($x) eq 'HASH') {
		    my $errstr;
		    
		    if (exists($x->{re})) {
			if ($v !~ /$x->{re}/) {
			    $self->{error}("$file:$line: invalid value for $k");
			    ++$err;
			    next;
			}
			if (exists($x->{check})
			    and defined($errstr = &{$x->{check}}(\$v))) {
			    $self->{error}("$file:$line: $errstr");
			    ++$err;
			    next;
			}
		    } elsif (exists($x->{check})) {
			if (defined($errstr = &{$x->{check}}(\$v))) {
			    $self->{error}("$file:$line: $errstr");
			    ++$err;
			    next;
			}
		    }
		}
	    }

            $section->{$k} = $v;
        } else {
    	    $self->{error}("$file:$line: malformed line");
	    ++$err;
	    next;
	}
    }
    close $fd;
    return $err;
}

sub file_up_to_date {
    my ($self, $file) = @_;
    my $st_conf = stat($self->{filename}) or return 1;
    my $st_file = stat($file)
	or carp "can't stat $file: $!";
    return $st_conf->mtime <= $st_file->mtime;
}

sub parse {
    my ($self) = @_;
    my %conf;

    if (exists($self->{cachefile}) and -f $self->{cachefile}) {
	if ($self->file_up_to_date($self->{cachefile})) {
	    my $ref;
	    &{$self->{debug}}("reading from cache file $self->{cachefile}")
		if exists $self->{debug};
	    eval { $ref = retrieve($self->{cachefile}); };
	    if (defined($ref)) {
		$self->{conf} = $ref;
		$self->{updated} = $self->{rw};
		return 1;
	    } elsif ($@) {
		$self->{error}("warning: unable to load configuration cache: $@");
	    }
	}
	unlink $self->{cachefile};
    }
    
    &{$self->{debug}}("parsing $self->{filename}") if exists $self->{debug};
    my $err = $self->readconfig($self->{filename}, \%conf);
    if ($err == 0) {
	$self->{conf} = \%conf ;
	$self->{updated} = 1;
    }
    return !$err;
}
	
sub get {
    my $self = shift;
    
    return undef unless exists $self->{conf};
    my $ref = $self->{conf};
    for (@_) {
	return undef unless exists $ref->{$_};
	$ref = $ref->{$_};
    }
    return $ref;
}

sub isset {
    my $self = shift;
    return defined $self->get(@_);
}

sub issection {
    my $self = shift;
    my $ref = $self->get(@_);
    return defined($ref) and ref($ref) eq 'HASH';
}

sub isscalar {
    my $self = shift;
    my $ref = $self->get(@_);
    return defined($ref) and ref($ref) ne 'HASH';
}
    
sub set {
    my $self = shift;
    $self->{conf} = {} unless exists $self->{conf};
    my $ref = $self->{conf};
   
    while ($#_ > 1) {
	my $arg = shift;
	$ref->{$arg} = {} unless exists $ref->{$arg};
	$ref = $ref->{$arg};
    }
    $ref->{$_[0]} = $_[1];
    $self->{updated} = $self->{rw};
}

sub unset {
    my $self = shift;
    return unless exists $self->{conf};
    my $ref = $self->{conf};
    my @path;
    
    for (@_) {
	return unless exists $ref->{$_};
	push @path, [ $ref, $_ ];
	$ref = $ref->{$_};
    }

    while (1) {
	my $loc = pop @path;
	delete ${$loc->[0]}{$loc->[1]};
	last unless (defined($loc) and keys(%{$loc->[0]}) == 0);
    }
    $self->{updated} = $self->{rw};
}    

#sub each {
#    my $self = shift;
#    return @{[ each %{$self->{conf}} ]};
#}

sub _foreach {
    my $self = shift;
    my $domain = shift;
    my $aref = shift;
    my $cb = shift;

    while (my ($k, $v) = each %{$aref}) {
	if (ref($v) eq 'HASH') {
	    push @{$domain}, $k;
	    $self->_foreach($domain, $v, $cb);
	    pop @{$domain};
	} else {
	    &{$cb}($k, $v, @{$domain});
	}
    }
}

sub foreach {
    my ($self, $cb) = @_;
    $self->_foreach([], $self->{conf}, $cb);
}

