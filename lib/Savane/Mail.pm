#!/usr/bin/perl
# <one line to give a brief idea of what this does.>
# 
# Copyright 2004-2005 (c) Mathieu Roy <yeupou--gnu.org>
# Copyright (C) 2011-2016 Sergey Poznyakoff <gray@gnu.org>
# 
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


##
## Desc: any subs related to mail sending.
## This module is not loaded by Savannah.pm
##

use strict "vars";
use Savane::Conf qw(:all);
require Mail::Send;
require Exporter;

# Exports
our @ISA = qw(Exporter);
our @EXPORT = qw(MailSend);
our $version = 1;

sub fixup_recipients {
    my @recipients = @_;
    my %allowed;
    if (IsSet('development.allow_mail_to')) {
	my @a = split /\s+/, GetConf('development.allow_mail_to');
	@allowed{@a} = (1) x @a;
	@recipients = grep { s/.*<(.+)>/$1/; exists $allowed{$_} } @recipients;
    }
    if (IsSet('development.force_mail_to')) {
	my @force_mail_to = split /\s+/, GetConf('development.force_mail_to');
	
	if (keys(%allowed)) {
	    foreach my $mail (@force_mail_to) {
		push @recipients, $mail
		    unless grep { s/.*<(.+)>/$1/; $_ eq $mail} @recipients;
	    }
	} else {
	    @recipients = @force_mail_to;
	}
    }
    return @recipients;
}
	
# Fixme: for now, there's only very limited testing here, it should in the
# end looks like sendmail.php

# from, to, subject, message
# Note: don't forget to escape @, otherwise address will be treated as 
# list.
# arg0: from
# arg1: to
# arg2: subject
# arg3: message
# arg4: Cc
# arg5: list of headers (can override previously set ones)
sub MailSend {
    my ($from, $to, $subject, $message, $cc, @headers) = @_;
    
    my $msg = new Mail::Send;
        
    $msg->to($to);
    $msg->subject($subject);
    if ($from ne "") {
	$msg->add("From", $from);	
    } else {
	$msg->add("From", GetConf('mail.sender_address'));
    }
	
    $msg->add("Cc", $cc) if $cc;
    $msg->add("X-Savane-Server", GetConf('core.domain'));
    $msg->add("User-Agent", "Savane::Mail.pm");

    
    if (@headers) {
	for (@headers) {
	    my ($header, $value) = split(": ", $_);

	    # Override previously set headers (apart the ones that identifies
	    # the sender (like X-Savane-Server)
	    $msg->delete($header) if
		$header eq "Cc" or
		$header eq "From" or
		$header eq "Subject" or
		$header eq "To";

	    # FIXME: unable to force the msg-id
	    $msg->add($header, $value);
	}
    }

    my $fh = $msg->open;
    print $fh $message."\n\n_______________________________________________
  Message sent via/by ".GetConf('path.common_name')."
  http://".GetConf('core.domain').GetConf('core.top_url')."\n";
    $fh->close;
}

return 1;
## EOF
