# Generic backend feature package for Savane               -*- perl -*-
# Copyright (C) 2016  Sergey Poznyakoff <gray@gnu.org>
#
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package Savane::Feature;
use strict;
use IPC::Open3;
use Symbol 'gensym';
use Storable qw(dclone);
use Carp;

require Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(PRIO_INFO PRIO_WARNING PRIO_ERROR
                    FEATURE_FAIL FEATURE_UNCHANGED FEATURE_CREATED FEATURE_UPDATED); 
our %EXPORT_TAGS = (
    prio => [ qw(PRIO_INFO PRIO_WARNING PRIO_ERROR) ],
    status => [ qw(FEATURE_FAIL FEATURE_UNCHANGED FEATURE_CREATED FEATURE_UPDATED) ],
    all => [ qw(PRIO_INFO PRIO_WARNING PRIO_ERROR
                FEATURE_FAIL FEATURE_UNCHANGED FEATURE_CREATED FEATURE_UPDATED) ]
);

use File::Path qw(make_path);
use File::Find;
use Cwd qw(getcwd);
use File::Temp;

use constant PRIO_WARNING => 0;
use constant PRIO_ERROR   => 1;
use constant PRIO_INFO    => 2;

use constant FEATURE_FAIL => 0;
use constant FEATURE_UNCHANGED => 1;
use constant FEATURE_CREATED => 2;
use constant FEATURE_UPDATED => 3;

=head1 Abstract Feature Object

    my $obj = new Savane::Feature($feature_name, $group);
    $obj->build() and $obj->updatedb();
    
    if ($obj->has_log(PRIO_ERROR)) {
	foreach my $msg ($obj->get_log(PRIO_ERROR)) {
	    print STDERR "$msg\n";
	}
    }

    if ($obj->success()) {
        ...
    }

=head2 new

    my $obj = new Savane::Feature($feature, $group);

Creates new feature object for B<$feature> in project group B<$group>.

The B<$group> argument is a reference to a HASH describing the project (a.k.a
I<group>), and its features.  A sample B<$group> follows:
    
    { 'group_id' => '103',
      'group_name' => 'whatever',
      'type' => '2',
      'is_public' => '1',
      'unix_group_name' => 'group',
      'gidNumber' => '7017',
      'group_type_name' => 'Software',
      'group_type_homepage_scm' => 'cvs',
      'feature' => {
          'homepage' => {
	     'dir_type' => 'basiccvs',
	     'dir' => '/home/project/%PROJECT'
          },
          ...	       
      }
    }
    
=cut
sub new {
    my $class = shift;
    my $feature = shift;
    my $ref = shift;
    
    croak "extra arguments" if @_;
    carp "unix_group_name is missing" unless exists $ref->{unix_group_name};
    carp "$feature: no such feature"
	unless exists $ref->{feature}{$feature};
    my $self = bless { feature => $feature,
		       log => [ [], [], [] ],
		       status => FEATURE_UNCHANGED }, $class;
    $self->{group} = dclone($ref);
    $self->{directory} = $self->expandvars(
	                        $ref->{feature}{$self->{feature}}{dir},
				PROJECT => { value => $ref->{unix_group_name},
					     mandatory => 1,
					     message =>
						 'The string %$variable was not found in template $tmpl, there may be a serious group type misconfiguration' },
				REPO => {
				    value => $ref->{repo_name},
				    mandatory=> 0 });
    return $self;
}

sub DESTROY {
    my ($self) = @_;
    $self->popdir(0);
}

=head2 reset

    $obj->reset();

Resets B<$obj> to initial values.
    
=cut

sub reset {
    my ($self) = @_;
    $self->{log} = [ [], [], [] ];
    $self->{status} = FEATURE_UNCHANGED;
}

=head2 directory

    $obj->directory()

Returns directory path associated with B<$obj>.

=cut    

sub directory {
    my ($self) = @_;
    return $self->{directory};
}

=head2 status

    use Savane::Feature qw(:status);
    my $s = $obj->status();

Returns status of the last B<build> operation performed by B<$obj>,

The values are:

=over 4    

=item B<FEATURE_FAIL>

Operation failed.  Use B<$obj-E<gt>get_log(PRIO_ERROR)> to get detailed
diagnostics.    
    
=item B<FEATURE_UNCHANGED>

Nothing done.
    
=item B<FEATURE_CREATED>

Feature created successfully.
    
=item B<FEATURE_UPDATED>

Feature updated successfully.    

=back
    
=cut
sub status {
    my ($self) = @_;
    return $self->{status};
}

=head2 created

    if ($obj->created()) {
       ... # do something
    }

Returns B<True> if the recent B<build> operation on this B<$obj> did
actually create the layout for the feature.    

=cut
sub created {
    my ($self) = @_;
    return $self->status() == FEATURE_CREATED;
}

=head2 updated

    if ($obj->updated()) {
       ... # do something
    }

Returns B<True> if the recent B<build> operation on this B<$obj> updated
the layout for the feature (as opposed to creating it).

=cut
sub updated {
    my ($self) = @_;
    return $self->status() == FEATURE_UPDATED;
}

=head2 build

    my $result = $obj->build();

The method creates the directory structure for the feature it was
constructed for and returns the resulting status (see B<status>, for
a detailed description).    

=cut
sub build {
    my ($self) = @_;
    $self->{status} = FEATURE_UNCHANGED;
}

=head2 updatedb

    $obj->updatedb();

Updates the database after successfull call to B<$obj-E<gt>build()>.

=cut    
sub updatedb {
    my ($self) = @_;
    # Nothing
}

=head2 backup

    $obj->backup($dir);

Creates a backup of the object in the directory B<$dir>.  The backup is usually
a single file (a tar archive or a VCS-specific dump file), that can be used to
create an exact copy of the object, including any history associated with it.

=cut    
sub backup {
    my ($self, $outdir) = @_;
    $self->reset();
    croak "output directory not given" unless defined $outdir;
    $self->make_dir($outdir, mode => 0700) unless -d $outdir;
}

=head2 snapshot

    $obj->snapshot($dir);

Creates a snapshot of the object in the directory B<$dir>.

=cut
sub snapshot {
    my ($self, $outdir) = @_;
    $self->reset();
    croak "output directory not given" unless defined $outdir;
    $self->make_dir($outdir, mode => 0700) unless -d $outdir;
}

=head2 success

    if ($obj->success()) {
       ... # do something
    }

Returns B<True> if the recent operation (B<build>, B<backup>, or B<snapshot>)
on this B<$obj> created or updated the corresponding disk files.

=cut    
sub success {
    my ($self) = @_;
    return $self->created or $self->updated;
}

=head2 created

    $obj->created($item);

Reports B<$item> (which is the name of a directory, file, or the like), as
successfully created.
    
=cut    
sub created {
    my $self = shift;
    push @{$self->{created}}, @_;
}

=head2 get_created

    my @ary = $obj->get_created();

Returns the list of items successfully created.
    
=cut    
sub get_created {
    my $self = shift;
    return () unless exists $self->{created};
    return @{$self->{created}};
}

=head2 log

    use Savane::Feature qw(:prio);
    $obj->log(PRIO_INFO, 'informational message');
    $obj->log(PRIO_WARNING, 'warning message');
    $obj->log(PRIO_ERROR, 'error message');

Logs the message with the given priority.

=cut    
sub log {
    my $self = shift;
    my $prio = shift;
    croak "invalid priority $prio"
	unless $prio >= 0 and $prio <= $#{$self->{log}};
    if (ref($_[0]) eq 'GLOB') {
	my $fd = shift;
	while (<$fd>) {
	    chomp;
	    push @{$self->{log}[$prio]}, $_;
	}
    } else {
	push @{$self->{log}[$prio]}, @_;
    }
}

=head2 has_log

    use Savane::Feature qw(:prio);

    if ($obj->has_log(PRIO_ERROR)) {
        ... # do something
    }

Returns B<True> if any messages with the given priority have been
issued.

=cut    
sub has_log {
    my $self = shift;
    my $prio = shift;
    croak "invalid priority $prio"
	unless $prio >= 0 and $prio <= $#{$self->{log}};
    return $#{$self->{log}[$prio]} >= 0;
}

=head2 get_log

    use Savane::Feature qw(:prio);

    my @ary = $obj->get_log(PRIO_ERROR);

Returns array of messages that were issued under the given priority so far.
    
=cut    
sub get_log {
    my $self = shift;
    my $prio = shift;
    return undef unless $self->has_log($prio);
    return @{$self->{log}[$prio]};
}

=head2 expandvars

=over 8
    
=item my $str = $obj->expandvars(I<$template>, I<KWARGS>);

=back

The I<$template> argument is a string.  I<KWARGS> are keyword arguments,
B<I<KEYWORD> =E<gt> I<HASH>>.  The method expands I<$template>, replacing
occurrences of B<%I<KEYWORD>> with values according to the corresponding
I<HASH> and returns the resulting string.

Occurrences that have no corresponding keyword defined, are left unchanged.

Each I<HASH> can contain the following keywords:

=over 4

=item B<value =E<gt> I<STRING>>

Replace B<%I<KEYWORD>> with I<STRING>.

=item B<mandatory =E<gt> I<BOOL>>

If B<1>, declares this keyword as mandatory.  If no occurrence of
B<%I<KEYWORD>> was encountered in B<$template>, a warning will be
issued (using B<$obj-E<gt>log(PRIO_WARNING)>).

=item B<message =E<gt> I<STRING>>

Defines the text of a warning issued if no expansion of a mandatory
keyword was produced.  I<STRING> can contain the following variables:
B<$variable>, which is expanded to the keyword, and B<$tmpl>, expanded
to the template string.  The default message is

    '$variable not found in template'
    
=back    

=cut    
sub expandvars {
    my $self = shift;
    my $tmpl = shift;
    local %_ = @_;
    my $res;
    my $dir = $tmpl;
    while ($dir =~ /(?<pref>[^{%]*)%(?:\{(?<var>[A-Z]+)\}|(?<var>[A-Z]+))(?<tail>.*)/) {
	my ($pref, $var, $tail) = ($+{pref}, $+{var}, $+{tail});
	if (exists($_{$var})) {
	    if (defined($_{$var}->{value})) {
		$res .= $pref;
		$res .= $_{$var}->{value};
		$dir = $tail;
	    } elsif ($pref =~ m#/$#
		     and $tail =~ m#^(?:(?<c>[/.])(?<tail>.*))?$#) {
		$res .= substr($pref,0,length($pref)-1);
		$dir = $+{c}.$+{tail};
	    } else {
		$res .= $pref;
		$dir = $tail;
	    }
	    $_{$var}->{mandatory} = 0 if $_{$var}->{mandatory};
	} else {
	    $res .= $pref . '%' . $var;
	    $dir = $tail;
	}
    }
    while (my ($variable,$v) = each %_) {
	if ($v->{mandatory}) {
	    my $diag = exists($v->{message})
		          ? $v->{message}
		          : "\%$variable not found in template";
	    $diag = eval "\"$diag\"";
	    if ($@) {
		confess "error evaluating $diag: $@";
	    } else {
		$self->log(PRIO_WARNING, $diag);
	    }
	}
    }
    return $res.$dir;
}

=head2 make_dir

=over 8

=item $obj->make_dir(I<DIR>, I<KW>);

=back

Create directory I<DIR> and any intermediate directories as needed.  Most
keyword arguments (I<KW>) are passed to B<File::Path::make_path> unchanged,
excepting B<mode>, which is applied only to the last pathname component.    

Upon successful return, reports I<DIR> as created (using B<$obj-E<gt>created()>,
which see) and returns B<True>.  Any errors are reported via B<$obj-E<gt>log>
with priority B<PRIO_ERROR>.  Returns B<False> on error.

=cut    
sub make_dir {
    my $self = shift;
    my $dir = shift;

    return 1 if -d $dir;
    
    local %_ = @_;
    my $err;
    $_{error} = \$err;
    my $mode = delete $_{mode};
    make_path($dir, \%_);
    if (@$err) {
	for my $diag (@$err) {
	    my ($file, $message) = %$diag;
	    if ($file eq '') {
		$self->log(PRIO_ERROR, $message);
	    } else {
		$self->log(PRIO_ERROR, "$file: $message");
	    }
	}
	return 0;
    } else {
	if (defined($mode)) {
	    chmod $mode, $dir
		or $self->log(PRIO_ERROR, "$dir: can't chmod: $!");
	}
	$self->created($dir);
	return 1;
    }
}

=head2

=over 8

=item my $res = $obj->run(I<COMMAND>, I<ARG>, ...);

=back

Runs the external command.  Returns B<True> if it exited with code 0,
otherwise returns B<False>.

I<COMMAND> output is recorded via B<$obj-E<gt>log()>.  The output that
appeared on STDERR is recorded with priority B<PRIO_WARNING>.  What
was printed on STDOUT is recorded with priority B<PRIO_INFO>.    

=cut    
sub run {
    my $self = shift;
    my $out = gensym;
    my $err = gensym;
#    print STDERR "RUN @_\n";
    my $pid = open3(\*STDIN, $out, $err, @_);
    waitpid($pid, 0);
    my $ret = 0;
    if ($? == -1) {
	$self->log(PRIO_ERROR, "failed to run @_");
    } elsif ($? & 127) {
	$self->log(PRIO_ERROR, "$_[0] exited on signal " . ($? & 127));
    } elsif (my $e = ($? >> 8)) {
	$self->log(PRIO_ERROR, "$_[0] exited with status $e");
    } else {
	$ret = 1;
    }
    $self->log(PRIO_WARNING, $err);
    $self->log(PRIO_INFO, $out);
    return $ret;
}

=head2 setup_privs

B<setup_privs(I<KW>)>    

Keywords:

=over 4

=item B<uid>

User name or UID of file owner.  Pass B<-1> to retain current UID.

=item B<gid>

Group name or GID of file owner.  If not given, GID of the primary group
of B<uid> is used, if B<uid> is supplied.  Pass B<-1> to retain current GID.   

=item B<mode>

Mode bits to set.  Its value can be either a scalar, or an array reference.
If it is scalar, these bits will be set on each file encountered.  Otherwise,
the array it refers to must consist of hash references.  Each hash defines
bits to be set condition.  The following keywords are defined:

=over 4

=item B<test =E<gt> I<BITS>>

Test if the file mode has I<BITS> set.  The return value is modified
by B<not>, whcih see.    
    
=item B<not =E<gt> I<BOOL>>

If true, revert the result of B<test> keyword.    

=item B<set =E<gt> I<BITS>>

Set these I<BITS> if the test succeeds.    

=item B<stop =E<gt> I<BOOL>>

If true, stop evaluating the array if the test succeeded.
    
=back    

=item B<dirs>

Directories to operate upon (a string or a ref to an array of strings).
This keyword is mandatory.     

=item B<verbose>

Verbosely print filenames being processed.    
    
=back    
    
=cut    
sub setup_privs {
    my $self = shift;
    local %_ = @_;

    my $x;
    my $uid = -1;
    my $gid = -1;
    my @prog;

    if ($uid = delete $_{uid}) {
	unless ($uid =~ /^\d+$/) {
	    (undef,undef,$uid,$gid) = getpwnam($x)
		or croak "no such user: $x";
	}
    }
    if ($x = delete $_{gid}) {
	if ($x =~ /^\d+$/) {
	    $gid = $x;
	} else {
	    (undef,undef,$gid) = getgrnam($x)
		or croak "no such group: $x";
	}
    }

    if ($x = delete $_{verbose}) {
	push @prog, sub {
	    print STDERR "processing ${File::Find::name}\n";
	};
    }
    
    if ($uid != -1 || $gid != -1) {
	push @prog, sub {
	    chown($uid, $gid, $_)
		or $self->log(PRIO_ERROR,
			      "${File::Find::name}: cannot chown: $!");
	};
    }

    if (my $bits = delete $_{mode}) {
	if (ref($bits) eq 'ARRAY') {
	    push @prog, sub {
		my (undef,undef,$mode) = lstat($_);
		foreach my $cmd (@$bits) {
		    if (ref($cmd) eq 'HASH') {
			next unless exists $cmd->{set};
			if (!exists($cmd->{test})
			    or ($cmd->{not} ? !($mode & $cmd->{test})
				            :  ($mode & $cmd->{test}))) {
			    unless ($mode & $cmd->{set}) {
				chmod((($mode | $cmd->{set}) & 0777), $_)
				    or $self->log(PRIO_ERROR,
						  "${File::Find::name}: cannot chmod: $!");
			    }
			    return if $cmd->{stop};
			}
		    } elsif (ref($cmd) eq 'SCALAR' or ref($cmd) eq '') {
			unless ($mode & $cmd->{set}) {
			    chmod((($mode | $cmd->{set}) & 0777), $_)
				or $self->log(PRIO_ERROR,
					      "${File::Find::name}: cannot chmod: $!");
			}
		    } else {
			croak "invalid command in mode array: ".ref($cmd);
		    }
		}
	    };
	} else {
	    push @prog, sub {
		my (undef,undef,$mode) = lstat($_);
		unless ($mode & $bits) {
		    chmod((($mode | $bits) & 0777), $_)
			or $self->log(PRIO_ERROR,
				      "${File::Find::name}: cannot chmod: $!");
		}
	    };
	}
    }

    unless ($x = delete $_{dirs}) {
	croak "dirs must be specified";
    }

    if (keys %_) {
	my $s = join(', ', keys %_);
	croak "unrecognized arguments: $s";
    }
    
    find(sub { foreach my $code (@prog) { &$code; } },
	 ref($x) eq 'ARRAY' ? @$x : $x);
}

=head2 symlink

    $obj->symlink($source, $dest);
    $obj->symlink($source, $dest, $dir);

Creates a symbolic link from B<$source> to B<$dest>.  If B<$dir> is supplied,
changes to that directory first.

=cut
sub symlink {
    my ($self, $old, $new, $dir) = @_;

    if (defined($dir)) {
	my $wd = getcwd();

	unless (chdir($dir)) {
	    $self->log(PRIO_ERROR, "can't change to $dir: $!");
	    return 0;
	}
	my $rc = $self->symlink($old, $new);
	unless (chdir($wd)) {
	    $self->log(PRIO_ERROR, "can't change back to $wd: $!");
	    $rc = 0;
	}
	return $rc;
    }
    
    if (-l $new) {
	unless (unlink($new)) {
	    $self->log(PRIO_ERROR, "cannot remove $new: $!");
	    return 0;
	}
    } elsif (-e $new) {
	$self->log(PRIO_ERROR,
		   "cannot link $old to $new: target exists and is not a symlink");
	return 0;
    }
    unless (symlink($old, $new)) {
	$self->log(PRIO_ERROR, "cannot link $old to $new: $!");
	return 0;
    }
    return 1;
}

=head2 tempdir

    my $dirname = $self->tempdir();

Returns the name of a temporary directory.  On the first call to B<tempdir>,
the directory is created.  Subsequent calls only return its name.

The directory (and any files in it) is removed when the object is destroyed.

=cut
sub tempdir {
    my ($self) = @_;
    unless (exists($self->{tempdir})) {
	$self->{tempdir} = File::Temp->newdir();
    }
    return $self->{tempdir}->dirname();
}

=head2 pushdir

    if ($self->pushdir()) {
        ...
        $self->popdir();
    } else {
        # error handling
    }

Pushes the name of the current working directory on the stack and
changes to the directory given as its argument.  When used without
arguments (as in the example above), creates a temporary directory
using B<$self-E<gt>tempdir()> and changes to it.

On success, returns the name of the directory changed to.
    
On error, returns B<undef> and reports the cause of the error via
B<$self-E<gt>log(PRIO_ERROR...)>.    

=cut    
sub pushdir {
    my ($self, $dir) = @_;
    my $dir = $self->tempdir() unless defined $dir;

    my $wd = getcwd();
#   print STDERR "changing to $dir\n";
    unless (chdir($dir)) {
#	print STDERR "cannot change to $dir: $!";#FIXME
	$self->log(PRIO_ERROR, "cannot change to $dir: $!");
	return undef;
    }
    push @{$self->{dir_stack}}, $wd;
    return $dir;
}

=head2 popdir

    $self->popdir();

Pops the last directory name pushed on stack by B<$self->pushdir> and
changes to that directory.

    $self->popdir($n);

With non-negative B<$n>, pops B<$n>th element off the directory stack,
discarding any elements pushed after it, and changes to the directory
named by it.  Thus B<$self-E<gt>popdir(0)> effectively changes back to the
directory that was current at the time B<$self-E<gt>pushdir()> was called for
the first time, and discards entire directory stack.

With negative argument, pops the B<-$n>th element counted from the bottom
of the stack, discarding any elements that followed it, and changes to that
directory.    

On error, bails out.
    
=cut
sub popdir {
    my ($self, $count) = @_;

    my $dir;

    return unless defined $self->{dir_stack};
    return unless @{$self->{dir_stack}};
    
    if (!defined($count)) {
	$dir = pop @{$self->{dir_stack}};
    } elsif ($count < 0) {
	while ($count++ and @{$self->{dir_stack}}) {
	    $dir = pop @{$self->{dir_stack}};
	}
    } else {
	@{$self->{dir_stack}} = @{$self->{dir_stack}}[0..$count];
	$dir = pop @{$self->{dir_stack}};
    }
    return unless defined $dir;
#    print STDERR "changing back to $dir\n";
    chdir($dir) or confess "can't change back to $dir: $!";
}

1;
