<?php
# <one line to give a brief idea of what this does.>
# 
# Copyright 1999-2000 (c) The SourceForge Crew
# Copyright 2004-2006 (c) Mathieu Roy <yeupou--gnu.org>
#
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require_once('../include/init.php');
require_once('../include/people/general.php');
register_globals_off();

extract(sane_import('get', array('user_id', 'flagspam')));

session_require(array('isloggedin'=>1));

if ($user_id == null)
  exit_missing_param();

$is_admin = member_check(0, 101, 'A');

if (isset($flagspam))
  {
    if (!user_isloggedin())
      exit_not_logged_in();
    if ($flagspam == 0)
      {
        if (!$is_admin)
          exit_permission_denied();
        spam_unflag($user_id, 0, 'resume', 0) ;
      }
    else if ($flagspam == 1)
      {
        if ($is_admin)
          $score = 5;
        else
          $score = 1;
        spam_flag_resume($user_id, $score);
      }
  }

$result=db_execute("SELECT * FROM user WHERE user_id=?", array($user_id));
  
if (!$result || (db_numrows($result) < 1))
  exit_error(_("User not found"));
else if (db_result($result,0,'people_view_skills') != 1)
  {
    if (user_is_super_user())
      fb(_("This user deactivated his/her Resume & Skills page"),1);
    else
      exit_error(_("This user deactivated his/her Resume & Skills page"));
  }

site_header(array('title'=>sprintf(_("%s Resume & Skills"),db_result($result, 0, 'realname')),
		  'context'=>'people'));


print '<p>'.sprintf(_("Follows Resume & Skills of %s."), utils_user_link(db_result($result, 0, 'user_name'),db_result($result, 0, 'realname'))).'</p>';
# we get site-specific content
utils_get_content("people/viewprofile");

$poster_id = db_result($result,0,'user_id');
$spamscore = db_result($result,0,'resume_spamscore');

print '<h3 style="display: table; width: 100%">';
print '<span style="display: table-cell; text-align: left">'._("Resume").'</span>';
print '<span style="display: table-cell; text-align: right; font-size: small; width:100%">';
if (user_isloggedin()
    && $poster_id != user_getid()
    && empty($_REQUEST['printer']))
  {
    if ($spamscore > 4)
      {
        print '(<a style="display: inline" title="'
          . sprintf(_("Current spam score: %s"), $spamscore)
          . '" href="'
          . $_SERVER['PHP_SELF']
          . '?flagspam=0&amp;user_id='
          . $poster_id
          . '"><img src="'
          . $GLOBALS['sys_home']
          . 'images/'
          . SV_THEME
          . '.theme/bool/ok.png" class="icon" alt="'
          . _("Unflag as spam")
          . '">'
          . _("Unflag as spam")
          .'</a>)';
      }
    else
      {
        print '(<a style="display: inline" title="'
          . sprintf(_("Current spam score: %s"), $spamscore)
          . '" href="'
          . $_SERVER['PHP_SELF']
          . '?flagspam=1&amp;user_id='
          . $poster_id
          . '"><img src="'
          . $GLOBALS['sys_home']
          . 'images/'
          . SV_THEME
          . '.theme/misc/trash.png" class="icon" alt="'
          . _("Flag as spam")
          . '">'
          . _("Flag as spam")
          .'</a>)';
      }
  }
print '</span>';
print '</h3>';

print markup_full(htmlspecialchars(db_result($result,0,'people_resume')));

print '<h3>'._("Skills").'</h3>';
print people_show_skill_inventory($user_id);


site_footer(array());



?>
