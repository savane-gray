<?php

require('include/init.php');
include_once $GLOBALS['sys_securimagedir'] . '/securimage.php';

$img = new securimage(array(
	'image_width' => 360,
	'code_length' => 12,
	'use_transparent_text' => false,
	'num_lines' => 16,
	'noise_level' => 10,
	'perturbation' => 1,
	'image_signature' => 'Click on the image to reload',
	'signature_color'  => '#0a3a0a',
        'image_bg_color'   => '#ffffff',
        'text_color'       => '#f07070',
        'line_color'       => '#e168da',
        'noise_color'      => '#111111'
));

$img->show();
