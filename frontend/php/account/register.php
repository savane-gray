<?php
# Register an account, part 1 (part 2 is e-mail confirmation)
# 
# Copyright 1999-2000 (c) The SourceForge Crew
# Copyright 2003-2006 (c) Mathieu Roy <yeupou--gna.org>
# Copyright (C) 2007  Sylvain Beucler
# Copyright (C) 2011-2015 Sergey Poznyakoff <gray@gnu.org>
# 
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_once('../include/init.php');
require_once('../include/sane.php');
require_once('../include/account.php');
require_once('../include/dnsbl.php');
require_once('../include/spam.php');
require_once('../include/form.php');
require_once('../include/utils.php');
require_once('../include/html.php');
require_once('../include/sendmail.php');
require_once('../include/user_register.php');

register_globals_off();

extract(sane_import('post',
  array('update', 'form_id',
	'form_loginname', 'form_pw', 'form_pw2', 'form_realname', 'form_email',
    'form_year',
	'form_usepam')));

# Logged users have no business here
if (user_isloggedin())
{
  session_redirect($GLOBALS['sys_home'] . 'my/');
}


# Block here potential robots
dnsbl_check();
# Block banned IP
spam_bancheck();


$login_is_valid = false;
$pw_is_valid = false;
$email_is_valid = false;
$realname_is_valid = false;
$antispam_is_valid = false;

if (!empty($update) and form_check($form_id))
// Form is submitted
{
  // feedback included by the check function

  if ($GLOBALS['sys_registration_captcha'])
  {
      include_once $GLOBALS['sys_securimagedir'] . '/securimage.php';
      $securimage = new Securimage();
              
      if ($securimage->check($_POST['captcha_code']) == false)
      {
          fb(_("Please correctly answer the antispam captcha!"),1);
      }
      else
      {
          $antispam_is_valid = true;
      }
  } else 
      $antispam_is_valid = true;

  // Login
  if ($form_loginname == '')
    {
      fb(_("You must supply a username."),1);
    }
  else if (!account_namevalid($form_loginname))
    {
      // feedback included by the check function
    }
  // Avoid duplicates
  else if (db_numrows(db_execute("SELECT user_id FROM user WHERE user_name = ?",
				 array($form_loginname))) > 0)
    {
      fb(_("That username already exists."),1);
    }
  else if (db_numrows(db_execute("SELECT group_list_id FROM mail_group_list WHERE "
				 . "list_name = ?", array($form_loginname))) > 0)
    {
      fb(_("That username is blocked to avoid conflict with mailing-list addresses."),1);
    }
  else
    {
      $login_is_valid = true;
    }

  // Password
  if (!account_pwvalid ($form_pw))
    {
      // feedback already emitted
    }
  // Password sanity checks - unless PAM is used
  else if ($GLOBALS['sys_use_pamauth'] != "1" and $form_usepam != 1 and $form_pw != $form_pw2)
    {
      fb(_("Passwords do not match."),1);
    }
  else if ($GLOBALS['sys_use_pamauth'] != "1" and $form_usepam != 1 and !account_pwvalid($form_pw))
    {
      // feedback included by the check function
    }
  else
    {
      $pw_is_valid = true;
    }

  // E-mail
  if (!$form_email)
    {
      fb(_("You must supply a valid email address."),1);
    }
  else if (!account_emailvalid($form_email))
    {
      // feedback included by the check function
    }
  else
    {
      $email_is_valid = true;
    }

  // Real name
  if ($form_realname == '')
    {
      fb(_("You must supply a real name."),1);
    }
  else
    {
      $realname_is_valid = true;
    }

  ####

  $krb5ret = -1;
  if ($GLOBALS['sys_use_krb5'] == "1")
    {
      $krb5ret = krb5_login($form_loginname, $form_pw);
      if($krb5ret == -1)
        { # KRB5_NOTOK
          fb(_("phpkrb5 module failure"),1);
          $pw_is_valid = false;
        }
      else if ($krb5ret == 1)
        { # KRB5_BAD_PASSWORD
          fb(sprintf(_("User is a kerberos principal but password do not match. Please use your kerberos password for the first login and then change your %s password. This is necessary to prevent someone from stealing your account name."),$GLOBALS['sys_name']),1);

          $pw_is_valid = false;
        }
      else if ($krb5ret == "2")
        {
          # KRB5_BAD_USER

	  /*

FIXME : this is broken and seems to be due to the kerberos module.
        we did not changed anything about that and we get 2 as return
        for any name.

	  if($_POST['form_loginname']."@".$GLOBALS['sys_mail_domain'])
	    {
	      $GLOBALS['register_error'] = sprintf(_("User %s is a known mail alias and cannot be used. If you own this alias (%s@%s) please create a another user (for instance xx%s) and ask %s@%s to rename it to %s."),
						   $_POST['form_loginname'],
						   $_POST['form_loginname'],

						   $GLOBALS['sys_mail_domain'],
						   $_POST['form_loginname'],
						   $GLOBALS['sys_mail_admin'],
						   $GLOBALS['sys_mail_domain'],
						   $_POST['form_loginname']);
	      return 0;
	    }
	  */
        }
    }
}

# Don't forget parenthesis to avoid precendence issues with 'and'
$form_is_valid = ($login_is_valid and $pw_is_valid
		  and $email_is_valid and $realname_is_valid
		  and $antispam_is_valid);

if ($form_is_valid)
{
  if ($GLOBALS['sys_use_pamauth'] == "1" && $form_usepam == 1)
    {
      // if user chose PAM based authentication, set his encrypted
      // password to the specified string
      $passwd = 'PAM';
    }
  else
    {
      $passwd = account_encryptpw($form_pw);
    }

  $confirm_hash = substr(md5(rand(0, 32768) . $passwd . time()), 0, 16);
  $result=db_autoexecute(
    'user',
    array(
      'user_name' => strtolower($form_loginname),
      'user_pw'   => $passwd,
      'realname'  => $form_realname,
      'email'     => $form_email,
      'add_date'  => time(),
      'status'    => 'P',
      'confirm_hash' => $confirm_hash),
    DB_AUTOQUERY_INSERT);

  if (!$result)
    {
      exit_error('error',db_error());
    }
  else
    {

      $newuserid = db_insertid($result);

      # clean id
      form_clean($form_id);

      $message = user_register_confirm_mail($confirm_hash, $krb5ret);
      sendmail_mail($GLOBALS['sys_mail_replyto']."@".$GLOBALS['sys_mail_domain'],
                        $form_email,
                        $GLOBALS['sys_name']." "._("Account Registration"),
                        $message);

      $HTML->header(array('title'=>_("Register Confirmation")));

      print '<h3>'.$GLOBALS['sys_name'].' : '._("New Account Registration Confirmation").'</h3>'
          .sprintf(_("Congratulations. You have registered on %s "),$GLOBALS['sys_name'])
          .sprintf(_("Your login is: %s"), '<strong>'.user_getname($newuserid).'</strong>');

      print '<p>'._("You are now being sent a confirmation email to verify your email address. Visiting the link sent to you in this email will activate your account.").' <span class="warn">'._("Accounts not confirmed after two days are deleted from the database.").'</span></p>';
    }
}

# not valid registration, or first time to page
else

{

  site_header(array('title'=>_("User account registration"),'context'=>'account'));


  print form_header($_SERVER['PHP_SELF'], $form_id);
  print '<p><span class="preinput">'._("Login Name:").'</span><br />&nbsp;&nbsp;';
  print form_input("text", "form_loginname", $form_loginname);

  print '<p><span class="preinput">'._("Password / passphrase:")." ".account_password_help().'</span><br />&nbsp;&nbsp;';
  print form_input("password", "form_pw", $form_pw);
  print "</p>";

  print '<p><span class="preinput">'._("Re-type Password:").'</span><br />&nbsp;&nbsp;';
  print form_input("password", "form_pw2", $form_pw2);
  print "</p>";

  print '<p><span class="preinput">'._("Real Name:").'</span><br />&nbsp;&nbsp;';
  print '<input size="30" type="text" name="form_realname" value="'.$form_realname.'" /></p>';

  print '<p><span class="preinput">'._("Email Address:").'</span><br />&nbsp;&nbsp;';
  print '<input size="30" type="text" name="form_email" value="'.$form_email.'" />';
  print '<br /><span class="text">'._("This email address will be verified before account activation.").'</span></p>';
  
  if ($GLOBALS['sys_registration_captcha'])
    {
      print '<p><span class="captcha">';
      print '<a href="#" onclick="document.getElementById(\'captcha\').src = \'' .
               $GLOBALS['sys_home'] . 'gencaptcha.php?\' + Math.random(); return false">';
      print '<img id="captcha" src="' . $GLOBALS['sys_home'] . 'gencaptcha.php" alt="CAPTCHA" />';
      print '</a>';
      print '</span></p>';
      print _("Enter text from the image above:") . '<input type="text" name="captcha_code" size="10" maxlength="16" />';

  }

  # Extension for PAM authentication
  # FIXME: for now, only the PAM authentication that exists is for AFS.
  #  but PAM is not limited to AFS, so we should consider a way to configure
  #  this (to put it in site specific content probably).
  if ($sys_use_pamauth=="1")
    {
      print "<p>Instead of providing a new password you
      may choose to authenticate via an <strong>AFS</strong> account you own
      at this site (this requires your new login name to be the
      same as the AFS account name):";

      print '<p>&nbsp;&nbsp;&nbsp;<INPUT type="checkbox"
      name="form_usepam" value="1" > use AFS based authentication';
    }


  print form_footer();

}



$HTML->footer(array());
