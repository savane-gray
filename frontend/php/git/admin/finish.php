<?php
site_project_header(array('title'=>_("Git Repository Created"),
			  'group'=>$group_id,
			  'context'=>'agit'));

print '<p>'.sprintf(_('The sub-repository <i>%s</i> has been scheduled for creation in project <b>%s</b>.'),
		    $name, $group).'</p>';

utils_get_content('git/createinfo');

print '<p>'.sprintf(_('Return to %s'),
		    '<a href="/git/admin?group='.$group.'">'.
		    _("Git Configuration").'.</a></p>');

site_project_footer(array());

?>