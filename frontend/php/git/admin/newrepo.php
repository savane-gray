<?php
require_once('../../include/init.php');
require_once('../../include/vars.php');

extract(sane_import('post', array('update','create',
				  'form_name','form_owner','form_description',
				  'form_readme')));

session_require(array('group'=>$group_id,'admin_flags'=>'A'));

if (!$group_id)
  {
    exit_no_group();
  }

$project = project_get_object($group_id);

if (!$project->Uses("git"))
  {
    exit_error(_("This project has turned off this tool"));
  }

###########
unset($name);
unset($owner);
unset($description);
unset($readme);
$confirm = 0;

$name = $form_name;
if ($name)
  {
    $res = db_query_escape("SELECT name FROM git_repo WHERE group_id=%d AND name=\"%s\" AND master='N'", $group_id, $name);
    if (db_numrows($res) > 0)
      {
	fb(_("Repository with this name already exists"), 1);
	$create = 0;
	$confirm = 1;
      }
  }
else if ($update || $create || $confirm)
  fb(_("No name entered"), 1);
$owner = $form_owner;
$description = $form_description;
$readme = $form_readme;

if ($update || $confirm)
  {
    $confirm = 1;
  }
else if ($create)
  {
    $result = db_autoexecute('git_repo',
			     array('master' => "N",
				   'owner' => $owner,
				   'name' => $name,
				   'group_id' => $group_id,
				   'description' => $description,
				   'readme' => $readme,
				   'readme_html' => markup_full($readme),
				   'updated=' => 'now()'));
    if (!$result)
      fb(_("Insert failed."), 1);
    else
      {
	@include('finish.php');
	exit;
      }
  }
else
  {
    $owner = user_getname(user_getid(), 1);
  }

############
site_project_header(array('title'=>_("Create a Subordinate Git Repository"),
			  'group'=>$group_id,
			  'context'=>'agit'));

$res_repos = db_query_escape("SELECT name,description,master FROM git_repo WHERE group_id=%d AND master='N' ORDER BY 1", $group_id);
$rows = db_numrows($res_repos); 
if ($rows)
  {
    print '<h3>'._('Repositories already in use').'</h3>';
    for ($i = 0; $i < $rows; $i++)
      {
	print '<div class="'.utils_get_alt_row_color($i).'">'
	  . db_result($res_repos, $i, "name")
	  . '</div>';
	}
    }

if ($confirm)
  printf("<h2>%s</h2>", _("Confirm creating a new repository"));
else
  printf("<h2>%s</h2>", _("Create new repository"));

print form_header($_SERVER['PHP_SELF'])
  .form_input("hidden", "group_id", $group_id);

print '
<p><span class="preinput">'._("Name:").'</span>
<br />&nbsp;&nbsp;&nbsp;'.form_input("text",
				     "form_name",
				     $name,
				     'size="32"').'</p>';

print '
<p><span class="preinput">'.sprintf(_("Owner %s:"), markup_info("none", ", 255 Characters Max")).'</span>
<br />&nbsp;&nbsp;&nbsp;'.form_input("text",
				     "form_owner",
				     $owner,
				     'size="70"').'</p>';

print '
<p><span class="preinput">'.sprintf(_("Short Description %s:"), markup_info("none", ", 255 Characters Max")).'</span>
<br />&nbsp;&nbsp;&nbsp;'.form_textarea("form_description",
					htmlspecialchars($description),
					'cols="70" rows="1" wrap="virtual"').'</p>';

print '
<p><span class="preinput">'.sprintf(_("Long Description (README) %s:"), markup_info("full")).'</span>
<br />&nbsp;&nbsp;&nbsp;'.form_textarea("form_readme",
					htmlspecialchars($readme),
					'cols="70" rows="10" wrap="virtual"').'</p>';

if ($confirm)
  {
    print '<h3>'._("Preview:").'</h3>'
       . markup_full($readme);

    print '<div class="center">';
    print form_submit(_("Preview"), "confirm");
    print form_submit(_("Create!"), "create");
    print '</div>';
    print '</form>';
  }
else
  print form_footer();

############

site_project_footer(array());

?>