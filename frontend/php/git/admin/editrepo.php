<?php
require_once('../../include/init.php');
require_once('../../include/vars.php');

session_require(array('group'=>$group_id,'admin_flags'=>'A'));

extract(sane_import('post', array('update',
				  'owner',
				  'description',
				  'readme',
				  'repo_id',
				  'preview',
				  'changerepo')));

if (!$group_id)
  {
    exit_no_group();
  }

$project = project_get_object($group_id);

if (!$project->Uses("git"))
  {
    exit_error(_("This project has turned off this tool"));
  }

###########
$num_repos = 0;

if ($update)
  {
    group_add_history ('Changed Git Configuration','',$group_id);
    $result = db_autoexecute('git_repo',
			     array ('owner' => $owner,
				    'description' => $description,
				    'readme' => $readme,
				    'readme_html' => markup_full($readme),
				    'updated=' => 'now()'),
			     DB_AUTOQUERY_UPDATE,
			     "repo_id=?",
			     array($repo_id));
    
    if (!$result)
      { fb(_("Update failed."), 1); }
  }
else if ($preview)
  {
    /* skip */
  }
else
  {
    if ($repo_id)
      $res_grp = db_query_escape("SELECT * FROM git_repo WHERE repo_id=%d",
				 $repo_id);
    else
      $res_grp = db_query_escape("SELECT * FROM git_repo WHERE group_id=%d AND master='Y'", $group_id);
    if (db_numrows($res_grp) < 1)
      {
	if ($repo_id)
	  exit_error(_("The repository does not yet exist"));
	else
	  {
	    /* Missing master record is OK, it means the user did not
	       edit its info since enabling `Use Git' feature.
	       Just insert it. */
	    $owner = user_getname(user_getid(), 1);
	    $description = "";
	    $readme = "";
	    $result = db_autoexecute('git_repo',
				     array ('master' => "Y",
					    'owner' => $owner,
					    'name' => $group,
					    'group_id' => $group_id,
					    'updated=' => 'now()'));
	    if (!$result)
	      { 
		exit_error(_("Failed to create the repository record"));
	      }
	    $repo_id = db_insertid($result);
	  }
      }
    else
      {
	$row_grp = db_fetch_array($res_grp);
    
	$owner = $row_grp['owner'];
	$description = $row_grp['description'];
	$readme = $row_grp['readme'];
	$repo_id = $row_grp['repo_id'];
      }
    
    $res_repos = db_query_escape("SELECT repo_id,name,master FROM git_repo WHERE group_id=%d ORDER BY master,name ASC", $group_id);
    $num_repos = db_numrows($res_repos);
  }

############
site_project_header(array('title'=>_("Configure Git Repository"),
			  'group'=>$group_id,
			  'context'=>'agit'));

if ($num_repos > 1)
  {
    print form_header($_SERVER['PHP_SELF'])
          .form_input("hidden", "group_id", $group_id);
    print '
<p><span class="preinput">Select another repository:</span>
&nbsp;&nbsp;&nbsp;';
    print '<select name="repo_id">';
    for ($i = 0; $i < $num_repos; $i++)
      {
	$name = db_result($res_repos,$i,'name');
	$value = db_result($res_repos,$i,'repo_id');
	print '<option '.(($value == $repo_id)?'selected ':'').'value="'
              . $value
	      . '">';
	print $name;
	if (db_result($res_repos,$i,'master') == 'Y')
	  print ' (master)';
	print '</option>';
      }
    print '</select>';
    print form_submit(_("Select"),"changerepo");
    print '</form>';
  }

print form_header($_SERVER['PHP_SELF'])
     .form_input("hidden", "repo_id", $repo_id)
     .form_input("hidden", "group_id", $group_id);

print '
<p><span class="preinput">'.sprintf(_("Owner %s:"), markup_info("none", ", 255 Characters Max")).'</span>
<br />&nbsp;&nbsp;&nbsp;'.form_input("text",
				     "owner",
				     $owner,
				     'size="70"').'</p>';

print '
<p><span class="preinput">'.sprintf(_("Short Description %s:"), markup_info("none", ", 255 Characters Max")).'</span>
<br />&nbsp;&nbsp;&nbsp;'.form_textarea("description",
					$description,
					'cols="70" rows="1" wrap="virtual"').'</p>';

print '
<p><span class="preinput">'.sprintf(_("Long Description (README) %s:"), markup_info("full")).'</span>
<br />&nbsp;&nbsp;&nbsp;'.form_textarea("readme",
					htmlspecialchars($readme),
					'cols="70" rows="10" wrap="virtual"').'</p>';

if ($preview)
  {
    print '<h3>'._("Preview:").'</h3>'
       . markup_full($readme);
  }

print '<div class="center">';
print form_submit(_("Preview"), "preview");
print form_submit(false, "update");
print '</div>';
print '</form>';

site_project_footer(array());

?>
