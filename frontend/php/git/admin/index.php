<?php
require_once('../../include/init.php');
require_once('../../include/vars.php');

session_require(array('group'=>$group_id,'admin_flags'=>'A'));

if (!$group_id)
  {
    exit_no_group();
  }

$project = project_get_object($group_id);

if (!$project->Uses("git"))
  {
    exit_error(_("This project has turned off this tool"));
  }

site_project_header(array('group'=>$group_id,'context'=>'agit'));

utils_get_content("git/admin");

print '<br />
 <a href="editrepo.php?group='.$group.'">'._("Edit Repository Info").'</a><p class="text">'._("You can edit owner name and description of a repository using the web interface.").'</p>';
 	
print '<br />
 <a href="newrepo.php?group='.$group.'">'._("Create New Repository").'</a><p class="text">'._("Here you can create a subordinate Git repository for your project.").'</p>';

site_project_footer(array());


?>