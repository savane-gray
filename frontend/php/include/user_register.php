<?php
function user_register_confirm_mail ($confirm_hash, $krb5 = -1)
{
  # send mail
  $message = sprintf(_("Thank you for registering on the %s web site."),$GLOBALS['sys_name'])."\n"
	."("._("Your login is not mentioned in this mail to prevent account creation by robots").")\n\n"
	._("In order to complete your registration, visit the following URL:\n\n")
	. utils_make_href("account/verify.php?confirm_hash=$confirm_hash")
	. "\n\n"
	._("Enjoy the site").".\n\n"
	. sprintf(_("-- the %s team.")."\n",$GLOBALS['sys_name']);

  if ($krb5 == 0) #KRB5_OK
    {
      $message .= sprintf(_("P.S. Your kerberos password is now stored in encrypted form\nin the %s database."),$GLOBALS['sys_name']);
      $message .= sprintf(_("For better security we advise you\nto change your %s password as soon as possible.\n"),$GLOBALS['sys_name']);
    }
  return $message;
}
?>
