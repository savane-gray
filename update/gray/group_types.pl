#!/usr/bin/perl
# Copyright (C) 2016  Sergey Poznyakoff <gray@gnu.org>
#
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use Savane::Conf;
use Savane::Backend;
use Savane::DB;
use File::stat;
use Fcntl ':mode';
use Data::Dumper;

my %trans = (
    cvs => {
	basiccvs          => 'basic',          # Cvs::Basic
	cvsattic          => 'gna',            # Cvs::Gna
	'savannah-gnu'    => 'savannah',       # Cvs::Savannah
	'savannah-nongnu' => 'savannah'        # Cvs::Savannah
    },
    homepage => {
	basicdirectory    => 'basic',          # Homepage::Basic
	basiccvs          => 'cvs',            # Homepage::Cvs
	'savannah-gnu'    => 'savannah',       # Homepage::Savannah
	'savannah-nongnu' => 'savannah_nongnu',# Homepage::Savannah_nongnu
	svnatticwebsite   => 'gna'             # Homepage::Gna
    },
    download => {
	basicdirectory    => 'basic'           # Download::Basic
    },
    svn => {
	basicsvn          => 'basic',          # Svn::Basic
	svnattic          => 'gna'             # Svn::Gna
    },
    arch => {
	basicdirectory    => 'basic'
    }, 
    git  => {
	basicgit          => 'basic'           # Git::Basic
    },
    hg => {
	basichg           => 'basic'           # Hg::Basic
    },
    bzr => {
	basicbzr          => 'basic'
    }
);

sub build_sql_stmt {
    my $res = shift;
    $res =~ s/\?/"'".shift."'"/gex;
    return $res;
}

sub alter_table {
    my $stmt = qq{
ALTER TABLE group_type CHANGE COLUMN `dir_type_cvs` `dir_type_cvs` varchar(255) NOT NULL DEFAULT 'basic',
  CHANGE COLUMN `dir_type_arch` `dir_type_arch` varchar(255) NOT NULL DEFAULT 'basic',
  CHANGE COLUMN `dir_type_svn` `dir_type_svn` varchar(255) NOT NULL DEFAULT 'basic',
  CHANGE COLUMN `dir_type_git` `dir_type_git` varchar(255) NOT NULL DEFAULT 'basic',
  CHANGE COLUMN `dir_type_hg` `dir_type_hg` varchar(255) NOT NULL DEFAULT 'basic',
  CHANGE COLUMN `dir_type_bzr` `dir_type_bzr` varchar(255) NOT NULL DEFAULT 'basic',
  CHANGE COLUMN `dir_type_homepage` `dir_type_homepage` varchar(255) NOT NULL DEFAULT 'basic',
  CHANGE COLUMN `dir_type_download` `dir_type_download` varchar(255) NOT NULL DEFAULT 'basic'};

    debug($stmt);
    db_modify($stmt) unless dry_run();
};

my $force;

backend_setup(descr => 'upgrade group_type.dir_type_* columns',
	      options => { "force" => \$force }  );

my @updates;
my @errors;
my @warnings;

db_foreach(sub {
               my ($ref) = @_;
	       my @upd;
	       while (my ($feature, $table) = each %trans) {
		   my $column = "dir_type_$feature";
		   die unless exists $ref->{$column};
		   if (exists($table->{$ref->{$column}})) {
		       push @upd, { column => $column,
				    value => $table->{$ref->{$column}} };
		   } else {
		       if ($ref->{"can_use_$feature"}) {
			   push @errors, { name => $ref->{name},
					   column => $column,
					   value => $ref->{$column} };
		       } else {
			   push @warnings, { name => $ref->{name},
					     column => $column,
					     value => $ref->{$column} };
		       }			   
		   }
	       }
	       if (@upd) {
		   my $s = "UPDATE group_type SET";
		   my $d = ' ';
		   foreach my $x (@upd) {
		       $s .= $d;
		       $s .= "$x->{column}=?";
		       $d = ',';
		   }
		   $s .= ' WHERE name=?';
		   my @args = ($s, (map { $_->{value} } @upd), $ref->{name});
		   push @updates, \@args;
	       }
	   },
	   qq{SELECT name,
                     can_use_cvs,can_use_arch,can_use_svn,can_use_git,
	             can_use_hg,can_use_bzr,can_use_homepage,
	             can_use_download,
                     dir_type_cvs,dir_type_arch,dir_type_svn,dir_type_git,
	             dir_type_hg,dir_type_bzr,dir_type_homepage,
	             dir_type_download
 	      FROM group_type});
	      
if (@errors) {
    print STDERR "Table contains ".($#errors + 1)." values that cannot be converted:\n";
    printf STDERR "%-32s %-16s %s\n", 'Group', 'Column', 'Value';
    foreach my $err (@errors) {
	printf STDERR "%-32s %-16s %s\n",
	       $err->{name}, $err->{column}, $err->{value};
    }
    if ($force) {
	printf STDERR "continuing anyway...\n";
    } else {
	abend(EX_DATAERR, "can't continue");
    }
}
if (@warnings) {
    print STDERR "WARNING: Table contains ".($#warnings + 1)." values that cannot be converted:\n";
    printf STDERR "%-32s %-16s %s\n", 'Group', 'Column', 'Value';
    foreach my $ent (@warnings) {
	printf STDERR "%-32s %-16s %s\n",
	       $ent->{name}, $ent->{column}, $ent->{value};
    }
    print STDERR "Since these features are not used, it is not considered an error.\n";
    print STDERR "These columns are left unchanged.\n";
}

alter_table();
foreach my $upd (@updates) {
    if (debug_level()) {
	my $s = build_sql_stmt(@{$upd});
	debug($s);
    }
    db_modify(@{$upd}) unless dry_run();
}

backend_done;    
=head1 NAME

group_types.pl - update dir_type_ columns in group_type table

=head1 SYNOPSIS

B<perl group_types.pl>
[B<-F> I<FACILITY>]
[B<-dn>]
[B<--debug>]
[B<--dry-run>]
[B<--facility=>I<FACILITY>]
[B<--stderr>]
[B<--syslog>]
    
B<perl group_types.pl>
[B<-h>]
[B<--help>]
[B<--usage>]
    
=head1 DESCRIPTION

The B<group_types.pl> script replaces the values of the B<dir_type_*> columns
in the B<group_type> table of Savane database with the new values.

=head1 OPTIONS    

=over 8
    
=item B<-d>, B<--debug>

Increase debugging level.

=item B<-n>, B<--dry-run>

Do nothing, print everything.  Implies B<--debug>.

=item B<-F>, B<--facility=>I<NAME>

Log to the syslog facility I<NAME>.  If I<NAME> begins with a slash,
it is understood as the name of the file where to direct the logging
output.

=item B<--stderr>

Log to standard error.  This is the default.
    
=item B<--syslog>

Log to syslog.

=item B<-h>

Show a terse help summary and exit.

=item B<--help>

Prints the manual page and exits.

=item B<--usage>

Prints a summary of command line syntax and exits.

=back

=head1 CONFIGURATION VARIABLES

=over 4

=item B<sql.database>

=item B<sql.user>

=item B<sql.password>

=item B<sql.host>

=item B<sql.params>
    
Database credentials.    

=back

=head1 EXIT CODES

=over 8

=item 0

Successful termination.

=item 64

Command line usage error.

=item 65

Values of some columns cannot be translated.  The detailed diagnostics is
printed to standard error.    
    
=item 78

Configuration error.
    
=back
    
