use strict;
use warnings;
use Savane::Conf;
use File::Basename;
use Cwd 'abs_path';

my @new_tables = qw(email_black_list);

my $mysql_binary = 'mysql';
my @mysql_options;

my $dir = dirname(abs_path($0));
$dir =~ s{/update/gray$}{/db/mysql}
    or die "database definition files not found (from $dir)
";

my $v;

if ($v = GetConf('sql.host')) {
    push @mysql_options, '-h', $v;
}
if ($v = GetConf('sql.user')) {
    push @mysql_options, '-h', $v;
}
if ($v = GetConf('sql.password')) {
    push @mysql_options, '-p', $v;
}
# FIXME: GetConf('sql.params')
if ($v = GetConf('sql.database')) {
    push @mysql_options, $v;
}

foreach my $table (@new_tables) {
    my $file = $dir . '/table_' . $table . '.structure';
    system($mysql_binary, @mysql_options, '<', $file);
}


    

