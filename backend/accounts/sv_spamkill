#! /usr/bin/perl
# Copyright (C) 2017 Sergey Poznyakoff <gray@gnu.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use Getopt::Long;
use Savane;
use Savane::DB;
use Savane::Backend;
use Savane::Trackers;
use Data::Dumper;
use User::pwent;
use POSIX qw(strftime);
use Time::ParseDate;
use utf8;

my $minscore = 9;
my $sys_cron = 0;
my $mark_status = 'S';

backend_setup(descr => "Clean up spammers",
	      cron => $sys_cron,
              options => {
		  "score|s=n" => \$minscore,
		  "delete|D" => sub { $mark_status = 'D' }
	      });

sub format_user {
    my $user = shift;
    $user->{user_name}.': "'.$user->{real_name}.'" <'.$user->{email}.'>';
}

my @users;	      

AcquireReplicationLock('groups-users.lock');

db_foreach(sub {
    push @users, shift;
	   },
	   qq{
SELECT user.user_id AS user_id,
       user.user_name AS user_name,
       user.realname AS real_name,
       user.spamscore AS spamscore,
       user.email AS email,
       count(user_group.group_id) AS group_count
FROM user
LEFT JOIN user_group
ON user.user_id = user_group.user_id
WHERE user.status = 'A' AND user.spamscore > ?
GROUP BY user_id},
    $minscore);
foreach my $user (@users) {
    if ($user->{group_count}) {
	logit('info', 'skipping user '.format_user($user).', score '.$user->{spamscore}.': member of '.$user->{group_count}.' projects');
	next;
    }
    debug(format_user($user).': '.$user->{spamscore});
    next if dry_run;
    db_insert('email_black_list',
	      email => $user->{email},
	      reason => 'Blocked for spam ('.$user->{spamscore}.')');
    db_modify("UPDATE user SET status=? WHERE user_id=?",
	      $mark_status, $user->{user_id});
}
backend_done;

=head1 NAME

sv_spamkill - suspend spammers

=head1 SYNOPSIS

B<sv_spamkill>
[B<-F> I<FACILITY>]]
[B<-S> I<MINSCORE>]]    
[B<-Ddn>]
[B<--cron>]
[B<--debug>]
[B<--delete>]    
[B<--dry-run>]
[B<--facility=>I<FACILITY>]
[B<--stderr>]
[B<--syslog>]
[B<--score=>I<MINSCORE>]]

B<sv_spamkill> [B<-h>] [B<--help>] [B<--usage>]

=head1 DESCRIPTION

Suspends or deletes users with spamscore greater than I<MINSCORE> (default 9).

=head1 OPTIONS

=over 8

=item B<-D>, B<--delete>

Mark spammers as deleted instead of as suspended.

=item B<-s>, B<--score=I<MINSCORE>>

Set minimum spam score.

=item B<-d>, B<--debug>

Increase debugging level.

=item B<-n>, B<--dry-run>

Do nothing, print everything.  Implies B<--debug>.

=item B<-F>, B<--facility=>I<NAME>

Log to the syslog facility I<NAME>.  If I<NAME> begins with a slash,
it is understood as the name of the file where to direct the logging
output.

=item B<--stderr>

Log to standard error.  This is the default.
    
=item B<-h>

Show a terse help summary and exit.

=item B<--help>

Prints the manual page and exits.

=back

=head1 CONFIGURATION VARIABLES

=over 4
    
=item B<sql.database>

=item B<sql.user>

=item B<sql.password>

=item B<sql.host>

=item B<sql.params>

Database credentials.    

=back
    
=head1 AUTHOR

Sergey Poznyakoff <gray@gnu.org>
    
=cut    
    
