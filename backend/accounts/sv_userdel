#! /usr/bin/perl
use strict;
use warnings;
use Savane;
use Savane::Conf;
use Savane::Backend;
use Savane::LDAP;
use Savane::Homedir;

my $remove_option;
my $force_option;
backend_setup(descr => 'Delete savane user',
	      options => {
		  'remove|r' => \$remove_option,
		  'force|f' => \$force_option
	      });

my $name = shift @ARGV
    or abend(EX_USAGE, "no user to delete");
abend(EX_USAGE, "extra arguments") if @ARGV;

my $ldap = new Savane::LDAP;
my $usr = $ldap->getpwnam($name)
    or abend(EX_USAGE, "no such user");
my @groups = $ldap->user_groups($name);
unless ($force_option) {
    if (my @s = grep { @{$_->mem} == 1 } @groups) {
	logit('error',
	      "user $name is the only member of the following groups: "
	      . join(', ', map { $_->name } @s));
	abend(EX_UNAVAILABLE, "use --force to delete");
    }
}

while (my $grp = shift @groups) {
    my $mem = $grp->mem;
    foreach my $n (reverse grep { $mem->[$_] eq $name } (0 .. $#{$mem})) {
	splice @$mem, $n, 1;
    }
    $grp->mem($mem);
    $ldap->chgrent($grp);
}

my $r = $ldap->delete($usr);
if ($r) {
    if ($remove_option) {
	my $h = new Savane::Homedir($usr->name,
				    dir => $usr->dir,
				    report => 1);
	$r = $h->delete;
    }
}

exit(!$r);


