#! /usr/bin/perl
# Copyright (C) 2014-2016 Sergey Poznyakoff <gray@gnu.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use Savane;
use Savane::Backend;
use Savane::Conf;
use CGI;
use Sphinx::Search;
use GDBM_File;
use HTML::TreeBuilder;
use HTML::TreeBuilder::XPath;

=head1 NAME

sv_searchdoc.cgi - search in the project's documentation on Puszcza

=head1 SYNOPSIS

B<ScriptAlias     /searchdoc      "/srv/puszcza/cgi-bin/sv_searchdoc.cgi">

B<SetEnv SEARCHDOC_TEMPLATE "/var/lib/searchdoc/searchdoc.in">

=head1 DESCRIPTION

The B<sv_searchdoc.cgi> is a CGI utility for searches in documentation files of
projects hosted on Puszcza.  It interprets the B<template file> pointed to
by the environment variable B<SEARCHDOC_TEMPLATE> and sends the
result on its standard output.  During the interpretation, the following
special sequences are expanded:

=over 4

=item B<$q>

Replaced with the original query (the value of the B<q> parameter).
    
=item B<$p>

Replaced with the name of the project (the value of the B<p> parameter).

=item B<$t>

Replaced with the template file name (the value of the B<t> parameter).
    
=item B<@@result@@>

If the B<q> parameter is empty, expands to an empty string.  Otherwise, the
search is performed and B<@@result@@> is replaced with its results.    
    
=back    

The template file is supposed to define a form that sets the three parameters
B<p>, B<q> and (optionally) B<t>.  The simplest template file is:

  <html>
  <body>
    <form method="post" action="/searchdoc"
          enctype="application/x-www-form-urlencoded">
      <input id="searchinput" type="text" size="30"
             maxlength="255" name="q" value="$q" />
      <input type="submit" name="search" value="Search" />
      <input type="hidden" id="searchproject" name="p" value="$p" />
    </form>
  </body>
  </html>  
    
=head1 CONFIGURATION

The following variables from B</etc/savane/savane.conf> affect the
utility's behavior:

=over 4    

=item B<index.html_root>

Directory where the documentation sites are hosted.
    
=item B<index.index_dir>

Index directory.    

=back    

=head1 ENVIRONMENT

=over 4

=item B<SEARCHDOC_TEMPLATE>

Full pathname to the page template file.    
    
=back    

=head1 SEE ALSO

B<sv_docindex>(1), B<sv_reindex>(1).    
    
=head1 AUTHOR                                                                   
                                                                                
Sergey Poznyakoff, <gray@gnu.org>    
    
=cut

my $cgi = CGI->new;
my $params = $cgi->Vars;
my $index;
my $projroot;
my $projidx;
my %nametab;

# Parameters:
#   q       -  query
#   p       -  project name
#   t       -  template file
my $template;

binmode STDIN, ":encoding(utf8)";  
binmode STDOUT, ":encoding(utf8)";

# #########################################################################
#
# #########################################################################
sub abend {
    print <<EOT;
Status: 500 Internal error
Content-Type: text/html

<html>
<title>500 Internal error</title>
<body>
@_
</body>
</html>
EOT
;
    exit;
}    

sub pvars {
    print "<pre>";
    while (my ($k,$v) =  each %ENV) {
	print "$k=$v\n";
    }
    print "</pre>";
}

sub print_match {
    my ($id, $cl, $q) = @_;
    my $res;
    
    my $basename = $nametab{$id};

    my $tree = HTML::TreeBuilder::XPath->new();
    open(my $fd, "<:encoding(utf8)", "$projroot/$basename") or do {
	return $basename;
    };
    $tree->parse_file($fd);
    close($fd);

    my $title = $tree->findvalue('//title');

    # FIXME: Hardcoded prefix
    $res .= "<a href=\"/software/$params->{p}/$basename\">$title</a>";
    
    my @content = $tree->findnodes_as_strings('//body');
    $res .= '<p>';
    foreach my $e ($cl->BuildExcerpts(\@content, $index, $q, {limit => 256})) {
	$res .= join(' ', @$e);
    }
    $res .= '</p>';
    
    return $res;
}

sub dosearch {
    my $q = $params->{q};
    return unless defined($q);
    my $cl = Sphinx::Search->new();
    $cl->SetFieldWeights({title => 5, content => 1});
    $cl->SetSortMode(SPH_SORT_RELEVANCE);

    my $res = "";
    
    my $result = $cl->Query($q, $index);
    if (defined($result)) {
	$res .= "<div class=\"res\">\n";
	if ($#{$result->{matches}} >= 0) {
	    $res .= "<ol class=\"res\">\n";
	    foreach my $m (@{$result->{matches}}) {
		$res .= "<li>";
		$res .= print_match($m->{doc}, $cl, $q);
		$res .= "</li>";
	    }
	    $res .= "</ol>";
	} else {
	    $res .= "<h3>No matches found</h3>";
	}
	$res .= "</div>";
    } else {
	$res .= "<div class=\"err\">No matches found: " . $cl->GetLastError() . ".</div>\n";
    }
    return $res;
}

my %replvar = (
    result => \&dosearch
);

sub interpret {
    open(my $fd, "<", $template) or abend("can't open template");

    print $cgi->header(-charset=>'utf-8');
    while (<$fd>) {
	chomp;
 	s/@@(.*?)@@/&{$replvar{$1}} if defined($replvar{$1});/gex;
	s/\$(\w+)/$params->{$1};/gex;
	s/\${(\w+)}/$params->{$1};/gex;
	s/\$\$/\$/g;
	print "$_\n";
    }
    close($fd);
}
# #########################################################################
#
# #########################################################################

abend("project not defined") unless defined($params->{p});
$index = "$params->{p}_idx";

my $val = GetConf('index.html_root')
    or abend('index.html_root not set');
$projroot = "$val/$params->{p}";
my $val =  GetConf('index.index_dir')
    or abend('index.index_dir not set');
$projidx = "$val/$params->{p}";
tie %nametab, 'GDBM_File', $projidx.'_name.db', &GDBM_READER, 0644;
    
if (defined($params->{t})) {
    abend("malformed template") if ($params->{t} =~ /\.\./);
    $template = "$projroot/$params->{t}";
} elsif (defined($ENV{'SEARCHDOC_TEMPLATE'})) {
    $template = $ENV{'SEARCHDOC_TEMPLATE'};
} else {
    abend("no template defined");
}

interpret;

