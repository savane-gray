# This file is part of Savane.                  -*- perl -*-
# Copyright (C) 2015 Sergey Poznyakoff <gray@gnu.org>
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
use strict;
use File::Copy;

=head1 NAME

sv_setup.pl - set up UNIX groups and mysql database for Savane

=head1 SYNOPSIS

B<perl sv_setup.pl> I<TOPDIR> I<VAR>=I<VAL> [I<VAR>=I<VAL>...]

=head1 DESCRIPTION    

A helper program to be used at the last stage of installing a Savane
system.  I<TOPDIR> is the topmost directory of the Savane source
tree.  I<VAR>s are the B<SV_*> variables defined during the configuration.
Depending on the variables, the program performs the following tasks:

=over 4
    
=cut    

# #############################
# Parse command line
# #############################
die "not enough arguments; try perldoc $0 for more info" unless $#ARGV > 0;

die "must run as root" unless $> == 0;

my $topdir = shift;
my $dbdir = "$topdir/db/mysql";
die "$dbdir does not exist" unless -d $dbdir;

my %symtab;

foreach my $v (@ARGV) {
    my @a = split /=/, $v, 2;
    die "$v: unrecognized argument" if ($#a != 1);
    $symtab{$a[0]} = $a[1];
}


# #############################
# Create UNIX groups
# #############################

sub safe_create_group {
    my ($name, $opt) = @_;
    my $gid = getgrnam $name;
    if (defined($gid)) {
	print "$0: group $name exists\n";
    } else {
	print "$0: creating group $name\n";
	my $cmd = "groupadd $opt $name";
	system($cmd);
	if ($? == -1) {
	    exit 1;
	} elsif ($? & 127) {
	    die "$cmd died with signal " . ($? & 127);
	} elsif ($? >> 8) {
	    die "failed to create group $name";
	}
    }
}

=item B<SV_SITEADM_GROUP>=I<NAME>

Tests if the UNIX group I<NAME> exists, and creates it if it doesn't.
This is the primary group for the Savane site administration project.
=cut

safe_create_group($symtab{SV_SITEADM_GROUP})
    if defined $symtab{SV_SITEADM_GROUP};

=item B<SV_USERS_GROUP>=I<NAME>    

Tests if the UNIX group I<NAME> exists, and creates it if it doesn't.
This group will be used as the primary group for all Savane users.
=cut

safe_create_group($symtab{SV_USERS_GROUP})
    if defined $symtab{SV_USERS_GROUP};

sub copy_crontab_vixie {
    my ($ifd, $ofd) = @_;

    while (<$ifd>) {
	chomp;
	if (/^PATH=/) {
	    if (exists($symtab{SV_BINDIR})) {
		my $path = join(':',
				map { ($_ eq $symtab{SV_BINDIR}) ? () : $_ }
				  split /:/, $ENV{PATH});
		print $ofd "PATH=$symtab{SV_BINDIR}:$path\n";
	    }
	} elsif (/^##/) {
	    next;
	} else{
	    s/^#;/#/;
	    print $ofd "$_\n";
	}
    }
}

sub copy_crontab_dillon {
    my ($ifd, $ofd) = @_;
    my $bindir = "$symtab{SV_BINDIR}/" if exists $symtab{SV_BINDIR};
    while (<$ifd>) {
	chomp;
	if (/^PATH=/) {
	    next;
	} elsif (/^##/) {
	    next;
	} elsif (/(^[^#]\S*\s+(?:\S+\s+){4})root\s+(sv_.+)/) {
	    print $ofd "${1}$bindir$2\n";
	} elsif (/(^#;)(\S+\s+(?:\S+\s+){4})root\s+(sv_.+)/) {
	    print $ofd "#${2}$bindir$3\n";
	} else {
	    print $ofd "$_\n";
	}
    }
}

my %copy_crontab = (
    vixie => \&copy_crontab_vixie,
    dillon => \&copy_crontab_dillon
);

sub find_crond {
    open(my $fd, '-|', 'ps -e -o user,pid,ppid,command') or do {
	warn "can't run ps: $!";
	return undef;
    };
    my ($found_pid, $found_cmd);
    while (<$fd>) {
	chomp;
	my ($user,$pid,$ppid,$cmd) = split /\s+/;
	if ($user eq 'root' and $ppid == 1 and $cmd =~ /cron/i) {
	    ($found_pid, $found_cmd) = ($pid,$cmd)
		unless (defined($found_pid) and $found_pid < $pid);
	}
    }
    close($fd);
    
    if (defined($found_pid)) {
	return $found_cmd if ($found_cmd =~ /^\//);
	my $exe = "/proc/$found_pid/exe";
	return readlink($exe) if -f $exe;
    }

    warn "cron not running";
    return undef;
}

sub scan_crond {
    my $cmd = shift;
    open(my $fd, '-|', "strings $cmd") or do {
	warn "can't run strings $cmd: $!";
	return undef;
    };
    my $ret;
    while (<$fd>) {
	chomp;
	if (/dillon's\s+cron/) {
	    $ret = 'dillon';
	    last;
	} elsif (/^\@\(#\) Copyright.*by Paul Vixie/) {
	    $ret = 'vixie';
	    last;
	}
    }
    close($fd);
    return $ret;
}

sub cron_flavor {
    my $cmd = find_crond;
    return scan_crond($cmd) if defined $cmd;
    return undef;
}

=item B<SV_CRONTAB=>I<FILE>

Install Savane crontab from I<FILE>.  Filename is given relative to
I<TOPDIR>.  The following variables are also used:

=over 8    
    
=item B<SV_CRONTAB_DIR=>I<DIR>

System crontab directory where to install Savane crontab.  Defaults to
F</etc/cron.d>.

=item B<SV_CROND_TYPE=>B<dillon>|B<vixie>

Type of cron daemon in use.  Normally it is determined automatically.
    
=item B<SV_BINDIR=>I<DIR>

Directory where sv_* files are installed.  If defined, this value will be
added at the start of PATH= assignment for Vixie crontab, or prepended (with
a trailing slash) to the names of sv_* binaries for Dillon crontab file.

=back
    
=cut

sub install_crontab {
    $symtab{SV_CRONTAB_DIR} = '/etc/cron.d'
	unless defined $symtab{SV_CRONTAB_DIR};

    if (defined($symtab{SV_CROND_TYPE})) {
	$symtab{SV_CROND_TYPE} = lc $symtab{SV_CROND_TYPE};
    } else {
	$symtab{SV_CROND_TYPE} = cron_flavor;
	unless (defined($symtab{SV_CROND_TYPE})) {
	    print "$0: can't determine cron daemon type\n";
	    return;
	}
    }
	
    die "unknown cron type: $symtab{SV_CROND_TYPE}"
	unless exists $copy_crontab{$symtab{SV_CROND_TYPE}};

    my $outfile = "$symtab{SV_CRONTAB_DIR}/savane";
    if (-e $outfile) {
	print "$0: $outfile already exists\n";
	return;
    }
    
    open(my $ifd, '<', "$topdir/$symtab{SV_CRONTAB}")
	or die "can't open input file $topdir/$symtab{SV_CRONTAB}: $!";
    
    open(my $ofd, '>', $outfile)
	or die "can't open $outfile for writing: $!";

    &{$copy_crontab{$symtab{SV_CROND_TYPE}}}($ifd, $ofd);
    
    close $ifd;
    close $ifd;
}

install_crontab
    if exists $symtab{SV_CRONTAB};

# #############################
# Create database
# #############################

=item B<SV_DB_NAME>=I<NAME>    

If defined, I<NAME> supplies the name of the Savane mysql database.
The program will attempt to create the database and populate it with
the initial data.  The following variables will be used during this
process:

=over 8

=item B<SV_DB_CMD>=I<PROG>

Full pathname of the B<mysql> client utility.

=item B<SV_DB_OPTIONS>=I<STRING>

Additional options to pass to B<mysql>.

=back

=cut    
    
sub catfiles {
    my $fd = shift;
    # Flush the buffer
    my $h = select($fd); 
    $|=1;
    $|=0; 
    select($h);
    # Copy all files to $fd
    foreach my $file (@_) {
	copy $file, $fd or die "failed to pipe $file: $!";
    }
}

sub interpret {
    my $ofd = shift;
    foreach my $file (@_) {
	open(my $fd, '<', $file) or die "can't open $file: $!";
	while (<$fd>) {
	    s/\$\{(\w+)\}/$symtab{$1}/g;
	    print $ofd $_;
	}
	close $fd;
    }
}

if (defined($symtab{SV_DB_NAME})) {
    my $dbname = $symtab{SV_DB_NAME};
    my $mysql = $symtab{SV_DB_CMD} || "mysql";
    $mysql .= ' ' . $symtab{SV_DB_OPTIONS} if defined $symtab{SV_DB_OPTIONS};
    
    # Check if the database exists
    open(my $fd, '-|', "$mysql --raw -N -B -e 'SHOW DATABASES'")
	or die "can't run $mysql";
    my $res = grep { chomp; $_ eq $dbname } <$fd>;
    close $fd;
    if ($res) {
	print "$0: database $dbname already exists\n";
	exit(0);
    }
    
    # Create it

    print "$0: Creating database $dbname\n";
    
    open($fd, '|-', $mysql)
	or die "can't run $mysql";
    print $fd "CREATE DATABASE $dbname DEFAULT CHARACTER SET utf8;\n";
    print $fd "USE $dbname;\n";
    catfiles($fd, glob "$dbdir/table_*.structure");
    interpret($fd, glob "$dbdir/table_*.initvalues");
    interpret($fd, "$dbdir/bootstrap.sql");
    close $fd;
}
__END__
=back

=head1 AUTHOR

Sergey Poznyakoff <gray@gnu.org>

=cut    
