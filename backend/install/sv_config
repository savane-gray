#!/usr/bin/perl
# Copyright (C) 2015, 2016  Sergey Poznyakoff <gray@gnu.org>
#
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;

use Getopt::Long qw(:config gnu_getopt no_ignore_case);
use Text::Wrap qw(&wrap $columns);
use File::Basename;
use File::Temp qw(tempfile);
use File::Path qw(make_path);
use File::Copy;
use File::Find;
use Pod::Usage;
use Pod::Man;
use Carp;
use Data::Dumper;
use Savane::Setup;
use Savane::Config;
use Savane::Conf qw(:all);

=head1 NAME

sv_config - Create and edit Savane configuration file

=head1 SYNOPSIS

B<sv_config>
[B<-t>]    
[B<--confdir=>I<DIR>]
[B<--conffile=>I<FILE>]
[B<--http-user=>I<NAME>]
[B<--lint>]    
[B<--translate>]
[B<--update>]    
    
B<sv_config>
B<-h> | B<--help> | B<--usage>    

=head1 DESCRIPTION

Creates and maintains Savane configuration file.

Savane uses two configuration files: one for backend and another for frontend.
Both files are valid sources written in the language of their corresponding
Savane component: Perl for backend, and PHP for frontend.  Backend
configuration file is the principal one and serves as a source for creating
frontend configuration.  Apart from being written in different languages,
both files contain the same settings.

The B<sv_config> utility copies the main configuration to a temporary file
and opens it in the editor of user's choice (as determined by B<VISUAL> and
B<EDITOR> environment variables) and allows user to make arbitrary changes
to it.  When the user leaves the editor, B<sv_config> checks the syntax and
consistency of the file.  If it encounters any problems, it asks the user
whether to edit file again or to quit it.  If the user select the former,
B<sv_config> marks erroneous fragments of the file with prominent diagnostic
messages and starts the editor again.  This process is continued until either
the resulting file contains no errors or the user chooses to abort the
editing.

If the file contains no errors, it is copied back to the main configuration
file, and is translated to PHP frontend configuration file.

If the main configuration file does not exist, it will be created.
    
Thus, B<sv_config> ensures that changes to the configuration files affect
Savane only when confirmed to be clean of errors and that both backend
and frontend configuration files remain in sync.    
    
Unless told otherwise, B<sv_config> operates on configuration files in
F</etc/savane> directory (see the B<FILES> section).  There are several
ways to change this behavior.    

If the B<--conffile> option is given, its value supplies the full pathname
of the main configuration file.  The directory part of that value is then
used as the configuration directory.    

Otherwise, if the B<--confdir> option is given, its value is used as the
name of the configuration directory.

Finally, the B<SAVANE_CONF> environment variable is consulted.  If defined,
its value is used as the name of the configuration directory.    
    
=head1 OPTIONS

=over 4

=item B<--conffile=>I<FILE>

Full name of the main configuration file.
    
=item B<--confdir=>I<DIR>

Full name of Savane configuration directory.    
    
=item B<--http-user=>I<NAME>

Name of the user httpd server runs as.

=item B<-t>, B<--lint>

Checks main configuration file for syntax errors, undefined variables
and the like.  Exits with code 0 if no errors were encountered.  Otherwise,
lists the errors on the standard error and exits with code 1.    
    
=item B<--translate>

Translate existing main configuration to php include file.  This is always
done at the end of the editing.  This option can be needed only if the
php file was accidentally deleted or botched in some way.    

=item B<--update>

Update existing configuration file with new or missing settings.  Use this
option after upgrading your Savane installation.    
    
=back    

The options B<--lint>, B<--translate> and B<--update> are mutually exclusive,
and cannot be used together.    
    
=head1 ENVIRONMENT

The editor to run is determined by consulting the following environment
variables in that order:    
    
=over 4

=item B<VISUAL>

=item B<EDITOR>

=back    

If neither of them is defined, B<vi> is used.

=over 4

=item B<SAVANE_CONF>

The value of this variable overrides the default configuration directory.    

=item B<SAVANE_CONFIG_CHECK_MASK>

Disables one or more checks that are normally performed on the configuration
file during its verification. The value is a comma-separated list of the
check names to disable. Allowed names are:

=over 8

=item files

Don't check for existence of files and directories mentioned in the
configuration file. Use this if backend and frontend are located on
different servers.

=item users

Don't check for existence of users and groups.

=item all

Disable all above checks.

=back

=back
    
=head1 FILES    

In the table below, B<CONFDIR> stands for Savane configuration directory
set at installation time (F</etc/savane> by default, unless overridden by
the B<--savane-conf-dir> option).  If the B<SAVANE_CONF> environment
variable is set, its value is used instead.
    
=over 4

=item F<B<CONFDIR>/savane.conf>

Main configuration file.
    
=item F<B<CONFDIR>/.savane.conf.cache>

Configuration cache file.  It is updated by B<sv_config>.
    
=item F<B<CONFDIR>/.savane.conf.php>

Frontend configuration file.    

=back
    
=head1 AUTHOR
    
Sergey Poznyakoff <gray@gnu.org>
    
=cut    

my $mode = 0750; # -rwxr-x---
my $http_user = setupconf(SV_HTTPD_USER);
my $http_gid;

sub Wrap {
    return wrap("", "", @_);
}

my $progname = basename($0);
my $progdescr = "Create and edit Savane configuration file";
my $phpconfname = '.savane.conf.php';
my $conffile;
my $confdir = $ENV{SAVANE_CONF} || setupconf(SV_CONFIG_DIR);

sub locate_template {
    my $ifd = shift;
    
    my $line;
    while (<$ifd>) {
	++$line;
	chomp;
	s/\s+$//;
	return $line if /^__END__$/;
    }
    return 0;
}

sub fix_file_access {
    my $file = shift;
    if ($> == 0) {
	chown 0, $http_gid, $file or
	      die "can't chown $file: $!";
    }
    chmod $mode, $file or
	die "can't chmod $file: $!";
}

my $temp_name;

sub cleanup {
    unlink $temp_name if (defined($temp_name) and -e $temp_name);
}

$SIG{HUP} = $SIG{INT} = $SIG{QUIT} = $SIG{TERM} = \&cleanup;
END { cleanup }

sub copy_template {
    my $output_name = shift;
    my $ofd;
    ($ofd, $temp_name) = tempfile("$output_name.XXXXXX")
	or die "can't open temporary file: $!";
    fix_file_access($temp_name);
    
    open(my $ifd, '<', $0) or
        die "can't open $0 for reading";
    my $line = locate_template($ifd);
    die "no template found in $0" unless $line > 0;
    
    my %macro = (PERL => eval {
	                    use Config;
			    $Config{perlpath};
		 },
		 SCRIPT => basename($0),
		 CONFIG => $conffile,
		 CONFDIR => $confdir,
		 HOSTNAME => `hostname|tr -d`); # FIXME

    while (<$ifd>) {
	++$line;
	chomp;
	s/@(\w+)@/defined($macro{$1}) ? $macro{$1} : ''/gex;
	s/(SV_[A-Z_]+)\(\)/setupconf($1)/gex;
	print $ofd "$_\n";
    }
    close $ifd;
    close $ofd;

    rename $temp_name, $output_name
	or die "can't rename $temp_name to $output_name: $!";    
}

sub oldvar {
    my $kw = find_keyword(@_);
    return $kw if $kw and exists($kw->{oldvar});
    return undef;
}

sub expandvar {
    my $vref = shift;
    my $name = shift;
    my $indent = shift;
    my $oldvar = oldvar(@_, $name);

    if (defined($oldvar)) {
	my $varname = $oldvar->{oldvar};
	if ($varname =~ /^\@/) {
	    if (exists($oldvar->{repr})) {
		$$vref = &{$oldvar->{repr}}(eval "our $varname; $varname");
	    } else {
		$$vref = eval "our $varname; join(' ', $varname)";
	    }
	    return 1;
	} elsif (eval "our $varname; defined($varname)") {
	    $$vref = eval "our $varname; $varname";
	    $$vref = &{$oldvar->{repr}}($$vref) if exists $oldvar->{repr};
	    return 1;
	}
    } else {
	my $section = find_keyword(@_);
	if (ref($section) eq 'HASH'
	    and exists($section->{oldvar})
	    and exists($section->{section})) {

	    my $varname = $section->{oldvar};

	    if ($varname =~ s/^%//
		and eval "our %$varname; exists(\$${varname}{$name})") {
		$$vref = eval "our \%$varname; \${$varname}{$name}";
		return 1;
	    }
	}
    }
    
    return 0;
}
    

sub convert_old_variables {
    my ($source, $conffile) = @_;

    open(my $ifd, '<', $0) or
        die "can't open $0 for reading";
    my $line = locate_template($ifd);
    die "no template found in $0" unless $line > 0;

    my ($ofd, $temp_name) = tempfile("$conffile.XXXXXX")
	or die "can't open temporary file: $!";
    fix_file_access($temp_name);

    my %macro = (PERL => eval {
	                    use Config;
			    $Config{perlpath};
		 },
		 SCRIPT => basename($0),
		 CONFIG => $conffile,
		 HOSTNAME => `hostname | tr -d '\n'`);
    my $line = 0;
    my @section;
    while (<$ifd>) {
	++$line;
	chomp;
	
	s/@(\w+)@/defined($macro{$1}) ? $macro{$1} : ''/gex;
	if (/^\[(.+)\]\s*$/) {
	    @section = split /\s+/, $1;
	    print $ofd "$_\n";
	} elsif (/^(\s+)([\w_-]+)\s*=\s*(.*)/) {
	    my ($i, $k, $v) = ($1, $2, $3);
	    expandvar(\$v, $k, $i, @section);
	    print $ofd "$i$k = $v\n";
	} elsif (/^(\s+)#([\w_-]+)\s*=/) {
	    my ($i, $k, $v) = ($1, $2);
	    if (expandvar(\$v, $k, $i, @section)) {
		print $ofd "$i$k = $v\n";
	    } else {
		print $ofd "$_\n";
	    }
	} else {
	    print $ofd "$_\n";
	}
    }
    close $ifd;
    close $ofd;

    rename $temp_name, $conffile
	or die "can't rename $temp_name to $conffile: $!";
}

sub getyn {
    my $text = shift;

    print STDERR Wrap($text)."\n" if defined $text;
    print STDERR "edit again [Y/n]?";

    while (<>) {
	chomp;
	s/^\s+//;
	return 1 if /^[yY]/ || /^$/;
	return 0 if /^[nN]/;
	print STDERR "please answer 'yes' or 'no': ";
	print STDERR "edit again?";
    }
}

sub format_diag {
    my ($ofd, $diag) = @_;
    foreach my $l (split /\n/, Wrap($diag)) {
	print $ofd "#>> $l\n";
    }
}

sub file_annotate {
    my ($ifile, $errlog) = @_;
    open(my $ifd, '<', $ifile)
	or die "can't open $ifile for reading: $!";
    my ($ofd, $ofile) = tempfile("$conffile.XXXXXX")
	or die "can't open temporary file: $!";

    if ($#{$errlog} >= 0) {
	format_diag($ofd,
		    "SOME VARIABLES IN THIS FILE ARE NOT DEFINED OR SET TO VALUES THAT ARE NOT CORRECT. SUCH VARIABLES ARE PRECEDED BY DESCRIPTIVE DIAGNOSTIC MESSAGES, THAT START WITH #>> MARKERS. PLEASE, FIX THESE SETTINGS.");
    }

    my $line = 0;
    my $anno = pop @{$errlog};
    while (<$ifd>) {
        ++$line;
	chomp;

        if (defined($anno) and $anno->[1] == $line) {
            format_diag($ofd, $anno->[2]);
            $anno = pop @{$errlog};
	} elsif (/^#>>/) {
	    next;
	}
	print $ofd "$_\n";
    }
    close $ifd;
    close $ofd;
    unlink $ifile;
    return $ofile;
}

my $editor = $ENV{'VISUAL'} || $ENV{'EDITOR'} || 'vi';

sub edit_and_eval {
    my ($ofd, $name) = @_;

    if (defined($ofd)) {
	$temp_name = $name;
    } else {
	($ofd, $temp_name) = tempfile("$conffile.XXXXXX")
	    or die "can't open temporary file: $!";
    }
    
    copy $conffile, $ofd or
	die "can't create temporary file $temp_name: $!";
    close $ofd;
    my $annotate = 0;
    while (1) {
	system("$editor $temp_name");
	if ($? == -1) {
	    die "failed to execute $editor: $!";
	} elsif ($? & 127) {
	    die "$editor died with signal " . ($? & 127) . ' '.
		(($? & 128) ? 'with' : 'without') .
		' coredump';
	} elsif ($? >> 8) {
	    die "$editor exited with code " . ($? >> 8);
	}

	my @errlog;
	my $conf = Parse($temp_name,
			 error => sub {
			     my $text = shift;
			     if ($text =~ /^([^:]+):(\d+):\s*(.*)/) {
				 push @errlog, [ $1, $2, $3 ];
			     } else {
				 # FIXME
				 push @errlog, [ undef, undef, $text ];
			     }
			 });

	unless ($conf) {
	    unless (getyn "Errors in config file") {
		print STDERR "$progname: configuration file left unchanged\n";
		exit(0);
	    }
	    $annotate = 1;
	    $temp_name = file_annotate($temp_name, \@errlog);
	    next;
	}

	last;
    }

    # Remove leftover annotations, now that the file is OK.
    $temp_name = file_annotate($temp_name, []) if $annotate;

    my $bak = "$conffile~";
    unlink $bak if -e $bak;
    rename $conffile, $bak
	or die "can't rename $conffile to $bak: $!";
    rename $temp_name, $conffile
	or die "can't rename $temp_name to $conffile: $!";
    fix_file_access($conffile);

    # Now parse the config once again to make sure cache is created.
    my $conf = Parse($conffile, cache => 1) or die; # shouldn't happen.
}

sub lint {
    my $file = shift;
    my $conf = Parse($file,
		     cache => 1,
		     error => sub {
			 my $text = shift;
			 print STDERR "$text\n";
		     }) or exit(1);
    exit(0);
}    

sub scan_features_dir {
    my ($dir,$h) = @_;
    my $sdir = "$dir/Savane/Feature";
    return unless -d $sdir;
    find({wanted => sub {
	     next unless s{^$sdir/(.+)\.pm$}{$1};
	     s#/#::#g;
	     my $pack = "Savane::Feature::$_";
	     next if exists $h->{$_};
	     $h->{$_} = eval "use $pack; \$${pack}::description";
	  },
	  no_chdir => 1
	 },
	 $sdir);
}

sub scan_features {
    my %tab;
    foreach my $dir (@INC) {
	scan_features_dir($dir, \%tab);
    }

    my %h;
    while (my ($pack, $descr) = each %tab) {
	my @p = split /::/, $pack, 2;
	next unless $#p == 1;
	$h{lcfirst($p[0])}->{lcfirst($p[1])} = $descr;
    }
    return %h;
}

sub php_feature_list {
    my $fd = shift;
 
    my %h = scan_features;
    
    my $s = '$sys_features = array(';
    my $delim;
    while (my ($feature, $types) = each %h) {
	$s .= $delim;
	$s .= "'$feature' => array(";
	my @a;
	while (my ($pack, $descr) = each %{$types}) {
	    push @a, "'$pack' => '$descr'";
	}
	$s .= join(',', @a);
	$s .= ')';
	$delim = ',';
    }
    $s .= ");\n";
    
    print $fd $s;
}

sub conf2php {
    my ($src, $dst) = @_;

    my $conf = Parse($src, cache => 1) or die;

    my %defn;
    for_each_keyword(sub {
	my $descr = shift;

	return unless ref($descr) eq 'HASH';
	return unless $descr->{php};

	if ($conf->isset(@_)) {
	    my $x = $conf->get(@_);
	    $x =~ s/'/\'/g;
	    $defn{$descr->{oldvar}} = $x;
	} else {
	    $defn{$descr->{oldvar}} = undef;
	}
    });
    
    my $ofd;
    ($ofd, $temp_name) = tempfile("$dst.XXXXXX")
	or die "can't create temporary file";
    if ($> == 0) {
	chown 0, $http_gid, $temp_name or
	      die "can't chown $temp_name $!";
    }
    chmod $mode, $temp_name or
	die "can't chmod $temp_name $!";
    
    open(my $ifd, '<', $src) or
        die "can't open $src for reading";

    print $ofd <<EOT;
<?php
// DO NOT MODIFY THIS FILE
// Instead, modify $conffile using:
//  $progname --confdir=$confdir
// To re-create from existing $conffile, run
//  $progname --confdir=$confdir --translate
EOT
;
    foreach my $var (sort keys %defn) {
	my $name = $var;
	my $value = $defn{$var};
	if (defined($value)) {
	    if ($name =~ s/^\@/\$/) {
		$value = 'array('
		         . join(',', map { "'$_'" } split /\s+/, $value)
			 . ')';
	    } else {
		$value = "'$value'";
	    }
	    print $ofd "$name = $value;\n";
	} else {
	    $name =~ s/^\@/\$/;
	    print $ofd "// $name undefined\n";
	}
    }

    php_feature_list($ofd);
    
    print $ofd '?>';
	
    close $ifd;
    close $ofd;

    system("php -l $temp_name >/dev/null");
    if ($? == -1) {
	die "failed to execute php: $!";
    } elsif ($? & 127) {
	die "php died on signal " . ($? & 127);
    } elsif ($? >> 8) {
	my $name = $temp_name;
	$temp_name = undef;
	die "created file \"$name\" contains errors: please report";
    }
    
    if (-e $dst) {
	unlink "$dst~";
	rename $dst, "$dst~"
	    or die "can't rename $dst to $dst~: $!";
    }
    
    rename $temp_name, $dst
	or die "can't rename $temp_name to $dst: $!";
    fix_file_access($dst);
}

# #######

use constant OP_EDIT      => 0;
use constant OP_TRANSLATE => 1;
use constant OP_LINT      => 2;
use constant OP_UPDATE    => 3;

my $operation = OP_EDIT;

GetOptions("h" => sub {
               pod2usage(-message => "$progname: $progdescr",
			 -exitstatus => 0);
           },
           "help" => sub {
	       pod2usage(-exitstatus => 0, -verbose => 2);
           },
           "usage" => sub {
	       pod2usage(-exitstatus => 0, -verbose => 0);
           },
	   "conffile=s" => \$conffile,
	   "confdir=s" => \$confdir,
	   "http-user=s" => \$http_user,
	   "translate" => sub { $operation = OP_TRANSLATE },
	   "update" => sub { $operation = OP_UPDATE },
	   "lint|t" => sub { $operation = OP_LINT }
    ) or do {
	print STDERR "Try $progname --help for more info\n";
	exit(1);
};

if ($#ARGV != -1) {
    print STDERR "Too many arguments\n";
    print STDERR "Try $progname --help for more info\n";
    exit(1);
}

$confdir = dirname($conffile)
    if $conffile;
$conffile = $confdir."/savane.conf"
    unless defined $conffile;
my $conffile_phpcopy = $confdir."/.savane.conf.php";

lint($conffile) if $operation == OP_LINT;

unless (-d $confdir) {
    make_path($confdir) or
	die "can't create directory $confdir";
}
die "can't write to $confdir" unless -w $confdir;

(undef,undef,undef,$http_gid) = getpwnam($http_user) or
    die "$http_user: no such user";

my $source;

if (-e $conffile) {
    # FIXME
    if ($operation == OP_UPDATE) {
	my ($fd, $name) = tempfile("$conffile.XXXXXX")
	    or die "can't open temporary file: $!";
	edit_and_eval($fd, $name);
	$operation = OP_TRANSLATE;
    }
} elsif (-e "${conffile}.pl") {
    $source = "${conffile}.pl";
    my $res = do $source;
    if (!defined($res)) {
	die "can't source $source: $@" if ($@);
	die "can't read $source: $!";
    }
    convert_old_variables($source, $conffile);
} else {
    copy_template($conffile);
}

edit_and_eval unless $operation == OP_TRANSLATE;
conf2php($conffile, $conffile_phpcopy);

if (defined($source)) {
    my $suf = 'OLD';
    while (-e "$source.$suf") {
	$suf .= '~';
    }
    rename $source, "$source.$suf"
	or die "can't rename $source to $source.$suf: $!";
    print "old configuration file $source renamed to $source.$suf; please remove it\n";
}
__END__
# Savane Site Configuration.
#
# This file MUST be edited with @SCRIPT@.  Failure to do so may result in
# syntax, data type or file permission errors which will render Savane
# unusable or vulnerable.
#
[core]
	# A default HTTP domain.
	# Example: "savannah.gnu.org"
	domain = @HOSTNAME@

	# Default HTTPS domain
	# Example: "savannah.gnu.org"
	https_host = @HOSTNAME@

	# HTTPS port number
	#https_port = 443

	# Brother HTTP(s) domain
	# You can run Savane with two different domain names. You'll be able
	# to write a different configuration for each one.
	# The two brother/companion sites will share the same database.
	# Here you can let your Savane installation aware of the existence
	# of a brother/companion site, so while people login, it will be
        # allowed to them to login on both sites in one click.
	# If you do not have brother/companion site, comment out.
	#
	#brother_domain = 

	# Default web directory.
	# Example: "/", "/savane"
	top_url = /

	# Default locale name.
	locale  = en_US.UTF-8

	# Date format.
	# Savane normally does a pretty good job at providing a correctly
	# localized date. This option allows you to force a specific date
	# format.  Unless you really know what you are doing, leave this
	# commented out.
	#date_format = "%Y-%m-%d %H:%M:

	# Use mediawiki instead of the built-in cookbook.
	# If you wish to use MediaWiki instead of the Savane built-in Cookbook
	# facilities, define this to the URL under which the MediaWiki
        # installation is available.
	#
	# You can obtain Mediawiki from http://mediawiki.org
	#
	#wiki_url =

	# File upload size limit.
	# People can attach files when posting items to trackers.
	# The default size limit for attached file is 512kB.
	# If it is too much or not enough for your installation, use this
	# setting.
	# Note that if you do, you must make sure it does not contradict 
	# PHP setup, most notably upload_max_filesize and your MySQL daemon
	# setup, most notably max_allowed_packet.
	#
	#upload_max =

[sql]
	# SQL server
	# Name of the server running the MySQL database.
	# Example: "localhost", "savane", "mysqluser", "mysqlpasswd",
	# [REQUIRED]
	host = localhost

	# Database name
	# [REQUIRED]
	database = savane

	# Name of the MySQL user that can access and write to the database.
	# [REQUIRED]
	user = savane

	# Password associated to the MySQL user account that can access and
	# write to the database.
	#password =

	# Connection parameters
	# Additional parameters needed to connect to the database.
	# A list of param=value pairs separated by colons.
	# Example: mysql_socket=/path-to/mysqld.sock:otherparam=value
	# Leave empty if not applicable.
	#
	#params =

[path]
	## INSTALLATION PATHS ##

	# Local directory of the installation of the PHP frontend
	# In the source package, it is the directory frontend/php.
	# IT MUST BE AN ABSOLUTE PATH NAME.
	# Example: /usr/src/savane/frontend/php
	# [REQUIRED]
	#
        top_directory = SV_DOCUMENT_ROOT()

	# Local directory of the site-specific content.
	# In the source package, it is the directory etc/site-specific-content.
	# This directory contains files you can modify to customize pages of
        # your Savane installation.
	# Example: "/etc/savane/savane-content"
	# [REQUIRED]
	#
	include_directory = CONFDIR/content

	# Application-specific data directory.
	# This is a directory where Savane stores various application-specific
	# files. Currently it is used as the base directory for storing files
        # attached to tracker comments.
	#
	application_directory = /var/lib/savane

	# Directory for tracker attachments.
	# Attachments submitted to various Savane trackers will be stored in
        # this directory.
	#
	attachment_directory = /var/lib/savane/trackers_attachments

	# Platform name, server administration project.
	# This is the name shown on public pages for the whole service.
	# [REQUIRED]
	#
	common_name = New Savane Installation

	# Server administration project unix name.
	# Unix group name of the meta-project used for administration.
	# Take care to avoid conflicts with group name existing on your system,
	# and to select a valid unix group name: no checks will be done for
	# this project unix group name.
	# [REQUIRED]
	#
	admin_project_name = SV_SITE_ADMIN_PROJECT()

	# Default theme.
	# [REQUIRED]
	#
	default_theme = Savannah

	# Logo name.
	# The engine will search for a file like
	# savane/frontend/php/images/$default_theme.theme/$logo_name.
	# If you do not want any logo, comment out.
	#
	logo_name = floating.png

	# Path to the user* binaries
	# Directory where useradd/usermod/userdel are located.
	# Normally, it is in the PATH, so leave this commented out
	# unless you wish to use specific versions.
	#
	#userx_prefix =

[forum]
	# Forum Comments.
	# Enable logged in users to post comments to forums.
	# This has proved to be the source of most spam messages on both Savane
	# and Puszcza.  The default is currently '0';
	#
	enable_comments = False

	# Use captcha when registering new users.
	# Set this to 1 if you have installed securimage
	# (see http://www.phpcaptcha.org) and want to use it to prevent
        # robots from registering new accounts in your Savane copy.
	#
	registration_captcha = False

	# Securimage installation directory.
	# If you set registration_captcha to 1, define this to the securimage
	# installation directory.
	#
	#securimage_directory =

[auth]
        # Pathname of the cracklib dictionary file.
        #crack_dictionary = 
    
	# Use pwqcheck to enforce password security
	# When set to True, passwords will be checked using pwqcheck utility
        # from passwdqc package.  The latter can be obtained from
	# http://www.openwall.com/passwdqc/
	#
	pwcheck = False

	# Accept md5 hashes in user.user_pw column.
	# If set to True, Savane will attempt to treat stored password
        # (the user.user_pw column) as MD5 hash of the password, if normal
        # validation fails.  This normally can be needed only when upgrading
        # older versions of Savane.
	#
	accept_md5_hashes = False

	# Verify password after login.
	# After successful login, verify if the password hash (user_pw column)
	# is up to date, and upgrade it if not.
	#
	upgrade_hash = True

	# PAM support.
	#
	#use_pam = No

	# IMPORTANT: this part will be removed, replaced by PAM
	# Kerberos 5
	# If you do not know what it is about, you surely don't have to
	# deal with a kerberos server, say no here.
	#
	#use_krb5 = No

[mail]
	# Mail domain.
	# [REQUIRED]
	#
	#mail_domain = 

	# Admin mail address.
	# [REQUIRED]
	#
	#admin_address = root@localhost

	# A "No reply" address used in trackers.
	#
	#sender_address = root@localost

[users]
	# Savane users group name
	# Name of the UNIX group which is used as primary group for Savane
	# users.
	# [REQUIRED]
	#
	group = SV_USERS_GROUP()

	# Minimum UID
	# Savane users are assigned UIDs starting from that number.
	# [REQUIRED]
	#
	min_uid = 5000

	# Minimum GID
	# [REQUIRED]
	#
	min_gid = 5000

	# User home directory
	#
	home_dir = /home

	# User home directory subdirs
	# Users home is by default /home/user. If you set this to 1,
	# you'll get /home/u/user, and if you set it to 2, you'll get
	# /home/u/us/user. It may be very convenient if you have plenty 
	# of users.
	#
	#home_subdirs = 

	# [BACKEND ONLY]
	# User default shell.
	#  Shell for the users.  Use rush(1) or sv_membersh for limited
	#  access, or one of the shells (such as /bin/sh) for full user access
	#  (not recommended).
	# Example: "/bin/rush"
	#
	#shell = /bin/rush

[download]
	# Owner for user download directories.
	#
	owner = root

[homepage]
	# Owner for project home directories.
	#
	owner = root
    
[web oncommit]
     	# Program responsible for synchronizing project webspace with the
     	# corresponding VCS.  This is the command line inserted into the
     	# appropriate hook script (loginfo for CVS, post-commit for SVN,
     	# post-receive for Git).
       	#
     	#cvs = /usr/bin/vcsync %s
     	#svn = /usr/bin/vcsync
     	#git = /usr/bin/vcsync

[backend]
	# Syslog facility to use
	# All Savane backend scripts will use this for diagnostics.
	# Allowed values: user, daemon, auth, authpriv, mail, cron, and
	# local0 through local7.
	#
	#syslog_facility = user

[backend sv_cleaner]
	# Database cleaning
	# A special backend script will clean regularly the database.
	# If you do not want that cleaning to be done, comment out.
	# It is recommended to use it, even if your installation use no other
	# backend tool.
	#
	cron = true

[backend sv_cleaner timeout]
	# Specific timeouts for each cleaner item
	#
	pending_accounts = 3 days
	inactive_groups  = 1 day
	inactive_forms   = 1 day
        banned_ips       = 6 hours
        sessions         = 1 year
        passwd_requests  = 1 day

[backend sv_reminder]
	# Trackers reminder
	# A special backend script will check regularly the database and send
	# email to users in defined cases. An user can decide to receive
	# regularly task assigned to him in a batch ; a project administrator
	# can decide to make people that got item with high priority not 
	# closed receiving a batch.
	# Also, when an item is supposed to start or to finish, a reminder 
	# should be sent to anybody supposed to get notification for the
	# item.
	#
	cron = True

        # Limit the number of mails to be sent.  The exact number of mails
        # sent can be somewhat greater than that number, depending on the
        # settings in the group_preferences table.
        #max_mails =

        # Limit the number of items per mail.
        #max_items =

[backend sv_aliases]
	# If you do not want your system to be synchronized with database
	# automatically regarding to mail infos (/etc/aliases...), comment 
	# out.
	#
	cron = True

	# List of emails aliases.
	# If you do not want such file to be updated by the backend, 
	# comment out.
	#
	alias_file = /etc/mail/aliases

	# Command to run if aliases were updated.
	#
	rebuild_command = /usr/bin/newaliases

	# Ignore users from these groups
	# Aliases won't be created for users who are members of at least one
	# of the groups listed in this variable.
	#
	ignore_groups = post-office

	# Ignore users who are not members of any group.
        #
	ignore_lurkers = True

[backend sv_users]	 
	# If you do not want your system to be synchronized with database
	# automatically regarding to users infos (/home/$user, /etc/passwd)),
	# comment out.
	#
	cron = True
	 
	# Update authorized_keys files
	#
	authorized_keys = True

	# Skeleton directory
	# Files from this directory will be copied to each newly-created user
	# home directory.
	#
	skel_dir = /etc/savane/skel

	# Remove users with no supplementary group
	# When set to 1, sv_users(1) will remove system accounts of Savane
	# users who are not members of any groups.
	remove_unassigned_users = False

[backend sv_groups]
	# If you do not want your system to be synchronized with database
	# automatically regarding to groups infos (/etc/group), comment out.
	#
	cron = True

	# Name of the gitweb .projects file.
	# If set, it will be updated with appropriate info when a new
	# project is added to Savane.
	#
	#project_list = /srv/repo/gitroot/.projects

	# Name of the Cgit .cgitrepos file.
	# If set, it will be updated with appropriate info when a new 
	# project is added to Savane.
	#cgitrepos_file = /srv/repo/gitroot/.cgitrepos

[backend sv_mailman]
	# Define these if you have set up Mailman
    
	# Mailman installation directory
	#
	#mailman_dir = /var/www/mailman

	# Directory for mailman list aliases
	#
	#list_dir = /etc/mail/mailman

	# Login name of the user Mailman runs as
	#
	#user = mailman

	# Mailman archive directory
	# This is where Mailman stores HTML versions of received mails.
	#
	#archive_dir = /var/www/mailman/var/archives/html
    
	# If you do not want your system to be synchronized with database
	# automatically regarding the mailman list (assuming you have mailman
	# installed on your system), comment out.
	#
	cron = True

	# Keep archives of deleted mailing lists
	# If set to 1, sv_mailman will not remove archives remaining after
	# deletion of mailing lists.
        #
	#keep_archives = False
    
	# List directory file
	# If not set, sv_mailman will directly update alias_file,
	# inserting or removing statements corresponding to the mailing
	# lists.
	# If set, sv_mailman will not touch alias_fole, but will
	# instead, write a list of list names to the named file.  Unless
	# the value of this variable begins with a '/', it is taken relative 
	# to list_dir.
	#
	#directory_file =

[backend sv_spamchecker]
	# Spam checks with SpamAssassin
	# You can filter content posted on trackers through SpamAssassin.
	# This assumes that you have spamassassin daemon (spamd) running
	# that spamc can connect to.

        # If you want cron to start sv_spamchecker periodically, set this
        # to True.
        cron = True
    
	# If you set sys_spamcheck_spamassassin to "1" or "anonymous" 
	# (recommended), all posted content by anonymous will be filtered.
	# If you set sys_spamcheck_spamassassin to "2" or "all", all posted
	# content by anonymous and logged-in users (except projects members)
	# will be filtered.
	#
	check = anonymous

	# SpamAssassin connection options
	#  Pathname to the socket file, if using UNIX sockets
	#
	#socket = /var/run/spamd.sock

	# If spamd is listening on TCP/IP, the following two variables
	# must be set.

	# Hostname or IP address
	#
	#host =

	# Port number the daemon is listening on
	#
	#port =

	# Timeout value (seconds)
	#
	#timeout =
	 
	# User name
	#
        user = savane

[backend sv_dumpdb]
        # Run as a cronjob
        cron = True
    
	# Directory where database dumps will be stored.
	#dumpdir =

	# Max. level for tar incremental archives
	# If this variable is defined, the sv_dumpdb utility will archive
	# tracker attachments in incremental archives.  These archives will
	# be named trackers-att.LEVEL.tar, where LEVEL is an integer number 
	# between 0 and max_level - 1.
	#
	max_level = 3

	# Number of old files kept.
	# This defines the number of backup archives to keep.
	# If max_level is also set, sv_dumpdb will ensure that
	# the oldest trackers-att.LEVEL.tar file available have LEVEL equal 
	# to 0.
	# This means that in this case, the maximal number of backup files
	# will be max_rotate + max_level - 1.
	#
	#max_rotate =      

	# Compress backup files using this command.
	#
	compress_program = gzip -9

	# Compressed file suffix.
	#
	compress_suffix = .gz

[backend sv_repo_backup]

        # If you want cron to start sv_repo_backup periodically, set this
        # to True.
        cron = False
    
        # Run sv_repo_backup as this user
	#
	#user = root

        # Create snapshots
	#snapshots = False

        # Backup only groups of given types.
	#backup_types = Software

        # List of types to be excluded from the backup.
        #exclude_types = 
    
        # List of groups to be excluded from the backup
	#exclude_groups = ...

[backend sv_repo_backup output]
	cvs = /srv/download/snapshots/cvs
	svn = /srv/download/snapshots/svn
	git = /srv/download/snapshots/git
	hg  = /srv/download/snapshots/hg
    
[backend sv_getpubkeys]
	# Additional public key options
	# These will be returned by sv_getpubkeys along with the user's keys
        #
	#pubkey_options = no-pty,no-agent-forwarding

[index]
	## DOC INDEXER ##
	# Directory where the documentation sites are hosted.
	#
	#html_root =

	# Project documentation directories.
	# This is a list of directories under html_root/PROJECT
	# where indexer will look for project's HTML files.
	# 
	#doc_dir =

	# Index directory.
	# The directory where Sphinx indexer will keep its index files.
	#
	#index_dir =

	# Sphinx configuration trunk.
	# Name of the Sphinx configuration trunk file.  When creating actual
	# configuration file for sphinx, contents of this file will be
	# output verbatim at the beginning.  Use it to supply static (Savane-
	# independent) configuration settings.
	#
	#index_config_trunk = '/etc/sphinx.conf';

	# A full pathname to the xmlpipe2-type processor to be used in the
	# generated source statements.
	#
	#xmlpipe_command = /usr/local/bin/ps_docindex --cron

	# Reindex command line
	# Command to run in order to reindex documentation files of a project.
	# This must be a complete command line.  The $project or ${project}
	# occurring in it is replaced with the UNUX group name of the project.
	#
	# This is used by ps_reindex to reindex HTML material after upload.
	#
	#
	#reindex_command = /usr/local/bin/indexer \
        #                  --config /usr/local/bin/ps_sphinxconf \
        #                  --rotate --quiet $project

	# Reindex database file.
	# This GDBM(3) file is used to keep track of running ps_reindex 
	# processes and to schedule the actual reindexing.  The default is 
	# /tmp/reindex.db.
	#
	#reindex_db = /tmp/reindex.db

	# Reindexing delay.
	# Delay reindexing by this number of seconds.  This is done in order
	# to coalesce reindexing requests when several updates are performed
	# within a short interval.  The default value is 20 seconds.
	#
	#reindex_delay =

[development]
	# Developers of Savane should probably set this setting on their
	# test machine
	#
	debug = False

	# Prevent redirections like sv.gnu.org -> sv.nongnu.org
	#
	no_base_host = False

	# Use the following two variables to prevent people from being
	# spammed with messages triggered by development version:
	#
	# List of mails, which are allowed to receive mails.  Any recipients
	# not in that list will be filtered out.
	#
	#allow_mail_to = gray@gnu.org.ua gray@gnu.org

	# List of addresses to sent mail to, irrespective of the actual
	# recipients.  It is merged with the list of recipients obtained
	# from applying @sys_allow_mail_to.  If the latter is not defined,
	# original recipient list is discarded and replaced by this:
	#force_mail_to = gray@gnu.org.ua


# END

