#!/usr/bin/perl
# Copyright (C) 2016, 2019  Sergey Poznyakoff <gray@gnu.org>
#
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use Savane::Conf;
use Savane::Backend;
use Savane::Trackers;
use Savane::DB;
use Safe;

my $filename;
my @rxlist;

backend_setup(descr => 'Update URLs in Savane database',
	      quiet => 1,
	      options => {
		  "file|f=s" => sub {
		      open(my $fd, '<', $_[1])
			  or abend(EX_NOINPUT, "can't open file $_[1]: $!");
		      local $/ = undef;
		      push @rxlist, <$fd>;
		      close $fd;
		  }
	      }
    );

while (my $rx = shift @ARGV) {
    push @rxlist, $rx, ';';
}

abend(EX_USAGE, "no code supplied") unless @rxlist;

my $code = join('', @rxlist);
my $compartment = new Safe;

my @tables = (
    { table => 'group_type',
      key => 'type_id',
      columns => [ 'base_host',
		   'mailing_list_host',
		   'url_homepage',
		   'url_download',
		   'url_cvs_viewcvs',
		   'url_arch_viewcvs',
		   'url_svn_viewcvs',
		   'url_git_viewcvs',
		   'url_hg_viewcvs',
		   'url_bzr_viewcvs',
		   'url_cvs_viewcvs_homepage',
		   'url_mailing_list_listinfo',
		   'url_mailing_list_subscribe',
		   'url_mailing_list_unsubscribe',
		   'url_mailing_list_archives',
		   'url_mailing_list_archives_private',
		   'url_mailing_list_admin',
		   'url_extralink_documentation' ] },
    { table => 'groups',
      key => 'group_id',
      columns => [ 'url_homepage',
		   'url_download',
		   'url_forum',
		   'url_support',
		   'url_mail',
		   'url_cvs',
		   'url_cvs_viewcvs',
		   'url_cvs_viewcvs_homepage',
		   'url_arch',
		   'url_arch_viewcvs',
		   'url_svn',
		   'url_svn_viewcvs',
		   'url_git',
		   'url_git_viewcvs',
		   'url_hg',
		   'url_hg_viewcvs',
		   'url_bzr',
		   'url_bzr_viewcvs',
		   'url_bugs',
		   'url_task',
		   'url_patch',
		   'url_extralink_documentation' ] },
    { table => '${tracker}',
      key => 'bug_id',
      columns => [ 'summary', 'details' ] },
    { table => '${tracker}_cc',
      key => 'bug_cc_id',
      columns => [ 'comment' ] },
    { table => '${tracker}_history',
      key => 'bug_history_id',
      columns => [ 'old_value', 'new_value' ] },
    { table => 'news_bytes',
      key => 'id',
      columns => [ 'summary', 'details' ] }
    );

sub change_record {
    my $table = shift;
    my $key = shift;
    my $ref = shift;
    my @update;
    my @values;
    while (my ($col, $val) = each %{$ref}) {
	next if $col eq $key || !defined $val;
	$_ = $val;
	if ($compartment->reval($code)) {
	    push @update, "$col=?";
	    push @values, $_;
	}
    }
    return unless @update;
    if (dry_run()) {
	my $expr = "UPDATE $table SET ";
	foreach my $upd (@update) {
	    my $val = shift @values;
	    $upd =~ s/\?$/'$val'/;
	    $expr .= $upd;
	    $expr .= ', ' unless $#values == -1;
	}
	$expr .= " WHERE $key=$ref->{$key}";
	debug($expr);
    } else {
	db_modify("UPDATE $table SET " . join(',', @update) . " WHERE $key=?",
 	          @values, $ref->{$key});
    }
}

sub change_table {
    my $t = shift;
    logit('info', "table $t->{table}");
    db_foreach(sub { change_record($t->{table}, $t->{key}, $_[0]) },
	       "SELECT $t->{key}, " . join(',', @{$t->{columns}}) .
	       " FROM $t->{table}");
}
	
foreach my $t (@tables) {
    if ($t->{table} =~ /\$\{tracker\}/) {
	foreach my $tracker (TrackerNames()) {
	    my %td = %{$t};
	    $td{table} =~ s/\$\{tracker\}/$tracker/;
	    change_table(\%td);
	}
    } else {
	change_table($t);
    }
}

backend_done;    
=head1 NAME

sv_sed - edit text fields in Savane database

=head1 SYNOPSIS

B<sv_sed>    
[B<-F> I<FACILITY>]
[B<-dn>]
[B<-f> I<FILE>]    
[B<--debug>]
[B<--dry-run>]
[B<--facility=>I<FACILITY>]
[B<--file=>I<FILE>]    
[B<--stderr>]
[B<--syslog>]
[I<EXPR>...]

=head1 DESCRIPTION

Edits text fields in Savane databases using the supplied Perl expressions.

The expressions can be supplied either as command line arguments, or via
I<FILE> given with the B<--file> (B<-f>) option.  If several B<--file> options
are used, the content of their arguments is concatenated.  If both B<--file>
options and command line arguments are supplied, files are read and
concatenated first, then the expressions read from the arguments are appended
to the resulting perl code.    

Example usage:

    sv_sed 's/savane\.gnu\.org/ps.gnu.org.ua/g'

For a list of affected tables and columns, inspect the source code, variable
B<@tables>.    
    
=head1 OPTIONS    

=over 8
    
=item B<-d>, B<--debug>

Increase debugging level.

=item B<-n>, B<--dry-run>

Do nothing, print everything.  Implies B<--debug>.

=item B<-F>, B<--facility=>I<NAME>

Log to the syslog facility I<NAME>.  If I<NAME> begins with a slash,
it is understood as the name of the file where to direct the logging
output.

=item B<-f>, B<--file>=I<FILE>

Read expressions from I<FILE>.     
    
=item B<--stderr>

Log to standard error.  This is the default.
    
=item B<--syslog>

Log to syslog.

=back

=head1 CONFIGURATION VARIABLES

=over 4

=item B<sql.database>

=item B<sql.user>

=item B<sql.password>

=item B<sql.host>

=item B<sql.params>
    
Database credentials.    

=back

=head1 EXIT CODES

=over 8

=item 0

Successful termination.

=item 64

Command line usage error.

=item 66
    
Cannot open input file.

=item 78

Configuration error.
    
=back
    
