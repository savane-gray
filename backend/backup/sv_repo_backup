#! /usr/bin/perl
# Copyright (C) 2005, 2011, 2016 Sergey Poznyakoff <gray--at--gnu.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use Savane;
use Savane::Backend;
use Savane::Conf;
use Cwd;
use File::stat;
use File::Basename;
use File::Path qw(make_path remove_tree);
use Savane::Conf;
use Data::Dumper;

# Preconfigure
my $ref;

my %output_dir;
if (defined($ref = GetConf('backend.sv_repo_backup.output'))) {
    %output_dir = %{$ref};
}

my %include_types;
if (defined($ref = GetConf('backend.sv_repo_backup.backup_types'))) {
    my @a = split /\s+/, $ref;
    @include_types{@a} = (1) x @a;
}

my %exclude_types;
if (defined($ref = GetConf('backend.sv_repo_backup.exclude_types'))) {
    my @a = split /\s+/, $ref;
    @exclude_types{@a} = (1) x @a;
}

my %exclude_groups;
if (defined($ref = GetConf('backend.sv_repo_backup.exclude_groups'))) {
    my @a = split /\s+/, $ref;
    @exclude_groups{@a} = (1) x @a;
}

my $build_snapshots = GetConf('backend.sv_repo_backup.snapshots');
my $runas_user = GetConf('backend.sv_repo_backup.user');

sub exclude_group($$) {
    my ($group, $type) = @_;
    return (defined($exclude_groups{$group})
	    || defined($exclude_types{$type})
	    || (keys(%include_types) && !$include_types{$type}));
}


backend_setup(descr => "Backup Savane repositories.",
              cron => GetConf('backend.sv_repo_backup.cron'));

if (defined($runas_user)) {
    if ($< == 0) {
	my (undef,undef,$uid,$gid,undef,undef,undef,$dir)
	    = getpwnam($runas_user)
	    or die("No such user: $runas_user");
	my $g = $gid;
	setgrent();
	while (my (undef,undef,$gid,$members) = getgrent()) {
	    $g .= " $gid" if ($members =~ /\b${runas_user}\b/);
	}
	endgrent();
	$g .= " $gid" if ($g eq $gid);
	
	$) = $g;
	die("can't set effective groups: $!") if ($!);
	$( = $gid;
	die("can't set real gid: $!") if ($!);
	$< = $> = $uid;
	die("can't set uid: $!") if ($!);
	$ENV{'USER'} = $runas_user;
	logit("info", "running as $runas_user ($uid,$gid)");
	chdir($dir)
	    or die("can't change to \"$dir\": $!");
	$ENV{'HOME'} = $dir;
    } else {
	abend(EX_USAGE, "can't switch to user $runas_user privileges");
    }
}

my @repo_types = ('cvs', 'arch', 'svn', 'git', 'hg', 'bzr');

foreach my $repotype (@repo_types) {
    my $outdir = $output_dir{$repotype};
    unless ($outdir) {
	debug("no output directory for $repotype");
	next;
    }
    db_foreach(sub {
	           my ($ref) = @_;

		   return if exclude_group($ref->{unix_group_name},
					   $ref->{group_type_name});

		   $ref->{feature}{$repotype}{dir_type} = $ref->{"dir_type_$repotype"};
		   $ref->{feature}{$repotype}{dir} = 
		       (exists $ref->{"dir_$repotype"}
			and $ref->{"dir_$repotype"} ne '')
		       ? $ref->{"dir_$repotype"}
		   : $ref->{"group_type_dir_$repotype"};

		   delete $ref->{"use_$repotype"};
		   delete $ref->{"group_type_can_use_$repotype"};
		   delete $ref->{"dir_type_$repotype"};
		   delete $ref->{"dir_$repotype"};
		   delete $ref->{"group_type_dir_$repotype"};

		   if (debug_level()) {
		       local $Data::Dumper::Indent = 0;
		       local $Data::Dumper::Terse = 1;
		       local $Data::Dumper::Purity = 0;
		       debug(Dumper($ref));
		   }

		   my $obj = backend_feature($repotype, $ref) or return;

		   $obj->backup($outdir);
		   backend_feature_log($obj);

		   if ($build_snapshots) {
		       $obj->snapshot($outdir);
		       backend_feature_log($obj);
		   }
	       },
               qq{SELECT groups.unix_group_name AS unix_group_name, 
		         groups.dir_$repotype AS dir_$repotype,
		         groups.group_id AS group_id,  
		         groups.group_name AS group_name,
		         groups.type AS type,  
		         groups.is_public AS is_public,
		         groups.gidNumber AS gidNumber,
                         group_type.name AS group_type_name,
                         group_type.dir_$repotype AS group_type_dir_$repotype, 
		         group_type.dir_type_$repotype AS dir_type_$repotype
                   FROM groups, group_type
                   WHERE groups.use_$repotype=1
		     AND group_type.can_use_$repotype=1
                     AND groups.type=group_type.type_id});
    # FIXME: Subrepos
}

backend_done();

=head1 NAME

sv_repo_backup - Backup source repositories.

=head1 SYNOPSIS

B<sv_repo_backup>
[B<-F> I<FACILITY>]
[B<-dn>]
[B<--cron>]
[B<--debug>]
[B<--dry-run>]
[B<--facility=>I<FACILITY>]
[B<--stderr>]
[B<--syslog>]    

=head1 DESCRIPTION

This utility creates backups and, optionally, snapshots for the repositories
controlled by Savane.

In its present state, it supports CVS, SVN, Git and Mercurial repositories.

=head1 OPTIONS

=over 8

=item B<-F>, B<--facility=>I<NAME>

Log to the syslog facility I<NAME>.  If I<NAME> begins with a slash,
it is understood as the name of the file where to direct the logging
output.

=item B<--syslog>

Log to syslog.

=item B<--cron>
    
Run only if the configuration variable B<$sys_cron_repo_backup> is set to
B<true>.  Implies B<--syslog>.
    
=item B<--stderr>

Log to standard error.  This is the default.
    
=item B<-d>, B<--debug>
    
Increase debugging verbosity.

=item B<-n>, B<--dry-run>

Do nothing, print everything.  Implies B<--debug>.

=item B<-h>

Show a terse help summary and exit.

=item B<--help>

Prints the manual page and exits.

=back

=head1 CONFIGURATION VARIABLES

=over 4
    
=item B<backend.sv_repo_backup.user>
    
User to run as.

=item B<backend.sv_repo_backup.snapshots>

Boolean value.  Create repository snapshots if B<true>.

=item B<backend.sv_repo_backup.backup_types>

Whitespace-separated list of group types to include in the backup.  By
default, all groups are included.    
    
=item B<backend.sv_repo_backup.exclude_types>

Whitespace-separated list of group types to exclude from the backup.
    
=item B<backend.sv_repo_backup.exclude_groups>

Whitespace-separated list of group names to exclude from the backup.   
    
=item B<sql.database>

=item B<sql.user>

=item B<sql.password>

=item B<sql.host>

=item B<sql.params>

Database credentials.    

=back

=head1 EXIT CODES

=over 8

=item 0

Successful termination.
    
=item 64

Command line usage error.

=item 69

Other error occurred.
    
=item 71

System error (e.g., can't fork).
    
=item 73

Can't create output file or directory.

=back
    
=head1 BUGS

Bazaar is not supported.
    
=head1 AUTHOR

Sergey Poznyakoff <gray@gnu.org>
    
=cut    
    

