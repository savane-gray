# Copyright (C) 2015      Sergey Poznyakoff <gray@gnu.org>
#
# This file is part of Savane.
# 
# Savane is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Savane is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;

use Getopt::Long qw(:config gnu_getopt no_ignore_case);
use Savane;
use File::Basename;
use Pod::Usage;
use Pod::Man;

=head1 NAME

mkstruct.pl - Dump Savane database structure

=head1 SYNOPSIS

B<perl mkstruct.pl>
[B<-ikh>]
[B<-o> I<DIR>]
[B<--help>]    
[B<--ignore-engine>]
[B<--keep>]
[B<--output-directory=>I<DIR>]
[B<--usage>]

=head1 DESCRIPTION

Dumps the database structure to disk files.  Each table I<T> is dumped into a
separate file named B<table_I<T>.structure>.  The contents of the directory
is scanned before dumping and existing files are read.  Their content is then
used to determine which tables have changed structure since the last dump.

Files without a corresponding table are deleted.  New files are created as
new tables appear.  Git indices are updated.    

It is supposed that Savane is properly installed on the system.  The utility
reads the standard Savane configuration file to determine database name and
credentials.    
    
=head1 OPTIONS

=over 4

=item B<-i>, B<--ignore-engine>

Don't add B<ENGINE=I<NAME>> statement to the created structure files.

=item B<-k>, B<--keep>

Don't delete files which have no corresponding tables in the database.
    
=item B<-h>

Display a summary of command line options and exit.    

=item B<--help>

Display a detailed help and exit.    
    
=item B<-o>, B<--output-directory=>I<DIR>

Create files in the directory I<DIR>, instead of the current working
directory.    
    
=item B<--usage>

Display command line usage summary and exit.

=back

=head1 BUGS    

Header text for created structure files is hardcoded.
    
=head1 AUTHOR

Sergey Poznyakoff <gray@gnu.org>

=cut
    
my $progname = basename($0);
my $progdescr = 'Dump Savane database structure';
my $outdir;
my $keep;
my $ignore_engine;
my $force;

my $file_header = <<EOT;
-- This file is part of Savane.
-- 
-- Savane is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
-- 
-- Savane is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.

EOT
;    

sub norm_defn {
    my $defn = shift;
    $defn =~ s/\n//m;
    $defn =~ s/\s+/ /g;
    $defn =~ s/\bdefault\b/DEFAULT/g;
    $defn =~ s/\bauto_increment\b/AUTO_INCREMENT/;
    $defn =~ s/ AUTO_INCREMENT=\d+//;
    $defn =~ s/\s+$//;
    $defn =~ s/\s*;//;
    return $defn;
}

sub read_defn {
    my $file = shift;
    open(my $fd, '<', $file) or die "can't open $file: $!";
    my $defn;
    while (<$fd>) {
	chomp;
	s/\s+$//;
	if (/^CREATE TABLE/) {
	    $defn = $_;
	} elsif (defined($defn)) {
	    $defn .= "\n$_";
	    last if /;$/;
	}
    }
    close $fd;
    $defn =~ s/\bENGINE=\w+\b// if $ignore_engine;
    return $defn;
}

sub table_defn {
    my ($table, $old) = @_;
    my $ref = $Savane::DB::dbd->selectall_arrayref("SHOW CREATE TABLE $table");
    my $dfn = $ref->[0][1];
    $dfn =~ s/\bENGINE=\w+\b// if $ignore_engine;
    return undef if norm_defn($dfn) eq norm_defn($old);
    $dfn =~ s/(\s+AUTO_INCREMENT=)\d+/${1}0/;
    return <<EOT
--
-- Table structure for table `$table`
--
DROP TABLE IF EXISTS `$table`;
$dfn;

EOT
;
}	

GetOptions("h" => sub {
               pod2usage(-message => "$progname: $progdescr",
			 -exitstatus => 0);
           },
           "help" => sub {
	       pod2usage(-exitstatus => 0, -verbose => 2);
           },
           "usage" => sub {
	       pod2usage(-exitstatus => 0, -verbose => 0);
           },
	   "keep|k" => \$keep,
	   "force|f" => \$force,
	   "ignore-engine|i" => \$ignore_engine,
	   "output-directory|o=s" => \$outdir) or exit(1);

chdir $outdir or die "can't change to $outdir: $!";

my %filetab;
foreach my $f (glob "table_*.structure") {
    $filetab{$f} = 1;
}

my @tablelist = sort @{$Savane::DB::dbd->selectcol_arrayref("SHOW TABLES")};

my $mod = 0;
foreach my $table (@tablelist) {
    my $fname = "table_$table.structure";
    my $old = read_defn($fname) if (!$force and defined($filetab{$fname}));
    my $dfn = table_defn($table, $old);
    if (defined($dfn)) {
	++$mod;
	print STDERR "$progname: rewrite $fname\n";
	open(my $tfd, '>', $fname) or die "can't create $fname: $!";
	print $tfd $file_header if defined $file_header;
	print $tfd $dfn;
	close $tfd;
    }
    if (defined($filetab{$fname})) {
	delete $filetab{$fname};
    } elsif (!$keep) {
	system("git add $fname");
    }
}

my $n = keys %filetab;
if ($n) {
    print STDERR "$progname: $n files removed\n";
    foreach my $file (keys %filetab) {
	print " - $file\n";
	next if $keep;

	system("git ls-files --error-unmatch $file >/dev/null 2>&1");
	if ($? == -1) {
	    die "failed to execute git: $!";
	} elsif ($? & 127) {
	    die "git died with signal " . ($? & 127);
	} elsif ($? >> 8) {
	    unlink $file or
		print STDERR "failed to remove file $file: $!\n";
	} else {
	    system("git rm $file");
	}
    }
}
