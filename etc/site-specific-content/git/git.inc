<?php

define('GIT_HOST_NAME',"git.gnu.org.ua");

function print_anongit()
{
  global $project;

  if ($project->CanUse("git")) {
    print '<p><pre>git clone git://'.GIT_HOST_NAME.'/'.$project->getUnixName().
          '.git</pre></p>';
  }
}

function print_sshgit($membername) 
{
  global $project, $sys_shell;

  $username = user_getname();
  $project_dir = "/".$project->getUnixName().".git";
  if ($username == "NA") {
    // for anonymous user :
    $username = '&lt;<em>'.$membername.'</em>&gt;';
  } else {
    $pw = posix_getpwnam($username); 
    if ($pw && $pw["shell"] != $sys_shell)
      $project_dir = $project->getTypeDir("git");
  }

  if ($project->CanUse("git")) {
    print '<p><pre>git clone ssh://'.$username.'@'.GIT_HOST_NAME.
          $project_dir. 
          '</pre></p>';
  }
}
?>
